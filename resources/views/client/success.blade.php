@extends('client.master')
@section('title','Home')
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- Style-CSS -->
<link href="css/font-awesome.css" rel="stylesheet"><!-- font-awesome-icons -->
<style type="text/css">
    .shop-tb{
        overflow: auto;
    }
    table{
        text-align: center;
        width: 100%;
    }
    th{
        min-width: 150px;
        padding: 10px 0;
    }
    th button{
        background: black;
        color: white;
        padding: 5px 10px;
        outline: none;
    }
    .table-p{
        border: 1px solid;
    padding: 10px 10px;
    }
    h1{
        text-align: center;
        padding-top: 10px;
        font-family: 'Times New Roman',Times New Roman;
        color:red;
    }
    #data{
        margin-top:30px;
    }
    ul{
        style="list-style-type:disc"
    }
</style>
@section('content')
    <h1>Đặt hàng thành công</h1>
    <hr>
    <div id="data" class="container">
        <ul style="list-style-type:square">
            <li>
                - Cảm ơn bạn đã mua sản phẩm của chúng tôi. Chúng tôi sẽ liên hệ với bạn sớm nhất có thể để giao hàng cho bạn.
                Thời gian giao hàng có thể từ 3 đến 5 ngày từ  ngày bạn đặt mua sản phẩm.
                Chúc bạn có một ngày tốt lành.
            </li>
            <li>- Bạn cũng có thể xem các chính sách mua hàng của chúng tôi tại đây.</li>
            <li>-Liên hệ với chúng tôi sớm nhất có thể nếu bạn có bất kì thắc mắc gì.</li>
            
        </ul>
        <div class="text-center mt-5 mb-5">
            
            <button type="submit" class="btn btn-primary" ><a style="color: white" href="https://mail.google.com/mail/u/0/#inbox">Đến hòm thư của bạn</a></button> 
        
            <button type="submit" class="btn btn-primary" ><a style="color: white" href="{{route('getHome')}}">Về Trang Chủ</a></button> 
        </div>
        
    </div>
    
@endsection('content')