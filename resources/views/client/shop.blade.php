@extends('client.master')
@section('title','Danh mục')
@section('content')
    <!-- content web -->
    <div class="container">
        <div class="row row__content mt-40">
            <div class="col-md-3 left-layout">
                <h4 class="sidebar__title">
                    Danh mục sản phẩm
                </h4>
                <ul class="sidebar__list">
                    @foreach($category as $value_cate)
                        <li class="mega__menu--item">
                            <a href="{{route('getCate',['id'=>$value_cate->id])}}" class="mega__menu--link">{{$value_cate->name}}</a>
                        </li>
                    @endforeach  
                </ul>
            </div>
            <div class="col-md-9 right-layout">
                <!-- grid product -->
                <div class="row">
                    
                        @if(count($product) == 0 )
                            Không có sản phẩm!
                        @else
                            @foreach($product as $value_prod)
                            <div class="col-md-4 mt-3">
                                    <div class=" item__product">
                                            <span class="onsale">
                                                <span class="saled">
                                                    @if($value_prod->promotion >0)
                                                       giảm giá {{$value_prod->promotion}} %
                                                    @endif
                                                </span>
                                                <br />
                                                <span class="featured">
                                                    @if($value_prod->buy >100)
                                                        {{$value_prod->buy}}
                                                    @endif
                                                </span>
                                            </span>
                                            <!-- group button on hover -->
                                            <div class="group-button">
                                                <div class="add-cart">
                                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                                </div>
                                                <div class="btn-wishliss">
                                                    <i class="fa fa-heart group-button--icon"></i>
                                                </div>
                                                <div class="quick-view">
                                                    <i class="fa fa-eye group-button--icon"></i>
                                                </div>
                                            </div>
                                            <!-- caption -->
                                            <div class="caption">
                                                <h3 class="name-product">{{$value_prod->name}}</h3>
                                                <div class="price-product">
                                                    @if($value_prod->promotion >0)
                                                        <span class="price-product--cost">
                                                            <strike>{{number_format($value_prod->price,0,'.',',')}}</strike> VNĐ
                                                        </span>
                                                        <span class="price-product--sale">
                                                            @if($value_prod->promotion >0)
                                                               - {{number_format($value_prod->price - ($value_prod->price*($value_prod->promotion/100)),0,'.',',')}}
                                                               VNĐ
                                                            @endif
                                                        </span>
                                                    @else
                                                         <span class="price-product--cost">
                                                            {{number_format($value_prod->price,0,'.',',')}}
                                                        </span>
                                                    @endif                                              
                                                </div>
                                            </div>
                                            <img src="../../public/uploads/img_product/{{$value_prod->img}}" class="product__img" alt="" />
                                        </div>
                                    </a> 
                                    </div>
                            @endforeach

                        @endif

                   
                    

                    
                </div>
                <!-- pagination -->
                <!-- <nav class="pagination mb-50">
                    <ul class="pagination__box">
                        <li class="pagination__item">
                            <a href="" class="pagination__link">1</a>
                        </li>
                        <li class="pagination__item">
                            <a href="" class="pagination__link">2</a>
                        </li>
                        <li class="pagination__item">
                            <a href="" class="pagination__link">3</a>
                        </li>
                        <li class="pagination__item">
                            <a href="" class="pagination__link"><i class="fa fa-chevron-right"></i></a>
                        </li>
                    </ul>
                </nav> -->
            </div>
        </div>
    </div>
@endsection('content')