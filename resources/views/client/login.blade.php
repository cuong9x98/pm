@extends('client.master')
@section('title','Đăng nhập')
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- Style-CSS -->
<link href="css/font-awesome.css" rel="stylesheet"><!-- font-awesome-icons -->
@section('content')
@if(session('error'))
<div class="alert alert-danger" role="alert">
    {{ session('error') }}
</div>
@endif
@if(session('mess'))
<div class="alert alert-success" role="alert">
    {{ session('mess') }}
</div>
@endif
<section class="signin-form">
    <div class="container-fluid">
        <div class="wrapper">
            <div class="logo text-center top-bottom-gap">
               
                <!-- if logo is image enable this   
			<a class="brand-logo" href="#index.html">
			    <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
			</a> -->
            </div>
            <div class="form34">
                <h4 class="form-head">Đăng nhập</h4>
               
                <p class="form-para">Đăng nhập nhanh bằng tài khoản facebook va google</p>
                <div class="main-div">
                    <a href="{{url('/getInfo-facebook/facebook')}}">
                        <div class="signin facebook">
                            <span class="fa fa-facebook" aria-hidden="true"></span>
                            <p class="action">facebook</p>
                        </div>
                    </a>
                    <a href="#google-plus">
                        <div class="signin google-plus">
                            <span class="fa fa-google-plus" aria-hidden="true"></span>
                            <p class="action">google</p>
                        </div>
                    </a>
                </div>
                <div class="form-34or">
                    <span class="pros">
                        <span>or</span>
                    </span>
                </div>
                <form action="{{route('postLoginUser')}}" method="post" id="login">
                @csrf
                    <div class="">
                        <p class="text-head">Tài Khoản</p>
                        <input type="text" name="username" class="input" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Mật khẩu</p>
                        <input type="password" name="password" minlength="3" class="input" placeholder="" required="" />
                    </div>
                    <label class="remember">
                    <a href="{{route('forget')}}">Quên mật khẩu</a>
                    </label>
                    <input type="submit" class="btn btn-success btn-block" value="Đăng nhập"></input>
                    <p class="signup">Bạn chưa có tài khoản?<a href="{{route('getAddUser')}}" class="signuplink">Đăng kí ngay bây
                            giờ</a>
                    </p>
                </form>
            </div>
        </div>

    </div>
</section>
<script src="assets/js/jquery-3.5.1.min.js"></script>
<script>
     $("#login").validate({
        submitHandler: function(form) {
        $(form).submit();
    }
 });
</script>
@endsection
