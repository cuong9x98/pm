@extends('client.master')
@section('title','Đăng kí')
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- Style-CSS -->
<link href="css/font-awesome.css" rel="stylesheet"><!-- font-awesome-icons -->
@section('content')
@if(session('success'))
<div class="alert alert-success" role="alert">
    {{ session('success') }}
</div>
@endif
<section class="signin-form">
    <div class="container-fluid">
        <div class="wrapper">
            <div class="logo text-center top-bottom-gap">

                <!-- if logo is image enable this   
			<a class="brand-logo" href="#index.html">
			    <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
			</a> -->
            </div>
            
            <div class="form34">
                <h4 class="form-head">Đăng Ký</h4>


                <div class="form-34or">
                    <span class="pros">
                        <span>or</span>
                    </span>
                </div>
                <form action="{{route('postAddUser')}}" method="post" id="register">
                    @csrf
                    <div class="">
                        <p class="text-head">Tên</p>
                        <input type="text" name="name" class="input" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Tài Khoản</p>
                        <input type="text" name="username" class="input" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Mật khẩu</p>
                        <input type="password" name="password" minlength=3 class="input" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Điện Thoại</p>
                        <input type="text" name="numberphone" class="input" number="true" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Email</p>
                        <input type="text" name="email" class="input" email="true" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Địa chỉ</p>
                        <input type="text" name="address" class="input" placeholder="" required="" />
                    </div>
                    
                    <!-- radio -->
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="sex" value="1"> Nam
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="sex" value="0">Nữ 
                        </label>
                    </div>
                    
                    </br>
                    <input type="submit" class="btn btn-success btn-block" value="Đăng kí"></input>

                </form>
            </div>
        </div>

    </div>
</section>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>


<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
// just for the demos, avoids form submit
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
$( "#register" ).validate({
  rules: {
    email: {
      required: true,
      email: true
    }
  },
  
  }

  }
  
  

});
</script>


@endsection
