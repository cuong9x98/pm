@extends('client.master')
@section('title','Home')

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style type="text/css">
    .shop-tb {
        overflow: auto;
    }

    table {
        text-align: center;
        width: 100%;
    }

    th {
        min-width: 150px;
        padding: 10px 0;
    }

    th button {
        background: black;
        color: white;
        padding: 5px 10px;
        outline: none;
    }

    .table-p {
        border: 1px solid;
        padding: 10px 10px;
    }

</style>
<style>
    /* ========= ******* Increment and Decerement Input Type Number ========= ******* */

    .product-quantity>* {
        width: 36.9px;
    }

    .product-quantity>input[type="number"]::-webkit-inner-spin-button,
    .product-quantity>input[type="number"]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .product-quantity>input {
        border: none;
        text-align: center;
        font-size: 12px;
    }

    .product-quantity {
        display: inline-flex;
        border: 1px solid #e6e6e6;
        align-items: center;
        height: 40px;
        border-radius: 4px;
    }

    .product-quantity-plus:before,
    .product-quantity-minus:before {
        width: 11px;
        display: block;
        margin: 0 auto;
    }

    .product-quantity-plus:before {
        content: "+";
    }

    .product-quantity-minus:before {
        content: "-";
    }

    .product-quantity-plus,
    .product-quantity-minus {
        cursor: pointer;
    }

    .product-quantity-plus:before,
    .product-quantity-minus:before {
        width: 11px;
        display: block;
        margin: 0 auto;
    }
    th{
        text-align: center;
    }
    .top__menu {
    margin-bottom: 0;
}
</style>

<?php 
    $content = Cart::content();
    $count = Cart::count();
?>
<h2 style="text-align: center;">Giỏ Hàng</h2>
<hr>
<div class="container">
    <div class="shop-tb mt-5">
        <table border="1" id="cart-items">
            <tr>
                <th>ID</th>
                <th>Tên của sản phẩm</th>
                <th>Ảnh sản phẩm</th>
                <th>Số lượng</th>
                <th>Giá</th>
                <th>Tổng tiền</th>
                <th></th>
            </tr>
            <form action="" method="post">
                @csrf
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <tbody class="row-cart">
                    @foreach($content as $result)
                <tr id="{{$result->id}}" class="{{$result->rowId}}">
                        <td>{{ $result->id }}</td>
                        <td>{{ $result->name }}</td>
                        <td><img src="../../public/uploads/img_product/{{$result->options->image}}" width="80px" height="80px" alt=""></td>
                        <td>
                            <div class="product-quantity">
                                {{-- <form action="{{route('updateCart')}}" method="post">
                                @csrf --}}
                                <div class="product-quantity" style="width:100px">
                                    <a class="update_cart" id="{{$result->rowId}}" onclick="return updateCart(this);"><span class=""></span></a>
                                    <input class="qty" type="" style="width:25px" value="{{$result->qty}}"name="quantity" />
                                    <a class="update_cart" id="{{$result->rowId}}" onclick="return updateCart(this);"><span class="glyphicon glyphicon-repeat"></span></a>
                                </div>
                                <input type="hidden" value="{{$result->rowId}}" name="rowId" class="form-control">
                                
                            </div>
                             {{-- </form> --}}
                        </td>
                        <td>
                            {{number_format($result->price,0,'.',',').' '.'VNĐ'}}
                        </td>
                        <td>
                            {{number_format($result->price*$result->qty,0,'.',',').' ' .'VNĐ'}}
                        </td>
                        <th style="text-align:center;font-size:48px" id="{{$result->rowId}}" class="delete_row" onclick="return delete_cart(this);"><a class="glyphicon glyphicon-trash" ></a></th>
                    </tr>
                    @endforeach
                </tbody>
            </form>
        </table>
    </div>
    <div class="table-p mt-5">
        <div class="table-p-content">
            <div class="row">
                <div class="col-md-2 col-4">
                    <span><b>Tổng:</b></span>
                </div>
                <div class="col-md-1 col-1">
                    <p id="subtotal">{{Cart::subtotal()}}</p>
                </div>
                <div class="col-md-9 col-7" style="float:left;">
                    <p>VNĐ</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2 col-4">
                    <span><b>Ghi chú:</b></span>
                </div>
                <div class="col-md-10 col-8">
                    <p>Ship trong vòng 3 ngày </p>
                </div>
            </div>
        </div>
    </div>

    <div class="text-center mt-5 mb-5">
        @if ($count!=0)
        <button type="button" class="btn btn-primary"><a style="color:white" href="{{route('checkout')}}">Thanh
                Toán</a></button>
        @endif
        <button type="button" class="btn btn-primary"><a style="color:white" href="{{route('getHome')}}">Tiếp tục mua
                sắm</a></button>
    </div>
</div>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
<script>
    function updateCart(e) {
            var row_id = $(e).attr('id');
            var qty = $(e).parent().find('.qty').val(); //prarent nhảy ra ngoài thẻ
            var token = $("input[name='_token']");
            $.ajax({
                url: '{{route('editCart')}}',
                type: 'GET',
                cache: false,
                data: {
                    "id": row_id,
                    "qty": qty,
                },
                success: function (data) {
                    $('#count').html(data['count']);
                    $('#subtotal').html(data['subtotal']);
                    alertify.success('Đã thêm sản phẩm thành công'); 
                    $(".row-cart").html(data['string']); 
                    
                }
            });
        }
</script>
<script>
   
    function delete_cart(e) {
        var row_id = $(e).attr('id');
    
        $.ajax({ 
            url: '{{route('xoaCart')}}',
            type: 'GET',
            data: {
                "id": row_id,
            },
            success: function (data) {
               
               $('tbody').find("tr."+row_id).remove();
               $('#count').html(data['count']);
               $('#subtotal').html(data['subtotal']);
               alertify.success('Xóa thành công');
               
            }
        });
    }
    

</script>

@endsection('content')
@section('scripts')

@endsection
