@extends('client.master')
@section('title','Chi tiết sản phẩm')
@section('style-k')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="assets/css/style-k.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-k.css">
<link rel="stylesheet" href="assets/css/img-product-detail.css">

@endsection('style-k')
@section('content')

<!-- Style commnet -->
<style>
    #mess {
        border: 2px solid #dedede;
        background-color: #f1f1f1;
        border-radius: 5px;
        padding: 10px;
        margin: 10px 0;
    }

    #img {
        float: left;
        max-width: 60px;
        width: 100%;
        margin-right: 20px;
        border-radius: 50%;
    }

</style>

</style>

@if(session('success'))
<div class="alert alert-success" role="alert">
    {{ session('success') }}
</div>
@endif
<div class="ct-banner pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="container-img">
                    <ul class="thumb">
                        <?php $i=1?>
                        @foreach ($detail_photo as $value)

                        @foreach ($value->img as $value_img)
                        @if($i<=3) <li><a href="../../public/uploads/detail_photo/{{$value_img}}" target="imgBox"> <img
                                    src="../../public/uploads/detail_photo/{{$value_img}}" alt=""> </a></li>
                            <?php $i++?>
                            @endif
                            @endforeach

                            @endforeach
                    </ul>
                    <div class="imgBox">
                        <img src="../../public/uploads/img_product/{{$product->img}}" alt="" srcset="">
                    </div>

                </div>
            </div>
            <div class="col-md-6">

                <div class="detail mt-5">
                    <h1><b>{{$product->name}}</b></h1>

                </div>
                @if($product->like!=0)
                @for($i=1; $i<= $product->like; $i++)
                <i class="fa fa-star fa-1x" style="color:red" data-index="0"></i>
                @endfor
                ({{$product->like}} điểm)
                @else
                <i>(Sản phẩm chưa được đánh giá)</i>
                @endif
                <form action="{{route('getCart')}}" method="post">
                    @csrf
                    <div class="detail-content mt-3">
                        <h3>
                            @if($product->promotion >0)
                            <span class="price-product--cost">
                                <strike>{{number_format($product->price,0,'.',',')}}</strike> VNĐ
                            </span>
                            <span class="price-product--sale">
                                @if($product->promotion >0)
                                -
                                {{number_format($product->price - ($product->price*($product->promotion/100)),0,'.',',')}}
                                VNĐ
                                @endif
                            </span>
                            @else
                            <span class="price-product--cost">
                                {{number_format($product->price,0,'.',',')}}
                            </span>
                            @endif
                        </h3>
                        <h4>{{ $product->description }}</h4>
                    </div>
                    <div>
                        @if($product->qty_product<=0)
                        <strike>
                            <h3 style="color:red">Đã hết hàng</h3>
                        </strike>
                        <button class="btn btn-success">Nhận thông báo khi có hàng</button>
                        @else
                        <select name="quantity" value="" id="" style="width:50px">
                            @for($i=1; $i<=$product->qty_product; $i++)
                                <option value="{{$i}}" name="">{{$i}}</option>

                                @endfor
                        </select>

                        <script src="">
                            $('select[name=selector] option').filter(':selected').val()

                        </script>
                        </br>
                        <div class="detail-button">
                            <button class="btn-1" type="submit">Mua ngay</button>
                            <a class="btn-2" id="{{$product->id}}" onclick="return add_cart(this);">Thêm vào giỏ</a>
                            @if(Auth::check())
                            <a style="margin-left:10px" id="{{$product->id}}" class="btn-3"
                                onclick="return add_mylove(this);"><i class="fa fa-heart"></i></a>
                            @endif
                        </div>

                        @endif
                        {{-- <span class="product-quantity-minus"></span>
                        <input type="" value="1" name="quantity" />
                        <span class="product-quantity-plus"></span> --}}
                    </div>
                    <input type="hidden" value="{{$product->id}}" name="productId">
                    <input class="price" id="{{($product->price)-($product->price*$product->promotion/100)}}"
                        type="hidden" value="{{($product->price)-($product->price*$product->promotion/100)}}"
                        name="price">
                    @foreach($tag as $t)
                        <a href=""><u>#{{$t->name}}</u></a>
                    @endforeach
                </form>

               
            </div>

        </div>

    </div>
</div>
<hr>
<div class="container">
    {!!$product->detail!!}
    <div class="fb-like" data-href="{{$url}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div>
    <div class="fb-share-button" data-href="http://a4ab932b9195.ngrok.io/shop-mart/public/xem-blog/11" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fa4ab932b9195.ngrok.io%2Fshop-mart%2Fpublic%2Fxem-blog%2F11&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
    <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text={{$url}}">Tweet</a>
    </div>
</div>

<div class="container">
    <div class="ct-info">
        <div class="tabs mt-5">
            <input id="tab1" type="radio" name="grp" checked="checked" />
            <label for="tab1"><span>Comment</span></label>
            <div class="detail mt-5">
                <div class="fb-comments" data-href="{{$url}}" data-numposts="10" data-width=""></div>
            </div>
                    
            <input id="tab2" type="radio" name="grp" />
            <label for="tab2"><span>Bình luận ({{count($comment)}})</span></label>
            <!-- Binh Luan -->
            <div>
                @if (count($comment) == 0)
                <h5>Chưa có bình luận</h5>
                @else
                @foreach($comment as $com)
                <div class="container" id="mess">
                    <img src="../../public/uploads/img_user/{{$com->users->img}}" id="img" alt="Avatar"
                        style="width:100%;">
                    <p>{{$com->content}}</p>
                    <span class="time-right">{{$com->users->name}}</span>

                    <span class="time-right" style="float:right">{{$com->created_at}}</span>
                </div>
                @endforeach
                @endif
            </div>

            <input id="tab3" type="radio" name="grp" />
            <label for="tab3"><span>Đánh giá</span></label>
            <div class="detail mt-5">
                <form action="{{route('postComment')}}" method="post">
                    @csrf
                    <h1>{{$product->name}}</h1>
                    <i class="fa fa-star fa-2x rating" data-index="0"></i>
                    <i class="fa fa-star fa-2x rating" data-index="1"></i>
                    <i class="fa fa-star fa-2x rating" data-index="2"></i>
                    <i class="fa fa-star fa-2x rating" data-index="3"></i>
                    <i class="fa fa-star fa-2x rating" data-index="4"></i>
                    <input type="hidden" class="rate" name="rate"></input>
                    <input type="hidden" class="id_product" name="id_product" value="{{$product->id}}">
                    <hr>
                    <br>
                    <textarea name="content" id="" cols="100%" rows="10"></textarea>
                    <br>
                    <br>
                    <button type="submit" class="btn btn-danger btn-lg">Gửi Đánh Giá</button>

                </form>
            </div>
             {{-- Comment --}}
               
                
                
            <input id="tab4" type="radio" name="grp" />
        </div>
    </div>
</div>
<script src="assets/js/jquery-3.5.1.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css" />
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css" />
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />
<script>
    function add_mylove(e) {
        var id = $(e).attr('id');

        $.ajax({
            url: '{{route('add_love')}}',
            type: 'GET',
            data: {
                "id": id,
            },
            success: function (data) {
                alertify.success(data);

            }
        })
    };

</script>
<script>
    var ratedIndex = -1;
    $(document).ready(function () {
        resetStarColor();
        if (localStorage.getItem('ratedIndex') != null) {
            setStars(parseInt(localStorage.getItem('ratedIndex')));

        }
        $('.rating').on('click', function () {
            ratedIndex = parseInt($(this).data('index'));
            localStorage.setItem('ratedIndex', ratedIndex);
            document.querySelector('.rate').value = ratedIndex + 1
        });
        $('.rating').mouseover(function () {
            resetStarColor();
            var currentIndex = parseInt($(this).data('index'));

            setStars(currentIndex);
        });
        $('.rating').mouseleave(function () {
            resetStarColor();
            if (ratedIndex != -1)
                setStars(ratedIndex);
        });

        function setStars(max) {
            for (var i = 0; i <= max; i++) {
                $('.rating:eq(' + i + ')').css('color', 'yellow')
            }
        }

        function resetStarColor() {
            $('.rating').css('color', '#c1bebe');
        }

    });

    function add_cart(e) {
        var id = $(e).attr('id');
        var price = $('.price').attr('id');

        $.ajax({
            url: '{{route('themCart')}}',
            type: 'GET',
            data: {
                "id": id,
                "price": price,
            },
            success: function (data) {
                alertify.success('Thêm 1 sản phẩm vào giỏ');
                $('#count').html(data);
            }
        });

    }

</script>


@endsection('content')
