@extends('client.master')
@section('title','Blog')
@section('content')


<style>
    #mess {
        border: 2px solid #dedede;
        background-color: #f1f1f1;
        border-radius: 5px;
        padding: 10px;
        margin: 10px 0;
    }
    #img {
        float: left;
        max-width: 60px;
        width: 100%;
        margin-right: 20px;
        border-radius: 50%;
    }
</style><head>
    @foreach($data as $commen)
        <meta property="og:title" content="{{$commen->title}}" />
        <meta property="og:description" content="{{$commen->detail}}" />
        <meta property="og:site_name" content="http://ef5739b7e91d.ngrok.io/shop-mart/public/" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="{{$url}}" />
        <meta property="og:image" content="../../public/uploads/img_blog/{{$commen->img}}}}" />
    @endforeach
</head>

@if(session('success'))
<div class="alert alert-success" role="alert">
    {{ session('success') }}
</div>
@endif
<div class="container" style="margin-top: 50px">
    <div class="row">
        <!-- left layout -->
        <div class="col-md-4 sidebar left-layout">
                            
            <h3 class="sidebar__title pt-20">
            <b> Những bài viết của chúng tôi</b>
            </h3>
            <br>
            <br>
            <br>
            <br>
            <ul class="ct__blog--box">
                <?php $i=1; ?>
                @foreach($blog as $b)
                <li class="ct__blog--item">
                    <a href="{{route('getDetailBlog',$b->id)}}" class="ct__blog--link" >
                        {{$i}}. {{$b->title}}
                        <?php $i++; ?>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        @foreach($data as $b)
        <div class="col-md-8">
            <div class="blog__box" style="">
                <img src="../../public/uploads/img_blog/{{$b->img}}" width="100px" height="100px"
                    class="img-fluid blog__img" alt="">
                <div class="entry-category">
                </div>
                <h3 class="entry-title">
                    {{$b->title}}
                </h3>
                
                <ul class="entry-meta-list">
                    <li class="entry-date">
                        <a href="" class="entry-icon">
                            <img src="assets/img/icon-calender.png" class="entry-img" alt="">
                        </a>
                        {{$b->created_at}}
                    </li>
                    <li class="entry-cmt">
                        <!-- <a href="" class="entry-icon">
                                    <img src="assets/img/commetn-calender.png" class="entry-img" alt="">
                                </a>
                                2 -->
                    </li>
                </ul>
                <p class="entry-description">
                    {!!$b->detail!!}
                </p>
            </div>
        </div>
        @endforeach
    </div>
</div>


@foreach($data as $b)
<div class="container">
    <!-- right layout -->
    <div class="col-md-8 article right-layout">
        
        <div class="fb-like" data-href="{{$url}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div>
        <div class="fb-share-button" data-href="http://a4ab932b9195.ngrok.io/shop-mart/public/xem-blog/11" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fa4ab932b9195.ngrok.io%2Fshop-mart%2Fpublic%2Fxem-blog%2F11&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
        <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text={{$url}}">Tweet</a>
        </div>
    @endforeach
    {{-- Messenger --}}
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    

    <!-- Your Chat Plugin code -->
    
    {{-- EndMessenger --}}
    <div class="tabs mt-5">
        <input id="tab1" type="radio" name="grp" checked="checked" />
        <label for="tab1"><span>Bình luận</span></label>
        <div>
            @if (count($comment) == 0)
            <h5>Chưa có bình luận</h5>
            @else
            @foreach($comment as $com)
            <div class="container" id ="mess">
                <img src="../../public/uploads/img_user/unnamed.jpg" id="img" alt="Avatar" style="width:100%;">
                <p></p>
            <span class="time-right">{{$com->content}}</span>
                
            <span class="time-right" style="float:right">{{$com->created_at}}</span>
            </div>
            @endforeach
            @endif
        </div>
        <input id="tab2" type="radio" name="grp" />
        <label for="tab2"><span>Gửi bình luận </span></label>
        <!-- Binh Luan -->
        <div class="ct-info-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="detail mt-5">
                        <form action="{{route('postcomment_blog')}}" method="post">
                        @csrf
                            <hr>   
                            <br>
                                <textarea name="content" id="" cols="100%" rows="10"></textarea>   
                                <input type="hidden" name="id" value="{{$id}}" >
                            <br>
                            <br>
                                <button type="submit" class="btn btn-danger btn-lg">Gửi bình luận</button>
                        
                        </form>
                    </div>
                   
                </div>
               
            </div>
        </div>
        {{-- Comment --}}
        @foreach($data as $commen)
        <input id="tab3" type="radio" name="grp" />
            <label for="tab3"><span>Comment</span></label>
            <div class="detail mt-5">
            <div class="fb-comments" data-href="{{$url}}" data-numposts="10" data-width=""></div>
            </div>
        @endforeach
    </div>
</div>
    
    @endsection('content')
    