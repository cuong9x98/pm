@extends('client.master')
@section('title','Home')
@section('content')
<!-- content web -->
<div class="container">

        <div class="row mt-20">
            <!-- left layout -->
            <div class="col-md-4 sidebar left-layout">
                <h1 class="sidebar__headding">
                    Tin tức mới
                </h1>
                <div class="search-blog--box">
                    <input type="search" name="" id="" class="form__search" placeholder="Bạn muốn tìm gì ... ?">
                    <button type="submit" class="btn--blog">
                        <i class="fa fa-search btn--blog--icon"></i>
                    </button>
                </div>
                <h3 class="sidebar__title pt-20">
                    Danh mục tin tức
                </h3>
                <ul class="ct__blog--box">
                    <?php $i=1; ?>
                    @foreach($blog as $b)
                    <li class="ct__blog--item">
                        <a href="{{route('getDetailBlog',['id'=>$b->id])}}" class="ct__blog--link" >
                            {{$i}}. {{$b->title}}
                            <?php $i++; ?>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="text-right">
                
            </div>

            <!-- right layout -->
            <div class="col-md-8 article right-layout">
                @foreach($blog as $b)
                <div class="blog__box" style="">
                    <img src="../../public/uploads/img_blog/{{$b->img}}" width="300px" height="300px" class="img-fluid blog__img" alt="">
                    <div class="entry-category">

                    </div>
                    <h3 class="entry-title">
                        {{$b->title}}
                    </h3>
                    <ul class="entry-meta-list">
                        <li class="entry-date">
                            <a href="" class="entry-icon">
                                <img src="assets/img/icon-calender.png" class="entry-img" alt="">

                            </a>
                            {{$b->created_at}}
                        </li>
                        <li class="entry-cmt">
                            <!-- <a href="" class="entry-icon">
                                <img src="assets/img/commetn-calender.png" class="entry-img" alt="">
                            </a>
                            2 -->
                        </li>
                    </ul>
                    <p class="entry-description">
                        
                    </p>
                    <a href="{{route('getDetailBlog',['id'=>$b->id])}}" class="btn entry-readmore">
                        Đọc tiếp
                    </a>
                </div>
                @endforeach
            </div>
            
            

        </div>
    
</div>
<!-- footer -->
<!-- add 'footer' snippet in css -->

@endsection('content')
