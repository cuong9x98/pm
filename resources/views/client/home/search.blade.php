@extends('client.master')
@section('title','Home')


@section('css')
<link rel="stylesheet" href="slick/slick/slick.css">
<link rel="stylesheet" href="slick/slick/slick-theme.css">
<style type="text/css">
    .avatar_cate {
        max-width: 100%;
        height: 150px;
        width: 170px;
        padding: 10px;
    }

</style>
@endsection('css')
@section('content')

<!-- Product -->
<div class="container ctvp">
    <h3 class="ctvp__title">
        @if(empty($data_tag))
            Kết quả tìm kiếm: {{count($data)}} sản phẩm
        @else
            Kết quả tìm kiếm: {{count($data)+count($data_tag)}} sản phẩm
        @endif
       
    </h3>
    <hr>
    <div class="ctvp__box">
        <div class="carousel-inner ctvp__box__slide">
            <div class="carousel-item active">
                <div class=" row__item__product" style="">
                    @if(empty($data_tag))
                    @else
                    @foreach($data_tag as $tag)
                    <a href="{{ route('getDetailproduct',$tag->id) }}">
                        <div class=" item__product">
                            <span class="onsale">
                                <span class="saled">
                                    @if($tag->promotion >0)
                                    giảm giá {{$tag->promotion}} %
                                    @endif
                                </span>
                                <br />
                                <span class="featured">
                                    @if($tag->buy >100)
                                    {{$tag->buy}}
                                    @endif
                                </span>
                            </span>
                            <!-- group button on hover -->
                            <div class="group-button">
                                <div class="add-cart">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </div>
                                <div class="btn-wishliss">
                                    <i class="fa fa-heart group-button--icon"></i>
                                </div>
                                <div class="quick-view">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </div>
                            </div>
                            <!-- caption -->
                            <div class="caption">
                                <h3 class="name-product">{{$tag->name}}</h3>
                                <div class="price-product">
                                    @if($tag->promotion >0)
                                    <span class="price-product--cost">
                                        <strike>{{number_format($tag->price,0,'.',',')}}</strike> VNĐ
                                    </span>
                                    <span class="price-product--sale">
                                        @if($tag->promotion >0)
                                        -
                                        {{number_format($tag->price - ($tag->price*($tag->promotion/100)),0,'.',',')}}
                                        VNĐ
                                        @endif
                                    </span>
                                    @else
                                    <span class="price-product--cost">
                                        {{number_format($tag->price,0,'.',',')}}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <img src="../../public/uploads/img_product/{{$tag->img}}" class="product__img"
                                alt="" />
                        </div>
                    </a>
                    @endforeach
                    @endif


                    @foreach($data as $tag)
                    <a href="{{ route('getDetailproduct',$tag->id) }}">
                        <div class=" item__product">
                            <span class="onsale">
                                <span class="saled">
                                    @if($tag->promotion >0)
                                    giảm giá {{$tag->promotion}} %
                                    @endif
                                </span>
                                <br />
                                <span class="featured">
                                    @if($tag->buy >100)
                                    {{$tag->buy}}
                                    @endif
                                </span>
                            </span>
                            <!-- group button on hover -->
                            <div class="group-button">
                                <div class="add-cart">
                                    <i class="fa fa-cart-plus group-button--icon"></i>
                                </div>
                                <div class="btn-wishliss">
                                    <i class="fa fa-heart group-button--icon"></i>
                                </div>
                                <div class="quick-view">
                                    <i class="fa fa-eye group-button--icon"></i>
                                </div>
                            </div>
                            <!-- caption -->
                            <div class="caption">
                                <h3 class="name-product">{{$tag->name}}</h3>
                                <div class="price-product">
                                    @if($tag->promotion >0)
                                    <span class="price-product--cost">
                                        <strike>{{number_format($tag->price,0,'.',',')}}</strike> VNĐ
                                    </span>
                                    <span class="price-product--sale">
                                        @if($tag->promotion >0)
                                        -
                                        {{number_format($tag->price - ($tag->price*($tag->promotion/100)),0,'.',',')}}
                                        VNĐ
                                        @endif
                                    </span>
                                    @else
                                    <span class="price-product--cost">
                                        {{number_format($tag->price,0,'.',',')}}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <img src="../../public/uploads/img_product/{{$tag->img}}" class="product__img"
                                alt="" />
                        </div>
                    </a>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')
