
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <base href="{{asset('shop')}}/">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- link BS4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous" />
    <!-- link fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script type="text/javascript" src="ckeditor/ckeditor.js"></script>

    <!-- My css -->
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/css/reset.css" />
    <link rel="stylesheet" href="assets/css/reponsive-menu.css">
    <link rel="stylesheet" href="assets/css/style-k.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style-k.css">
    <link rel="stylesheet" href="assets/css/img-product-detail.css">
    
     

    @yield('css')
    <title>@yield('title')</title>
    <style type="text/css">
    li.ct__blog--item:hover a.ct__blog--link{
        color:red;
        font-weight:900
    }
        li.mega__menu--item {
            font-weight: 700;
        }

        a#category:hover {
            color: white
        }


        .product-quantity>* {
            width: 36.9px;
        }

        .product-quantity>input[type="number"]::-webkit-inner-spin-button,
        .product-quantity>input[type="number"]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .product-quantity>input {
            border: none;
            text-align: center;
            font-size: 12px;
        }

        .product-quantity {
            display: inline-flex;
            border: 1px solid #e6e6e6;
            align-items: center;
            height: 40px;
            border-radius: 4px;
        }

        .product-quantity-plus:before,
        .product-quantity-minus:before {
            width: 11px;
            display: block;
            margin: 0 auto;
        }

        .product-quantity-plus:before {
            content: "+";
        }

        .product-quantity-minus:before {
            content: "-";
        }

        .product-quantity-plus,
        .product-quantity-minus {
            cursor: pointer;
        }

        .product-quantity-plus:before,
        .product-quantity-minus:before {
            width: 11px;
            display: block;
            margin: 0 auto;
        }

    </style>
</head>
<?php 

    $count = Cart::count();  

?>
<body>
    <div class="container-fluid top__bar--box">
        <div class="container">
            <div class="row top__bar">
                <div class="col-md-4 top__bar-left">
                    <p class="top__bar-left--phone">
                        <i class="fa fa-phone icon__phone"></i>
                        <span class="top__bar-left--text">
                            @foreach($phone as $value)
                            Hotline:{{$value->so_dien_thoai}}
                            @endforeach
                        </span>
                        
                    </p>
                </div>
                <div class="col-md-4 top__bar-middle">
                    <p class="top__bar-middle--email">
                        @foreach($email as $value_email)
                            @if($value_email->status == 1)
                                Email: <a>{{$value_email->email}}</a><br>
                            @endif
                        @endforeach
                    </p>
                </div>
                <div class="col-md-4 top__bar-right">
                    <div class="top__bar-right--social">
                        <a href="https://www.facebook.com/Test-ch%E1%BB%A9c-n%C4%83ng-chat-FB-102305811558577/?modal=admin_todo_tour"><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-instagram"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                    </div>
                    <div class="top__bar-right--language">
                        <img src="assets/img/en.png" class="img-fluid img__language" alt="USA" />
                        <span>
                            English
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header-main -->
    <div class="container header-main">
        <div class="row header-main">
            <!-- logo -->
            <div class="col-md-3 header__logo">
                <a href="{{route('getHome')}}" class="header__logo--link">
                    <img src="assets/img/90e338a0349acec4978b.jpg" class="img-fluid header__logo--img" alt="logo" />
                </a>
            </div>
            <!-- box search -->
            <div class="col-md-6 header__search">
                <form action="{{route('postsearch')}}" method="post" autocomplete="off">
                
                
                    @csrf
                    <div class="box__search">
                        <input style="width:300px" type="text" class="import__search" name="key"
                            placeholder="Bạn muốn tìm gì ?" />
                        <button type="submit" class="submit__search">
                            <i class="fa fa-search submit__search--icon"></i>
                        </button>
                    </div>
                </form>

            </div>
            <div class="col-md-3 header__right">
                @if(Auth::check())
                <a onclick="notifi(this)" class="top-wishlist">
                    @if(Auth::User()->unreadnotifications->count() > 0)
                        <i class="fa fa-bell header__right--icon">{{Auth::User()->unreadnotifications->count()}}</i>     
                    @else
                        <i class="fa fa-bell header__right--icon"></i>     
                    @endif          
                </a>
                <div id="notifi" style="margin-top: 10px;display: none;position: absolute;z-index: 9999;left: -20px; width: 500px; height: auto;position: absolute; z-index: 9999; background-color: white">
                    <div style="width: 100%; background-color: #d1cdcd; padding:7px; color: black; font-weight: 800 ">
                        Bạn có {{Auth::User()->unreadnotifications->count()}} thông báo mới
                        <div id="close" style="position: absolute;display: inline-block;right: 0;width: 30px;top: -10px;right: -10px;height: 30px;border-radius: 30px;text-align: center;line-height: 30px;background-color: red;color: white;">X</div>
                    </div>
                    <ul style="min-height: 50px">
                        <li style="padding: 10px"><a href="#">
                            @foreach(Auth::User()->notifications as $value)
                            @if($value->read_at == null)
                            <a class="notifi click" href="{{route('read_notifi_client',['id'=>$value->id])}}"><span style="color: red;" id="{{$value->id}}">{{$value->data['letter']['title']}}</span><br></a>
                            @else
                            <a class="notifi">{{$value->data['letter']['title']}}<br></a>
                            @endif
                            @endforeach
                            <div class="clearfix"></div>
                        </a></li>

                    </ul>
                    <div style="width: 100%; background-color: #d1cdcd; padding:7px; color: black; font-weight: 800; text-align: center;">
                        <a href="{{route('read_all')}}">Đọc tất cả</a>
                    </div>

                </div>
                <a href="{{route('showCart')}}" class="top-cart">
                    <i class="fa fa-shopping-cart header__right--icon"></i>
                    <span class="header__right--number2" id='count-item'>{{$count}}</span>
                    <p class="top-cart--info">
                        Giỏ hàng
                    </p>
                </a>
                @else
                <a href="{{route('showCart')}}" class="top-cart">
                    <i class="fa fa-shopping-cart header__right--icon" style="margin-left: 40px"></i>
                    <span class="header__right--number2" id="count">{{$count}}</span>
                    <p class="top-cart--info">
                        Giỏ hàng
                    </p>
                </a>
                @endif
            </div>
        </div>
    </div>
    <!-- menu -->
    <div class="container-fluid main-menu">
        <!-- main menu -->
        <div class="container main-menu">
            <div class="row">
                <ul class="top__menu col-md-12">
                    <li class="menu__item">
                        <a href="{{route('getHome')}}" class="menu__link menu__link--home"> Trang chủ</a>
                    </li>
                    <li class="menu__item">
                        <a id="category" class="menu__link"> Danh mục phần mềm </a>
                        <ul class="mega__menu" >
                            <div class="row row__mega__menu" >
                                <?php $i = 1?>
                                @foreach($category as $value_cate)
                                <div class="col-md-4 column__mega__menu">
                                        <li class="mega__menu--item">
                                            <a href="{{route('getCate',['id'=>$value_cate->id])}}"
                                                class="mega__menu--link">{{$value_cate->name}}</a>
                                        </li>
                                        <?php $i++?>
                                </div>
                                @endforeach
                            </div>
                        </ul>
                    </li>
                    <li class="menu__item">
                        <a href="{{route('getBlog')}}" class="menu__link"> Tin tức </a>
                    </li>
                    <li class="menu__item">
                        <a href="{{route('getPolicy')}}" class="menu__link"> Chính sách </a>
                        <ul class="sub__menu ">
                            <li class="sub__menu--link">
                                <a href="" class="sub__menu--link">
                                    Bảo hành
                                </a>
                            </li>
                            <li class="sub__menu--link">
                                <a href="" class="sub__menu--link">
                                    Cài đặt
                                </a>
                            </li>
                            <li class="sub__menu--link">
                                <a href="" class="sub__menu--link">
                                    Nâng cấp
                                </a>
                            </li>
                            <li class="sub__menu--link">
                                <a href="" class="sub__menu--link">
                                    Gia hạn
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="menu__item">
                        <a href="{{route('getContact')}}" class="menu__link"> Liên hệ </a>
                    </li>
                    @if(Auth::check())
                    <li class="menu__item">
                        <a id="category" class="menu__link"> Xin Chào : {{Auth()->user()->name}} </a>
                        <ul class="mega__menu">
                            <div class="row row__mega__menu">
                                <div class="col-md-4 column__mega__menu">

                                    {{-- <li class="mega__menu--item">
                                        <a href="{{route('myorder')}}" class="mega__menu--link">Sản phẩm đã mua</a>
                                    </li> --}}
                                    <li class="mega__menu--item">
                                        <a href="{{route('mylove',['id'=>Auth::User()->id])}}"
                                            class="mega__menu--link">Sản phẩm yêu thích</a>
                                    </li>
                                    <li class="mega__menu--item">
                                        <a href="{{route('getLogoutUser')}}" class="mega__menu--link">Đăng Xuất</a>
                                    </li>
                                </div>
                            </div>
                        </ul>
                    </li>

                    @else
                    <li class="menu__item"> <a href="{{route('getLoginUser')}}" class="menu__link"> Đăng Nhập </a></li>
                    @endif
                </ul>
            </div>

        </div>
    </div>
    </div>

    <!-- responsive menu -->
    <div class="menu-mobile clearfix">
        <button class="openbtn" onclick="openNav()">☰</button>
        <div class="menu-mobile-img">
            <img src="assets/img/90e338a0349acec4978b.jpg">
        </div>
        <div class="menu-mobile-input">
            <button><i class="fa fa-search"></i></button>

            <button><i class="fa fa-shopping-cart"></i></button>
            <span class="shop-span">10</span>
        </div>
    </div>

    <div id="mySidepanel" class="sidepanel">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
        <a href="index.html">Trang chủ</a>
        <button class="dropdown-btn">Danh mục
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-container">
            <a href="#">Phần mềm 1</a>
            <a href="#">Phần mềm 2</a>
            <a href="#">Phần mềm 3</a>
        </div>
        <a href="{{route('getPolicy')}}">Tin tức</a>
        <button class="dropdown-btn">Chính sách
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-container">
            <a href="#">Bảo hành</a>
            <a href="#">Cài đặt</a>
            <a href="#">Nâng cấp</a>
            <a href="#">Gia hạn</a>
        </div>
        <a href="contact.html">Liên hệ</a>
        <button class="dropdown-btn">Ngôn Ngữ
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-container">
            <a href="#">Vie</a>
            <a href="#">Eng</a>
        </div>
    </div>


    <!-- CONTENT -->
    @yield('content')
    <!-- END CONTENT -->


    <!-- footer -->
    <!-- add 'footer' snippet in css -->
    <div class="">
        <div class="footer-v1 bg-img footer__wrap">
            <div class="footer no-margin">
                <div class="container">
                    <div class="row pt-40 pt-20 pl-3">
                        <div class="col-md-5 footer__column">
                            <div class="headline">
                                <h3 class="headline--title">
                                    About Us:
                                </h3>
                                <p class="headline--detail">
                                    Duis ut ornare elit. Proin sit amet rutrum nibh. Morbi pulvinar est quis ma iaculis
                                    malesuada. Fusce in aliquam erat. Suspendisse id mattis urnafas Ut euismod euismod
                                    lectus.
                                </p>
                            </div>
                            <ul class="list-unstyled link-list">
                                <li>
                                    @foreach($address as $value_address)
                                        @if($value_address->status == 1)
                                            <a>{{$value_address->tieu_de}} : {{$value_address->dia_chi}}</a><br>
                                        @endif
                                    @endforeach
                                </li>
                                <li>
                                    @foreach($phone as $value_phone)
                                        @if($value_phone->status == 1)
                                            <a>{{$value_phone->tieu_de}} : {{$value_phone->so_dien_thoai}}</a><br>
                                        @endif
                                    @endforeach
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/Test-ch%E1%BB%A9c-n%C4%83ng-chat-FB-102305811558577/?modal=admin_todo_tour"><i class="fa fa-facebook footer__social--icon"></i></a>
                                    <a href=""><i class="fa fa-instagram footer__social--icon"></i></a>
                                    <a href=""><i class="fa fa-twitter-square footer__social--icon"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-6 footer__column">
                            <div class="headline">
                                <h3 class="headline--title">
                                    Chính sách
                                </h3>
                            </div>
                            <ul class="list-unstyled link-list">
                                <li><a href="#">Bảo hành</a></li>
                                <li><a href="">Cài đặt</a></li>
                                <li><a href="">Nâng cấp</a></li>
                                <li><a href="">Gia hạn</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-6 footer__column">
                            <div class="headline">
                                <h3 class="headline--title">
                                    Danh mục phần mềm
                                </h3>
                            </div>
                            <ul class="list-unstyled link-list">
                                @foreach($category as $value_cate)
                                <li class="mega__menu--item">
                                    <a href="{{route('getCate',['id'=>$value_cate->id])}}"
                                        class="mega__menu--link">{{$value_cate->name}}</a>
                                </li>
                                @endforeach
                                <a href="{{route('getCate',['id'=>$value_cate->id])}}"
                                    class="footer__readmore--category">
                                    Xem tất cả <i class="fa fa-chevron-right"></i></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer bottom -->
            <div class="container-fluid footer__bottom-hr">
                <div class="container">
                    <div class="row footer__bottom">
                        <div class="col-md-6 pt-20">
                            <p class="footer__coppyright">
                                Copyright © 2020 .
                            </p>
                        </div>
                        <div class="col-md-6 footer__flow pt-20">
                            <div class="footer__flow--bell">
                                <p class="footer__flow--text">
                                    Đăng ký để nhận thông tin.
                                </p>
                            </div>
                            <div class="footer__flow--box">
                                <input type="text" class="footer__flow--form" placeholder="Nhập Email của bạn ..." />
                                <button type="submit" class="footer__flow--btn">
                                    <i class="fa fa-envelope footer__flow--icon"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="assets/js/reponsive-menu.js"></script>
    <script src="slick/slick/slick.js"></script>


    <script type="text/javascript">
        $('.row__item__product').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            autoplay: true,
            dots: true,
            autoplaySpeed: 2000,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]

        });

    </script>

    <script type="text/javascript">
        $('.row__item__blog').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }

            ]
        });
        (function incrementDecrementInput() {
            var increment = document.querySelector(".product-quantity-plus");
            var decrement = document.querySelector(".product-quantity-minus");
            var input = document.querySelector(".product-quantity > input");

            // increment.addEventListener("click", function () {
            //     input.value++;
            // });

            // decrement.addEventListener("click", function () {
            //     if (input.value > 1) {
            //         input.value--;
            //     }
            // });

        })();

        }

    </script>
    
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>

    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.update_cart').click(function () {

            });


        });
        function notifi(e){
            $("div#notifi").css("display","block");
        }
        $(document).ready(function () {
            $("div#close").click(function(){
                $("div#notifi").css("display","none");  
            })
        });
        // function updateCart(e) {
        //     var row_id = $(e).attr('id');
        //     var qty = $(e).parent().find('.qty').val(); //prarent nhảy ra ngoài thẻ
        //     var token = $("input[name='_token']");
        //     $.ajax({
        //         url: '{{route('editCart')}}',
        //         type: 'GET',
        //         cache: false,
        //         data: {
        //             "id": row_id,
        //             "qty": qty,
        //         },
        //         success: function (data) {
        //             alertify.success('Cập nhật thành công');
        //             $(".row-cart").html(data['string']); 
        //             $('#count').html(data['count']);
        //             $('#subtotal').html(data['subtotal']);
                   
        //         }
        //     });
        // }
    </script>

 {{-- Tim kiem --}}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 
<script type="text/javascript">
    var path = "{{ route('autocomplete') }}";
    $('input.import__search').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data_name) {
                return process(data_name,data_name);
            });

        }

    });
</script>
{{-- Chat Zalo --}}
<div class="zalo-chat-widget" data-oaid="4568845614008004173" data-welcome-message="Của hàng Soft Mart xin chào quý khách.Chúng tôi có thể giúp gì cho bạn" data-autopopup="0" data-width="350" data-height="420"></div>
<script src="https://sp.zalo.me/plugins/sdk.js"></script>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v7.0'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="113065197148778">
    </div>
    
</body>

</html>
