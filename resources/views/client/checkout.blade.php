@extends('client.master')
@section('title','Home')
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" /><!-- Style-CSS -->
<link href="css/font-awesome.css" rel="stylesheet"><!-- font-awesome-icons -->

@section('content')
<section class="signin-form">
    <div class="container-fluid">
        <div class="wrapper">
            <div class="logo text-center top-bottom-gap">
            
            </div>
            <div class="form34">
                <h4 class="form-head">Xác nhận thông tin</h4>
               
                <div class="form-34or">
                    <span class="pros">
                        <span>or</span>
                    </span>
                </div>
                <form action="{{route('postPayment')}}" method="post" id="checkout">
                @csrf
                    <div class="">
                        <p class="text-head">Tên Khách</p>
                        <input type="text" name="name" class="input" value="" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Số điện thoại</p>
                        <input type="text" name="phone" class="input" value="" placeholder="" required="" />
                    </div>
                    <div class="">
                        <p class="text-head">Email</p>
                        <input type="text" name="email" class="input" value="" email="true" placeholder="" required="" />
                    </div>
                   
                    <div class="">
                        <p>Địa chỉ</p>
                        <select class="input" name="city" id="city" selected="get_val()" value="">
                        <option value="volvo">--Mời bạn chọn Tỉnh/Thành phố--</option>
                        @foreach($citys as $city)
                        <option  value="{{$city->id}}">{{$city->_name}}</option>                   
                        @endforeach
                    </select>
                    </div>

                     <div class="">
                       
                        <select class="input" name="town" id="town" >
                        <option value="volvo">--Mời bạn chọn Quận/Huyện--</option>
                        @foreach($citys as $city)
                        <option value="{{$city->id}}">{{$city->_name}}</option>
                        
                        @endforeach
                    </select>
                    </div>

                     <div class="">
                       
                        <select class="input" name="village" id="PhuongXa">
                        <option value="volvo">--Mời bạn chọn Phường/Xã--</option>
                        @foreach($citys as $city)
                        <option > </option>
                        @endforeach
                    </select>
                    </div> 
                    <div class="">
                        <input type="text" name="street" class="input" value="" placeholder="Địa chỉ" required="" />
                        
                    </div>
                    <input type="hidden" value="" id="get_city" placeholder="" name="get_city">
                    <input type="hidden" value="" id="get_town" placeholder="" name="get_town">
                    <input type="hidden" value="" id="get_village" placeholder="" name="get_village">
                    <button type="submit" class="btn btn-success btn-block">Tiếp tục</button>
                   
                </form>
            </div>
        </div>

    </div>
</section>
<script src="assets/js/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function(){
            $("#city").change(function(){
                var x = $('#city :selected').text();
                $("#get_city").val(x);
            });
            $("#town").change(function(){
                var x = $('#town :selected').text();
                $("#get_town").val(x);
            });
            $("#PhuongXa").change(function(){
                var x = $('#PhuongXa :selected').text();
                $("#get_village").val(x);
            })
        })
</script>
<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function () {
        $("select#city").change(function () {
            var id_TinhTP = $('select#city').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('post_quan_huyen')}}',
                type: "POST",
                cache: false,
                data: {
                    _token: CSRF_TOKEN,
                    id_TinhTP: id_TinhTP
                },
                success: function (data) {
                    $("select#town").empty();
                    $("select#town").append(data);
                },
                error: function (error) {
                    alert("Thêm thất bại");
                }
            });

        });

    });

</script>
<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function () {
        $("select#town").change(function () {
            var id_Quan = $('select#town').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('post_phuong_xa')}}',
                type: "POST",
                cache: false,
                data: {
                    _token: CSRF_TOKEN,
                    id_Quan: id_Quan
                },
                success: function (data) {
                    $("select#PhuongXa").empty();
                    $("select#PhuongXa").append(data);
                },
                error: function (error) {
                    alert("Thêm thất bại");
                }
            });

        });

    });

</script>


<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>


<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
// just for the demos, avoids form submit
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
$( "#checkout" ).validate({
  rules: {
    email: {
      required: true,
      email: true
    }
  },
  rules: {
    password: {
      required: true,
      minlength: 3,
    }
  },
  rules: {
    phone: {
      required: true,
      number: true,
      maxlength:10
    },
 
  }

  }
  
  

});
</script>

@endsection('content')
