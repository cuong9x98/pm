<?php

namespace App\Http\Controllers\admin\banner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use App\banner;
use DB;
class BannerController extends Controller
{
    // Hiển thị trang banner
	public function show(){
		$banner = banner::all();
		return view('admin.banner.index',['banner'=>$banner]);
	}
	public function add(Request $Request){
		$banner = new banner;
		if($Request->hasFile('files')){		
			if($Request->files_compare){
				$array_image = [];
				foreach ($Request->files_compare as $key=>$item){
					if(!is_null($item)){
						array_push($array_image,$key);
					}
				}
				foreach ($Request->file('files') as $key => $item){
					if(in_array($key, $array_image)){
						$file_name=$item->getClientOriginalName();
						DB::table('banner')->insert([
							'img'=>$file_name
						]);
						$item->move('uploads/banner/',$file_name);
					}
				}
			}
		}
		return redirect()->route('getBanner');
	}
	public function delete($id){
		$banner = banner::where('id',$id)->delete();
		return redirect()->route('getBanner'); 
	}

	public function selectBanner($id){
		$banner = banner::where('id',$id)->get();
		foreach ($banner as $value) {
			$status = $value->status;
		}
		if($status == 0){
			$banner = banner::where('id',$id)->update([
				'status'=>1
			]);
		}else{
			$banner = banner::where('id',$id)->update([
				'status'=>0
			]);
		}
		return redirect()->route('getBanner'); 
	}
}
