@extends('admin.master')
@section('title','Danh mục')
@section('content')
<!--heder end here-->   	
<div>
	<div class="container" style="margin-bottom: 50px ">
		<h3>Banner lớn</h3>
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<?php $i=0 ?>
				@foreach ($banner_big as $value_b)
				@if($value_b->check == 0 && $value_b->status == 1)
				@if($i==0)
				<div class="carousel-item active">
					<img src="../../public/uploads/banner/{{$value_b->img}}" style="height: 600px;"  class="d-block w-100" alt="...">
				</div>
				{{$i++}}
				@else
				<div class="carousel-item">
					<img src="../../public/uploads/banner/{{$value_b->img}}" style="height: 600px;"  class="d-block w-100" alt="...">
				</div>
				@endif
				@endif
				@endforeach
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div class="container" style="margin-bottom: 50px ">
		<h3>Banner nhỏ</h3>
		@if(session('thongbao'))
		<strong style="color: red; font-size: 25px">{{session('thongbao')}}</strong>
		@endif
		<div class="row">
			@foreach ($banner_small as $value_s)
			@if($value_s->check == 1 && $value_s->status == 1)
			<div id="banner_small" style="padding-top: 20px;height:auto" class="col-md-4">
				<img src="../../public/uploads/banner/{{$value_s->img}}" style="height: 230px;"  class="d-block w-100" alt="...">
			</div>
			@endif
			@endforeach		
		</div>
	</div>
	<div style="border: 1px solid black; height: 260px; margin: 0 0 30px 0; padding-left: 30px">
		<form method="post" enctype="multipart/form-data">
			@csrf
			<table>
				<tr>
					<td>
						<img id="add_img" src="../../public/uploads/Add_img.png" style="display: inline-block;float: left;">
					</td>
					<td rowspan="2">
						<input type="file" id="image_list" name="files[]" multiple />
					</td>
				</tr>
				<tr>
					<td>
						<select name="check">
							<option value="0">Banner lớn</option>
							<option value="1">Banner nhỏ</option>
						</select>
						<input style="margin-top: 40px" type="submit" name="add" value="Thêm ảnh">
					</td>
				</tr>
			</table>
			
			
			
		</form>	
	</div>
	<div>
		<div class="row">
			<div class="col-md-6" style="border: 1px solid black">
				<h3>Danh sách banner nhỏ</h3>
				@if(count($banner_small)>0)
				@foreach ($banner_small as $value_small)
				<div id="img_{{$value_small->id}}" class="col-md-3">
					@if($value_small->status == 1)
					<div id="confirm" style="width: 30px;height: 30px;z-index: 9999;position: absolute;background-color: green;color: white;font-weight: 900;text-align: center;border-radius: 30px;line-height: 30px;"><i class="fas fa-check"></i></div>
					@endif
					<input type="text" class="custom-file-input" id="{{ $value_small->id }}" name="files_compare[]" value_small="{{ $value_small->img }}" readonly style="display: none;"/>
					<span class="pip">
						<a href="{{route('selectBannerSmall',['id'=>$value_small->id])}}">
							<img class="imageThumb" style="width:225px ; height:100px;" src="../../public/uploads/banner/{{$value_small->img}}" title="file.name"/>
						</a>
						<br/>
						<a href="{{route('deleteBanner',['id'=>$value_small->id])}}"><span class="remove" id="{{$value_small->id}}" onclick="document.getElementById('{{ $value_small->id }}').value_small = '' " >Xóa</span></a>
					</span>
				</div>
				@endforeach
				@else
				<p>ko có banner</p>
				@endif
			</div>
			<div class="col-md-6" style="border: 1px solid black">
				<h3>Danh sách banner lớn</h3>
				@if(count($banner_big)>0)
				@foreach ($banner_big as $value_big)
				<div id="img_{{$value_big->id}}" class="col-md-3">
					@if($value_big->status == 1)
					<div id="confirm" style="width: 30px;height: 30px;z-index: 9999;position: absolute;background-color: green;color: white;font-weight: 900;text-align: center;border-radius: 30px;line-height: 30px;"><i class="fas fa-check"></i></div>
					@endif
					<input type="text" class="custom-file-input" id="{{ $value_big->id }}" name="files_compare[]" value_big="{{ $value_big->img }}" readonly style="display: none;"/>
					<span class="pip">
						<a href="{{route('selectBanner',['id'=>$value_big->id])}}">
							<img class="imageThumb" style="width:225px ; height:100px;" src="../../public/uploads/banner/{{$value_big->img}}" title="file.name"/>
						</a>
						<br/>
						<a href="{{route('deleteBanner',['id'=>$value_big->id])}}"><span class="remove" id="{{$value_big->id}}" onclick="document.getElementById('{{ $value_big->id }}').value_big = '' " >Xóa</span></a>
					</span>
				</div>
				@endforeach
				@else
				<p>ko có banner</p>
				@endif
			</div>
		</div>	
	</div>
</div>
@endsection('content')
