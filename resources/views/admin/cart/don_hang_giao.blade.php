@extends('admin.master')
@section('css')
<style type="text/css" media="screen">
	table#tbl_order{
		width: 99%;
		margin-top: 40px;
	}
	table#tbl_order td{
		text-align: center;
		padding: 10px;
	}
	div.status{
		width: 190px;
		text-align: center;
		padding: 12px;
		margin: auto;
		background-color: #fd8c8c;
		color: white;
		border-radius: 10px;
	}
</style>
@endsection('css')
@section('content')				
<div class="link">
	<!-- HIển thị đơn hàng mới để admin chuyển cho sale duyệt -->
	Đơn hàng 
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px;padding-left: 15px; border-radius: 4px;padding-bottom: 50px; ">
	<a href="{{ route('getListOrder',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Đơn hàng mới</button></a>
	<a href="{{ route('don_hang_giao',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Đơn hàng đã giao</button></a>
	<!-- Hiển thị chi tiết đơn hàng -->
	<div class="tbl_order_detail" style="width: 99%;height:300px;border: 1px solid black;display: none">
		<div onclick="exit()" style="border: 1px solid;height: 50px;padding: 8px 10px 10px 10px;float: right;font-size: 25px;font-weight: 900;background-color: red;color: white;"><i class="fas fa-times"></i></div>
		<h3 id="title">Chi tiết đơn hàng <i><span style="color: red" id="name_order"></span></i></h3>
		<table id="tbl_order" border="1" style="width: 98%;margin-left: 1%;">
			<thead>
				<tr>
					<td class="title">STT</td>
					<td class="title">Tên mặt hàng</td>
					<td class="title">Số lượng</td>						
					<td class="title">Thành tiền</td>
					<td class="title">% hoa hồng cho sale</td>
					<td class="title">Tiền sale</td>
				</tr>
			</thead>
			<tbody id="detail_order">

			</tbody>
		</table>
	</div>
	<!-- end hiển thị chi tiết đơn hàng -->

	<h3 id="title">Đơn hàng đã giao</h3>
	<table id="tbl_order" border="1">
		<thead>
			<tr>
				<td class="title">STT</td>
				<td class="title">Tên đơn hàng</td>
				<td class="title">Người đặt</td>
				<td class="title">Số điện thoại</td>
				<td class="title">Địa chỉ người nhận</td>
				<td class="title">Số lượng</td>
				<td class="title">Thành tiền</td>
				<td class="title">Tiền hoa hồng</td>
				<td class="title">Ngày đặt</td>
				<td class="title">Ngày chốt</td>
				<td class="title">Sale xử lí</td>
				<td class="title">Trạng thái</td>
			</tr>
		</thead>
		<tbody>
			@php$i=1@endphp
			@if(count($order) ==0 )
			<td colspan="13">Không có đơn hàng nào</td>	
			@else
			@foreach($data as $value)
			<tr>
				<td>1</td>
				<td id="name_order_{{$value['id']}}"><span onclick="showOrderDetail(this)" id="{{$value['id']}}">{{$value['TenĐH']}}</span></td> <!-- Xem chi tiết đơn hàng -->
				<td>{{$value['name']}}</td>
				<td>{{$value['phone']}}</td>
				<td>{{$value['address']}} - {{$value['XaPhuong']}} - {{$value['QuanHuyen']}} - {{$value['TinhTP']}}</td>
				<td>{{$value['qty']}}</td>
				<td>{{number_format($value['total_price'],'0',',',',')}} VNĐ</td>
				<td><span id="total_commission">{{number_format($value['total_commission'],'0',',',',')}} VNĐ</span></td>
				<td>{{substr( $value['created_at'],  0, 10 )}}</td>
				<td></td>
				<td>{{$value['sale']}}</td>
				<td>
					@if($value['status'] == 1)
						<div class="status">Mới tạo</div>
					@elseif($value['status'] == 2)
						<div class="status">Khách hàng hủy</div>
					@elseif($value['status'] == 3)
						<div class="status-yes">Đã nhận</div>
					@elseif($value['status'] == 4)
						<div class="status-yes">Đã liên hệ</div>
					@else
						<div class="status-yes">Đã chốt sale</div>
					@endif
				</td>
			</tr>
			@endforeach
			@endif		
		</tbody>
	</table>
		<!-- <audio controls id="myAu" autoplay="">
			<source src="../../public/uploads/meomeo.mp3" type="audio/mpeg">
			</audio> --> 
		</div>
		<div style="height: 30px"></div> 
		<!-- hiển thị danh sách sale -->
		<script type="text/javascript">
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			$(document).ready(function(){ 
				$.ajaxSetup({
					headers : {
						'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url : "{{route('getListSale')}}",
					type : "POST",
					cache: false,
					data : {_token:CSRF_TOKEN},
					success:function(data){ 
						$("select.sale").append(data);
					},error:function(error){
						alert("Thêm thất bại");
					}
				});  
			});  
		</script>

		<script type="text/javascript">
			//Thoát detail_orders
			function exit(){
				$("div.tbl_order_detail").css('display','none');
				$("tbody#detail_order").empty();
			}
			//# Thay đổi % hoa hồng của sản phẩm
			function changeCommission(e){
				var id = $(e).attr('id');
				var value = $(e).text();
				$(e).html("");
				$(e).html("<input type='number' id='commissions' name='commission' max='100' min='0' placeholder='0' value='"+value+"'><button>Xác nhận</button>");
				$('button').click(function(){
					$(e).text($('input#commissions').val());
					var value = $(e).text();
					$.ajaxSetup({
						headers : {
							'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : "{{route('changeCommissions')}}",
						type : "POST",
						cache: false,
						data : {_token:CSRF_TOKEN,id:id,value:value},
						success:function(data){ 
							$("span#commission_price").text(data);
							$.ajaxSetup({
								headers : {
									'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
								}
							});
							$.ajax({
								url : "{{route('orderCommissions')}}",
								type : "POST",
								cache: false,
								data : {_token:CSRF_TOKEN,id:id,value:value},
								success:function(data){ 
									$("span#total_commission").text(data + " VNĐ");
								},error:function(error){
									alert("Thêm thất bại");
								}
							});  
						},error:function(error){
							alert("Thêm thất bại");
						}
					});  
				})
			}
			// Hiển thị chi tiết đơn hàng
			function showOrderDetail(e){
				$("div.tbl_order_detail").css('display','inline-block');
				$("tbody#detail_order").empty();
				var id = $(e).attr("id");
				$.ajaxSetup({
					headers : {
						'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url : "{{route('getListDetailOrderAdmin',['id'=>"+id+"])}}",
					type : "POST",
					cache: false,
					data : {_token:CSRF_TOKEN,id:id},
					success:function(data){ 
						$("tbody#detail_order").append(data);
						$("span#name_order").text($("td#name_order_"+id).text());
						var value = $('select#status-detail').val();
						if(value<3){
							$('select#status-detail').css('background-color','#fd8c8c');
						}else{
							$('select#status-detail').css('background-color','green');
						}
						// thay đổi màu lựa chọn trạng thái khi click
						$('select#status-detail').change(function(){
							var value = $(this).val();
							if(value<3){
								$(this).css('background-color','#fd8c8c');
							}else{
								$(this).css('background-color','green');
							}
						})
					},error:function(error){
						alert("Thêm thất bại");
					}
				});  
			}
			// Đổi màu ô select
			$(document).ready(function(){
			// thay đổi màu lựa chọn trạng thái khi load
			var value = $('select#status').val();
			if(value<3){
				$('select#status').css('background-color','#fd8c8c');
			}else{
				$('select#status').css('background-color','green');
			}
			// thay đổi màu lựa chọn trạng thái khi click
			$('select#status').change(function(){
				var value = $(this).val();
				if(value<3){
					$(this).css('background-color','#fd8c8c');
				}else{
					$(this).css('background-color','green');
				}
			})
		})	
	</script>
	@endsection