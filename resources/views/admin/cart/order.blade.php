@extends('admin.master')
@section('css')
<style type="text/css" media="screen">
	table#tbl_order{
		width: 99%;
		margin-top: 40px;
	}
	table#tbl_order td{
		text-align: center;
		padding: 10px;
	}
	div.status{
		width: 190px;
		text-align: center;
		padding: 12px;
		margin: auto;
		background-color: #fd8c8c;
		color: white;
		border-radius: 10px;
	}
	option{
		background-color: #a0e1ec;
		color: black;
	}
</style>
@endsection('css')
@section('content')			
<div class="link">
	<!-- HIển thị đơn hàng mới để admin chuyển cho sale duyệt -->
	Đơn hàng > <a class="link" href="{{ route('getListOrder',['id'=>Auth::User()->id]) }}" >Đơn hàng chờ duyệt</a>
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px;padding-left: 15px; border-radius: 4px;padding-bottom: 50px; ">
<a href="{{ route('getListOrderSale',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Đơn hàng mới</button></a>
<a href="{{ route('order_confirm',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Đơn hàng đã chốt</button></a>
		<!-- Hiển thị chi tiết đơn hàng -->
		<div class="tbl_order_detail" style="width: 99%;height:300px;border: 1px solid black;display: none">
			<div onclick="exit()" style="border: 1px solid;height: 50px;padding: 8px 10px 10px 10px;float: right;font-size: 25px;font-weight: 900;background-color: red;color: white;"><i class="fas fa-times"></i></div>
			<h3 id="title">Chi tiết đơn hàng <i><span style="color: red" id="name_order"></span></i></h3>
			<table id="tbl_order" border="1" style="width: 98%;margin-left: 1%;">
				<thead>
					<tr>
						<td class="title">STT</td>
						<td class="title">Tên mặt hàng</td>
						<td class="title">Số lượng</td>						
						<td class="title">Thành tiền</td>
						<td class="title">% hoa hồng cho sale</td>
						<td class="title">Tiền sale</td>
					</tr>
				</thead>
				<tbody id="detail_order">
					
				</tbody>
			</table>
			<p style="color: black;font-size: 20px;float: right;margin-right: 1%;margin-top: 15px;font-weight: 700;">Tổng tiền hoa hồng : <span id="total_commission" style="color: red;font-size: 25px;font-weight: 900;"></span></p>
		</div>
		<!-- end hiển thị chi tiết đơn hàng -->

		<h3 id="title">Đơn hàng mới</h3>
		<table id="tbl_order" border="1">
			<thead>
				<tr>
					<td class="title">STT</td>
					<td class="title">Tên đơn hàng</td>
					<td class="title">Người đặt</td>
					<td class="title">Số điện thoại</td>
					<td class="title">Địa chỉ người nhận</td>
					<td class="title">Số lượng</td>
					<td class="title">Thành tiền</td>
					<td class="title">Tiền hoa hồng</td>
					<td class="title">Ngày đặt</td>
					<td class="title">Ngày chốt</td>
					<td class="title">Trạng thái</td>
					<td class="title">Chức năng</td>
				</tr>
			</thead>
			<tbody>
				@php$i=1@endphp
				@if(count($order) ==0 )
				<td colspan="12">Không có đơn hàng nào</td>	
				@else
				@foreach($data as $value)
				<tr>
					<td>1</td>
					<input type="hidden" id="id-orders" value="{{$value['id']}}">
					<td id="name_order"><span onclick="showOrderDetail(this)" id="{{$value['id']}}">{{$value['TenĐH']}}</span></td> <!-- Xem chi tiết đơn hàng -->
					<td>{{$value['name']}}</td>
					<td>{{$value['phone']}}</td>
					<td>{{$value['address']}} - {{$value['XaPhuong']}} - {{$value['QuanHuyen']}} - {{$value['TinhTP']}}</td>
					<td>{{$value['qty']}}</td>
					<td>{{number_format($value['total_price'],'0',',',',')}} VNĐ</td>
					<td>{{number_format($value['total_commission'],'0',',',',')}} VNĐ</td>
					<td>{{substr( $value['created_at'],  0, 10 )}}</td>
					<td></td>
					<form method="post" action="{{route('confirmOrder')}}">
						@csrf
						<td>
							<select id="status" name="status" style="border: 1px solid white;width: 210px;text-align: center;padding: 12px;margin: auto;background-color: green;color: white;border-radius: 10px;">
								@if($value['status'] == 1)
								<option selected="selected" value="0">Mới tạo</option>
								@else
								<option value="1">Mới tạo</option>
								@endif

								@if($value['status'] == 2)
								<option selected="selected" value="2">Khách hàng hủy</option>
								@else
								<option value="2">Khách hàng hủy</option>
								@endif

								@if($value['status'] == 3)
								<option selected="selected" value="3">Đã nhận</option>
								@else
								<option value="3">Đã nhận</option>
								@endif

								@if($value['status'] == 4)
								<option selected="selected" value="4">Đã liên hệ</option>
								@else
								<option value="4">Đã liên hệ</option>
								@endif

								@if($value['status'] == 5)
								<option selected="selected" value="5">Đã chốt sale</option>
								@else
								<option value="5">Đã chốt sale</option>
								@endif
							</select>
						</td>
						<td>
							<p id="test"></p>
							<input type="hidden" name="id" value="{{$value['id']}}">
							<input type="hidden" name="id_user" value="{{Auth::User()->id}}">					
							<input class="status-yes" type="submit" name="confirm" value="Xác nhận">
						</td>
					</form>	
				</tr>
				@endforeach
				@endif		
			</tbody>
		</table>
	</div>
	<div style="height: 30px"></div> 
	<script type="text/javascript">
		//Thoát detail_orders
		function exit(){
			$("div.tbl_order_detail").css('display','none');
			$("tbody#detail_order").empty();
		}
		// Hiển thị chi tiết đơn hàng
		function showOrderDetail(e){
			$("div.tbl_order_detail").css('display','inline-block');
			$("tbody#detail_order").empty();
			var id = $(e).attr("id");
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{route('getListDetailOrder',['id'=>"+id+"])}}",
				type : "POST",
				cache: false,
				data : {_token:CSRF_TOKEN,id:id},
				success:function(data){ 
					$("tbody#detail_order").append(data);
					$("option#status_order_detail").val();
					$("span#total_commission").text($('input#total_commission').val()+" VNĐ");
					$("span#name_order").text($("td#name_order").text());
					var value = $('select#status-detail').val();
					if(value<3){
						$('select#status-detail').css('background-color','#fd8c8c');
					}else{
						$('select#status-detail').css('background-color','green');
					}
					// thay đổi màu lựa chọn trạng thái khi click
					$('select#status-detail').change(function(){
						var value = $(this).val();
						if(value<3){
							$(this).css('background-color','#fd8c8c');
						}else{
							$(this).css('background-color','green');
						}
					})
				},error:function(error){
					alert("Thêm thất bại");
				}
			});  
		}
		// Thay đổi màu
		$(document).ready(function(){
			// thay đổi màu lựa chọn trạng thái khi load
			var value = $('select#status').val();
			if(value<3){
				$('select#status').css('background-color','#fd8c8c');
			}else{
				$('select#status').css('background-color','green');
			}
			// thay đổi màu lựa chọn trạng thái khi click
			$('select#status').change(function(){
				var value = $(this).val();
				if(value<3){
					$(this).css('background-color','#fd8c8c');
				}else{
					$(this).css('background-color','green');
				}
			})
		})	
	</script>
@endsection