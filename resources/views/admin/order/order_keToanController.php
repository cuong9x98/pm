<?php

namespace App\Http\Controllers\admin\order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\orders;
use App\orders_detail;
use App\User;
use App\product;
use App\province;
use App\district;
use App\ward;

class order_keToanController extends Controller
{
    // Hiển thị danh sách đơn hàng bên kế toán
    public function orderkeToan(){
    	$data = [];
		$orders = orders::where('status',5)->where('notifi',3)->get();
		if(count($orders)>0){
			foreach ($orders as $key=>$value_order) {
                // Lấy thông tin đơn hàng
				$data[$key]['qty'] = $value_order->qty;
				$data[$key]['TenĐH'] = $value_order->TenĐH;
				$data[$key]['total_price'] = $value_order->total_price;
				$data[$key]['total_commission'] = $value_order->total_commission;
				$data[$key]['id'] = $value_order->id;
				$data[$key]['created_at'] = $value_order->created_at;
				$data[$key]['updated_at'] = $value_order->updated_at;
				// Lấy thông tin người chốt sale
				$customer = User::where('id',$value_order->id_sale)->get();
				foreach ($customer as $value_customer) {    
					$data[$key]['name_sale'] = $value_customer->name;  
				}
                // Lấy thông tin người đặt
				$customer = User::where('id',$value_order->id_customer)->get();
				foreach ($customer as $value_customer) {    
					$data[$key]['name'] = $value_customer->name;  
					$data[$key]['phone'] = $value_customer->phone;  
					$data[$key]['address'] = $value_customer->address['dia_chi'];
                    // Lất TinhTP
					$province = province::where('id',$value_customer->address['TinhTP'])->get();
					foreach ($province as $value_province) {
						$data[$key]['TinhTP'] = $value_province->_name;
					}
                    // Lấy QuanHuyen
					$district = district::where('id',$value_customer->address['QuanHuyen'])->get();
					foreach ($district as $value_district) {
						$data[$key]['QuanHuyen'] = $value_district->_name;
					}
                    // Lấy XaPhuong
					$ward = ward::where('id',$value_customer->address['XaPhuong'])->get();
					foreach ($ward as $value_ward) {
						$data[$key]['XaPhuong'] = $value_ward->_name;
					}
				}
			} 
			return view('admin.cart.order_keToan',['order'=>$orders,'data'=>$data]);
		}else{
        //echo "không";
			return view('admin.cart.order_keToan',['order'=>$orders]);
		}
    }
    // Láy chi tiết đơn hàng bên kế toán
    public function getListDetailOrder(Request $Request){
        $data = "<tr>";
        $orders = orders::where('id',$Request->get('id'))->get();
        foreach ($orders as $value_orders) {
            $data.="<input type='hidden' id='total_commission' value='".number_format($value_orders->total_commission,'0',',',',')."'>";
        }
        $orders_detail = orders_detail::where("order_id",$Request->get('id'))->get();
        foreach ($orders_detail as $value) {
            $data.="<td>1</td>";
            $product = product::where('id',$value->id_prod)->get();
            foreach ($product as $value_prod) {
                $data.="<td>".$value_prod->name."</td>";
            }
            $data.="<td>".$value->qty."</td>";
            $data.="<td>".number_format($value->total_price,'0',',',',')." VNĐ </td>";
            if($Request->get('id_user') != 2){
                $data.="<td><span ondblclick='changeCommission(this)' id='".$Request->get('id')."'>".$value->commission_."</span> % </td>";
            }else{
                $data.="<td><span>".$value->commission_."</span> % </td>";
            }
            $data.="<td><span id='commission_price'>".number_format($value->commission_price,'0',',',',')."</span> VNĐ </td>";
        }
        $data.="</tr>";
        echo $data;
    }
}
