@extends('admin.master')
@section('title','Danh mục')
@section('content')
<!--heder end here-->       
<div>
    <h1>Chính sách nâng cấp</h1>
    <form method="post">
        @csrf
        @if(count($nang_cap) == 0)
            <textarea required class="input_add ckeditor" name="detail"></textarea>
            <input type="submit" name="edit" value="Sửa">
        @else
            @foreach($nang_cap as $value)
                <textarea required class="input_add ckeditor" name="detail">{{$value->detail}}</textarea>
                <input type="hidden" name="id" value="{{$value->id}}">
                <input type="submit" name="edit" value="Sửa">
            @endforeach
        @endif
    </form>
</div>
@endsection('content')
