@extends('admin.master')

@section('css')	
<style type="text/css">
	select {
		padding: 10px;

	}
	label{
		margin: 0px 5px 0 10px;
	}
	.btn-move{
		border: 1px solid black;
		height: 70px;
		width: 70px;
		opacity: 0.1;
		border-radius: 70px;
		display: inline-block;
		position: absolute; 
		margin: 210px 70px 0 70px;
		font-size: 30px;
		font-size: 50px;
		line-height: 70px;
		padding-left: 20px;
	}
	.btn-move:hover{
		opacity: 0.4;
	}
	div#btn-right{
		right: 0;
	}
	div#chart{
		position: relative;
	}
	h1#title_DoanhThu{
		text-align: center;
	}
	div#time{
		float: right;
		padding-right: 30px
	}
	span.time{
		padding: 10px;
		border: 1px solid black;
		border-radius: 6px;
	}
</style>
@endsection('css')
@section('content')				
<div class="link">
	<a class="link" href="#" >Trang chủ</a>
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px;padding-left: 65px;padding-right: 65px; border-radius: 4px;padding-bottom: 50px; ">
	<div id="doanh_so" style="width: 97%; height: 170px">
		<div id="Tong_san_pham_ban" style="width: 510px;height:150px ;padding:0 20px; 150px;margin-left: 30px;display: inline-block;float: right; line-height: 
		150px">
		<select id="date">
			<option value="null">-- Ngày --</option>
		</select>
		<select id="month">
			<option value="null">-- Tháng --</option>
		</select>
		<select id="year">
			<option value="null">-- Năm --</option>
		</select>
		<div id="search-doanhThu" style="border: 1px solid black;display: inline-block;line-height: 0;padding: 13px"><i class="fas fa-search"></i></div>
	</div>
	<div id="Tong_san_pham_ban" style="border: 1px solid black; width: 350px;height: 150px;margin-left: 30px;display: inline-block;">
		<div style="display: inline-block;padding: 20px">
			<span style="font-weight: 900;display: inline-block;">Tổng doanh thu</span><br>
			<span id="DoanhThu" style="font-weight: 900;font-size: 30px;display: inline-block;"></span><br>
			<span style="font-weight: 900;display: inline-block;">VNĐ</span>
		</div>
		<p style="float: right;font-weight: 900;font-size: 50px;padding: 20px;display: inline-block;"><i class="fas fa-coins" style="margin-top: 25px"></i></p>
	</div>
	<div id="Tong_san_pham_ban" style="border: 1px solid black; width: 350px;height: 150px;margin-left: 30px;display: inline-block;">
		<div style="display: inline-block;padding: 20px">
			<span style="font-weight: 900;display: inline-block;">Phần mềm bán</span><br>
			<span id="SanPhamBan" style="font-weight: 900;font-size: 30px;display: inline-block;"></span><br>
			<span style="font-weight: 900;display: inline-block;">Phần mềm</span>
		</div>
		<p style="float: right;font-weight: 900;font-size: 50px;padding: 20px;display: inline-block;"><i class="fas fa-shopping-cart" style="margin-top: 25px"></i></p>
	</div>
	@if(Auth::User()->role == 2 || Auth::User()->role == 3 || Auth::User()->role == 7)
	<div id="Tong_san_pham_ban" style="border: 1px solid black; width: 350px;height: 150px;margin-left: 30px;display: inline-block;">
		<div style="display: inline-block;padding: 20px">
			<span style="font-weight: 900;display: inline-block;">Số khách hàng</span><br>
			<span id="KhachHang" style="font-weight: 900;font-size: 30px;display: inline-block;"></span><br>
			<span style="font-weight: 900;display: inline-block;">Người</span>
		</div>
		<p style="float: right;font-weight: 900;font-size: 50px;padding: 20px;display: inline-block;"><i class="fas fa-user" style="margin-top: 25px"></i></p>
	</div>
	@endif
</div>

<h1 id="title_DoanhThu">Doanh thu</h1>
<div id="chart">
	<div id="time">
		<span class="time" id="year" onclick="BieuDoTheoNam(this)">Năm</span>	
		<span class="time" id="month" onclick="BieuDoTheoThang(this)">Tháng</span>	
	</div>
	<div id="btn-left" class="btn-move"><i class="fas fa-angle-left"></i></div>
	<div id="btn-right" class="btn-move"><i class="fas fa-angle-right"></i></div>
	<canvas id="lineChart" style="display:inline-block; animation-delay: 0.5s;width: 1760px !important; height: 500px;"></canvas>
	<canvas id="lineChart_1" style="display:none;width: 1760px !important;  animation-delay: 0.5s;height: 500px;"></canvas>
</div>
@if(Auth::User()->role==2)
<h1>Tổng sản phẩm</h1>
<label>Danh mục</label>
<select id="category">
	<option value="all">Tất cả</option>
	@foreach ($category as $value)
	<option value="{{$value->id}}">{{$value->name}}</option>
	@endforeach
</select>
<button class="btn btn-dark" id="search" style="border-radius: 0px;height: 41px;margin-bottom: 3px;margin-left: 60px;">Tìm kiếm</button>
<table border="1"style="width: 100%;text-align: center;">
	<thead>
		<td class="title">Tên sản phẩm</td>
		<td class="title">Giá</td>
		<td class="title">Ảnh</td>
		<td class="title">Giảm giá</td>
	</thead>
	<tbody id="product"></tbody>
</table>
@endif
</div>
<div style="height: 30px"></div> 
<script type="text/javascript">
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var role = {{Auth::User()->role}};
	var id_user = {{Auth::User()->id}};
	// Ajax lấy biểu đồ theo tháng
	function BieuDoTheoThang(e){
		if(role == 2 || role == 3 || role == 6 || role==7){
			var x = [];
			var y = [];
			var date = new Date();
			var year_now = date.getFullYear();
			var now = date.getMonth()+Number(2);
			for (var i = 1; i <=10 ; i++) {
				if(now==1){
					now=13;
					year_now -= Number(1);
				}
				now-=1;
				x.push(now+"-"+year_now);
				var last = now;
			}
			// sự kiện click biểu đồ tháng sang trái
			$("#btn-left").click(function(){
				for (var i = 1; i <=10 ; i++) {
					if(now==1){
						now=13;
						year_now -= Number(1);
					}
					now-=1;
					x.push(now+"-"+year_now);
					var last = now;
				}
			});
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{route('getDoanhThuThang')}}",
				type : "POST",
				cache: false,
				data : {_token:CSRF_TOKEN,role:role,id_user:id_user,x:x,year_now:year_now},
				success:function(data){ 
					$("canvas#lineChart").css("display","none");
					$("canvas#lineChart_1").css("display","inline-block");
					var value = JSON.parse(data);
					for (var i = 9; i >= 0; i--) {
						y.push(value["month_"+i]);
					}
					var x2 =  [];
					for (var i = 9; i >= 0; i--) {
						x2.push(x[i]);
					}					
					var CHART = document.getElementById('lineChart_1').getContext('2d');
					var line_chart = new Chart(CHART,{
						type: "bar",
						data:{
							labels:x2,
							datasets:[{
								labels:'Doanh thu',
								data:y
							}],
							backgroundColor:'red',
						}
					});
				},error:function(error){
					alert("Thêm thất bại");
				}
			});  
			console.log(x);
		}			
	}
	// Ajax lấy biểu đồ
	function BieuDoTheoNam(e){
		if(role == 2 || role == 3 || role == 6 || role==7){
			var x = [];
			var y = [];
			var date = new Date();
			var now = date.getFullYear();
			for (var i = 2015; i <=2020 ; i++) {
				x.push(i);
			}
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{route('AjaxdoanhThu')}}",
				type : "POST",
				cache: false,
				data : {_token:CSRF_TOKEN,role:role,id_user:id_user,x:x},
				success:function(data){ 
					$("canvas#lineChart").css("display","inline-block");
					$("canvas#lineChart_1").css("display","none");
					var value = JSON.parse(data);
					$("span#DoanhThu").text(value['sum']);
					$("span#SanPhamBan").text(value['buy']);
					$("span#KhachHang").text(value['user']);
					for (var i = 0; i < 6; i++) {
						y.push(value["year_"+i]);
					}
					var CHART = document.getElementById('lineChart').getContext('2d');
					var line_chart = new Chart(CHART,{
						type: "bar",
						data:{
							labels:x,
							datasets:[{
								labels:'Doanh thu',
								data:y
							}],
							backgroundColor:'red',
						}
					});
				},error:function(error){
					alert("Thêm thất bại");
				}
			});  
		}	
	}
	$(document).ready(function(){	
		$("span#year").click();
		for (var i = 1; i <= 31; i++) {
			$("select#date").append("<option>"+i+"</option>")
		}
		for (var i = 1; i <= 12; i++) {
			$("select#month").append("<option>"+i+"</option>")
		}
		for (var i = 2010; i <= 2100; i++) {
			$("select#year").append("<option>"+i+"</option>")
		}
		// Ajax lấy sản phẩm danh mục
		$("button#search").click(function(){
			var category = $('select#category').val();	
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{route('search')}}",
				type : "POST",
				cache: false,
				data : {_token:CSRF_TOKEN,category:category,role:role,id_user:id_user},
				success:function(data){
					$("tbody#product").empty();
					$("tbody#product").append(data);
				},error:function(error){
					alert("Thêm thất bại");
				}
			});	                                       
		});	
		// Ajax lấy doanh thu tìm kiếm theo ngày tháng năm
		$("div#search-doanhThu").click(function(){
			var date = $("select#date").val();
			var month = $("select#month").val();
			var year = $("select#year").val();
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{route('searchDoanhThu')}}",
				type : "POST",
				cache: false,
				data : {_token:CSRF_TOKEN,date:date,month:month,year:year,role:role,id_user:id_user},
				success:function(data){
					$('span#DoanhThu').text(data);
				},error:function(error){
					alert("Thêm thất bại");
				}
			});	                                       
		});	  
	})
</script>
@endsection('content')