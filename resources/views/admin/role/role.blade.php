@extends('admin.master')
@section('title','Danh mục')
@section('css')
<style type="text/css">
	td{
		padding: 15px
	}
	div.border{
		border: 1px solid gray
	}
</style>
@endsection('css')
@section('content')
<div class="link">
	<a class="link" href="{{route('getRole')}}">Phân quyền</a>
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px; border-radius: 4px;padding-bottom: 50px; ">
	<h3 id="title">Phân quyền</h3>
	<div class="row">
		<div class="border col-md-4 col-xs-12 col-sm-12">
			<h2 style="text-align: center;">Tạo tài khoản</h2>
			<form method="post" action="{{route('create')}}">
				@csrf
				<table id="list-user" style="width: 100%;text-align: center;">
					<tbody>
						<tr>
							<td>Họ và tên</td>
							<td>
								<input type="text" required="required" name="name">
							</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>
								@if($errors->has('email'))
								<p>
									<strong>
										{{$errors->first('email')}}
									</strong>
								</p>
								@endif
								<input type="email" required="required" name="email">
							</td>
						</tr>	
						<tr>
							<td>Tên đăng nhập</td>
							<td>
								@if($errors->has('username'))
								<p>
									<strong>
										{{$errors->first('username')}}
									</strong>
								</p>
								@endif
								<input type="text" required="required" name="username">
							</td>
						</tr>
						<tr>
							<td>Mật khẩu</td>
							<td><input type="password" required="required" name="password"></td>
						</tr>
						<tr>
							<td>Xác nhận mật khẩu</td>
							<td>
								@if($errors->has('confim_password'))
								<p>
									<strong>
										{{$errors->first('confim_password')}}
									</strong>
								</p>
								@endif
								<input type="password" required="required" name="confim_password">
							</td>
						</tr>
						<tr>
							<td>Quyền</td>
							<td>
								<select name="role">
									@foreach($role as $sl_role)
									@if($sl_role->id !=  1)        
									<option value="{{$sl_role->id}}">{{$sl_role->name}}</option>
									@endif
									@endforeach	
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="create" value="Tạo tài khoản"></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div class="border col-md-4 col-xs-12 col-sm-12">
			<table id="list-user" style="width: 100%;text-align: center;">
				<thead>
					<td>STT</td>
					<td>Họ và tên</td>
					<td>Tên đăng nhập</td>
					<td>Quyền</td>
					<td>Chức năng</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@foreach($users as $value_users)       
					@if($value_users->role>1) 
					<tr>
						<td>{{$i}}</td>
						<td>{{$value_users->name}}</td>
						<td>{{$value_users->username}}</td>						
						<td>
							@foreach($role as $value_role)        
							@if($value_users->role == $value_role->id)
							{{$value_role->name}}
							@endif
							@endforeach	
						</td>
						<td>
							<div id="{{$value_users->id}}" class="btn-fucntion" onclick="return getRole(this);" style="padding: 10px;border-radius: 5px;color: white;background-color: #6161db;">
								Chỉnh sửa
							</div>
						</td>
					</tr>
					<?php $i++ ?>
					@endif
					@endforeach		
				</tbody>
			</table>
		</div>
		<div class="border col-md-4 col-xs-12 col-sm-12" id="div_role" style="display: none">
			<form method="post" action="{{route('updateRole')}}">
				@csrf
				<p style="text-align: center;text-align: center;font-size: 25px;font-weight: 900;margin-top: 8px;">Danh sách quyền</p>
				<input type="hidden" id="id_user" name="id_user" value="">
				<div id="content_role"></div>
				<input type="submit" name="update" value="Cập nhật" class="btn btn-dark" style="margin-bottom: 20px; float: right;margin-right: 70px">
			</form>
		</div>
	</div>
	
</div>
<div style="height: 30px"></div> 
<script type="text/javascript">
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	function getRole(e){
		
			// $("option:first").removeAttr("selected");
			var id = $(e).attr('id');
			$("input#id_user").val(id);
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "http://localhost:8888/shop-mart/public/admin/quyen",
				type : "GET",
				cache: false,
				data : {_token:CSRF_TOKEN,id:id},
				success:function(data){
					$("div#div_role").css("display","inline-block");
					var id = $(this).attr('id');
					$("input#id_user").attr('id',id);
					$("div#content_role").empty();
					$("div#content_role").append(data);
				},error:function(error){
					alert("Thêm thất bại");
				}
			});
		}
		$(document).ready(function(){  
			$("div.btn-fucntion").click(function(){


			})
		})
	</script>
	@endsection('content')
