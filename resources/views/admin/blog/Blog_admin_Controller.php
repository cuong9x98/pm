<?php

namespace App\Http\Controllers\admin\blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog;
class Blog_admin_Controller extends Controller
{
    // tin tức đã đăng
    public function TinTucDaDang()
    {
        $blog = blog::where('status',1)->get();
        return view('admin.blog.tin_tuc_da_dang',['blog'=>$blog]);
    }

    // tin tức chờ duyệt
    public function TinTucChoDuyet($id)
    {
        $blog = blog::where('admin_confirm',$id)->where('status',0)->get();
        return view('admin.blog.tin_tuc_cho_duyet',['blog'=>$blog]);
    }

    // tin tức chờ duyệt
    public function TinTucDaDuyet($id)
    {
        $blog = blog::where('admin_confirm',$id)->where('status',1)->get();
        return view('admin.blog.tin_tuc_da_duyet',['blog'=>$blog]);
    }

    // duyệt blog
    public function duyetBlog(Request $Request)
    {
        $blog = blog::where('id',$Request->id)->update([
            'status'=>$Request->check,
        ]);
        return back();
    }
}