
@extends('admin.master')
@section('title','Danh mục')
@section('css')
<style type="text/css">
	table#tbl_prod{
		width: 99%;
		margin-top: 40px;
	}
	table#tbl_prod td{
		text-align: center;
		padding: 10px;
	}
</style>
@endsection('css')

@section('content')
<!--heder end here-->   	
<div class="link">
	
</div>
<div style="border: 1px solid #ebeff6;margin-top: 40px;padding-left: 15px;padding-top: 20px; border-radius: 4px ">
	@if(Auth::User()->role == 2 || Auth::User()->role == 3 )
	<a href="{{route('getBlogIndex')}}"><button class="btn btn-dark">Tin tức đăng</button></a>
	<a href="{{route('getBlogIndexAdmin',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức chờ duyệt</button></a>
	<a href="{{route('getBlogIndexAdmin_confirm',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức đã duyệt</button></a>
	@endif
	@if(Auth::User()->role == 5 )
	<a href="{{route('getBlogAdd',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức đã đăng</button></a>
	<a href="{{route('TinTucDoiDuyet',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Tin tức chờ duyệt</button></a>
	@endif
	<a href="{{route('getAddBlogIndex')}}"><button class="btn btn-dark">Thêm tin tức</button></a>
	<h3 id="title">Tin tức chờ duyệt</h3>
	<table id="tbl_prod" border="1">
		<thead>
			<td class="title">STT</td>
			<td class="title">ID</td>
			<td class="title">Name</td>
			<td class="title">Ảnh</td>
			<td class="title">Người tạo</td>
			<td class="title" colspan="2">Chức năng</td>
		</thead>
		<tbody>

			@if(count($blog) == 0)
			<tr>
				<td colspan="7">Không có blog nào !</td>
			</tr>
			@else
			<?php $i=1 ?>
			@foreach($blog as $value_blog)
			<tr>
				<td>{{$i}}</td>
				<td>{{$value_blog->id}}</td>
				<td style="text-align: left;">{{$value_blog->title}}</td>
				<td><img id="img_prod" src="../../public/uploads/img_blog/{{$value_blog->img}}"></td>
				<td>Đặng nam</td>
				<td><a href="{{route('getEditBlogShow',['id'=>$value_blog->id])}}"><i class="fas fa-pencil-alt"></i></a></td>
				<td><a href="{{route('DeleteBlog',['id'=>$value_blog->id])}}"><i class="fas fa-trash"></i></a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<div style="height: 40px"></div> 
</div>
<div style="height: 30px"></div> 
@endsection('content')
