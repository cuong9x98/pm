@extends('admin.master')
@section('title','Danh mục')
@section('css')
<style type="text/css">
  input.input_name_cate{
   width: 300px;
   display: inline-block;
 }
 div.tbl_add_cate{
   width: 49%;
   position: absolute;
   margin-left: 40%;
   margin-top: 10px;
 }
 table.tbl_cate{
   width: 50%;
   margin-top: 20px;
 }
 table.tbl_cate td{
   text-align: center;
   padding: 17px;

 }
 img#img_cate{
  width: 60px;
  height: 60px
}
input#imgInp{
  position: absolute;
  z-index: -99999;
}
img#blah{
  border: 1px solid black;
  margin-top: 10px;
}
a#del_cate{
  color: black
}

</style>
@endsection('css')

@section('content')
<!--heder end here-->
<div class="link">
  <a class="link" href="{{route('getAdCate')}}">Danh mục</a>
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px;
padding-left: 65px; border-radius: 4px;padding-bottom: 50px;    min-height: 600px; ">
<h3 id="title">Danh mục</h3>


<div class="tbl_add_cate">
  <button type="#" id="add_category" class="btn btn-dark">Thêm danh mục</button>
  <form method="post" enctype="multipart/form-data" action="{{ route('postAdCate') }}" id="form_category">
    {{-- Danh mục --}}
    @csrf
    <table>
      <label id="label">Thêm danh mục</label>
      <tr>
        <td><input type="text" name="name" id="name" placeholder="Tên danh mục ...." class="input_name_cate form-control" required="required"></td>
      </tr>
      <tr>
        <td>
          <input type='file' id="imgInp" name="img_name">
          <img id="blah" src="../../public/uploads/img_category/Add_img.png" style="width: 200px; height: 200px"  />     
          <script type="text/javascript">
            function readURL(input) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();                        
                reader.onload = function(e) {
                  $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);       
              } 
            }
            $("#imgInp").change(function() {
              readURL(this);
            });
            $('#blah').click(function(){
              $('#imgInp').click();
            })
          </script>
        </td>
      </tr>
      <tr>
        <td style="font-weight: 700">Phần trăm hoa hồng : <input type="number" min="0" max="100" placeholder="0" id="commission" name="commission" style="width: 70px;margin: 20px 0;"> %</td>
      </tr>
      <td>
        <input type="hidden" name="id_cate" id="id_cate" value="">
        <input type="submit" id="submit" name="add" class="btn btn-dark" value="Thêm danh mục">
      </td>
    </table>
    <input type="hidden" name="creater" value="{{Auth::User()->id}}">       
  </form>	
</div>

<table class="tbl_cate" border="1">
  <head>
   <tr>
    <td class="title">STT</td>
    <td class="title">ID</td>
    <td class="title">Name</td>
    <td class="title">Ảnh</td>
    <td class="title">% hoa hồng</td>
    <td class="title" colspan="2">Chức năng</td>

  </tr>
</head>
<body>
  <?php $i=1 ?>
  @if(count($category) == 0)
  <tr>
    <td colspan="6">Không có danh mục !</td>
  </tr>
  @else
  @foreach($category as $value_cate)        
  <tr>
    <td>{{$i}}</td>
    <td>{{$value_cate->id}}</td>
    <td class="name_cate">{{$value_cate->name}}</td>              
    <td><img id="img_cate" src="../../public/uploads/img_category/{{$value_cate->img}}"></td>
    <td>
      {{$value_cate->commission}} %
    </td>
    <td>
      <span id="edit_cate" class={{$value_cate->id}}><i class="fas fa-pencil-alt"></i></span>
    </td>

    <td><a href="{{Route('delAdCate',['id'=>$value_cate->id])}}" id="del_cate"><i class="fas fa-trash"></i></a></td>
  </tr>
  <?php $i++ ?>
  @endforeach
  @endif
</body>
</table>
<script type="text/javascript">
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $(document).ready(function(){  

    $("button#add_category").click(function(){
      $('label#label').text('Thêm danh mục');
      $('input#name').attr('value',"");
      var url = "../../public/uploads/img_category/Add_img.png";
      $('img#blah').attr('src',url);
      $("input#submit").attr("value","Thêm danh mục");
      $("form#form_category").attr("action","{{ route('postAdCate') }}")
    }); 
    // Sửa sản phẩm
    $("span#edit_cate").click(function(){
      event.preventDefault();
      $('label#label').text('Sửa danh mục');       
      var id = $(this).attr('class');
      $('input#id_cate').attr('value',id);
      $("input#submit").attr("value","Sửa danh mục");
      $("form#form_category").attr("action","{{ route('updateCate') }}")
      $.ajaxSetup({
        headers : {
          'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url : "http://localhost:8888/shop-mart/public/admin/sua-danh-muc",
        type : "POST",
        cache: false,
        data : {_token:CSRF_TOKEN,id:id},
        success:function(data){
          var data_json = JSON.parse(data);
          $('input#name').attr('value',data_json['name']);
          var url = "../../public/uploads/img_category/"+data_json['img'];
          $('img#blah').attr('src',url);
          $('input#commission').attr('value',data_json['commission']);
        },error:function(error){
          alert("Thêm thất bại");
        }
      });
    });
  });
</script>
<div style="height: 20px"></div> 
</div>
<div style="height: 30px"></div> 

@endsection('content')
