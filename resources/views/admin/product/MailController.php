<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmailProduct;
use App\User;
class MailController extends Controller
{
    // Gửi mail đến admin có sản phẩm mới
	public function sendEmailToUser($admin_confirm,$name) {
        $admin = User::where('id',$admin_confirm)->get();
        foreach ($admin as $value) {
        	$email_user = $value->email;
        	$name_user = $value->name;
        }
		$to_email = $email_user;
		$objDemo = new \stdClass();
		$objDemo->name = $name_user;
		$objDemo->messages = "Cửa hàng Shop-mart chúng tôi xin thông báo, bạn có sản phẩm \"".$name."\"  đang chờ được duyệt.
		Xin trân thành cảm ơn bạn đã tin tưởng chúng tôi. Chúc bạn một ngày mới vui vẻ!";
		Mail::to($to_email)->send(new SendEmailProduct($objDemo));
		return back();
	}
	
	// Gửi mail đến người tạo
	public function sendEmailToUserSell($creater,$messages){
       	$creater = User::where('id',$creater)->get();
       	foreach ($creater as $value) {
            $to_email = $value->email;
            $objDemo = new \stdClass();
            $objDemo->name = $value->name;
            $objDemo->messages = $messages;
            Mail::to($to_email)->send(new SendEmailProduct($objDemo));
            return back();
        }
    }
}
