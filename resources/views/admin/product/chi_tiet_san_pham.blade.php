@extends('admin.master')
@section('title','Danh mục')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="css/style - Copy.css">
<link rel="stylesheet" href="css/style-k - Copy.css" type="text/css">
<link rel="stylesheet" href="css/img-product-detail - Copy.css">
@endsection('css')

@section('content')
<div class="link">
	<a class="link" href="{{route('getProdIndex')}}">Danh sách sản phẩm</a> 
</div>
<div style="height: 30px"></div>

<div style="border: 1px solid #ebeff6;padding-top: 20px;
padding-left: 15px; border-radius: 4px;padding-bottom: 50px; ">

<div class="ct-banner pb-5">
@if(Auth::User()->role == 2 || Auth::User()->role == 3 )
<a href="{{ route('getProductSell',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Sản phẩm đang bán</button></a>
<a href="{{ route('getProdIndex',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Sản phẩm chờ duyệt</button></a>
<a href="{{ route('getListconfirmProduct',['id'=>Auth::User()->id]) }}"><button class="btn btn-dark">Sản phẩm đã duyệt</button></a>
@endif
@if(Auth::User()->role == 4 )
<a href="{{route('SanPhamDaTao',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Sản phẩm đã tạo</button></a>
<a href="{{route('SanPhamChoDuyet',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Sản phẩm chờ duyệt</button></a>
@endif
<a href="{{ route('addAdPro' )}}"><button class="btn btn-dark">Thêm sản phẩm</button></a>
    @foreach ($product as $value)
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="container-img">
                    <ul class="thumb">
                     @foreach ($detail_photo as $value_detail_photo)
                     @foreach ($value_detail_photo->img as $key=>$value_img)
                     <li><a href="../../public/uploads/detail_photo/{{ $value_img }}" target="imgBox"> <img style="height: 100%" src="../../public/uploads/detail_photo/{{ $value_img }}" alt=""> </a></li>
                     @endforeach                  		
                     @endforeach
                 </ul>
                 <div class="imgBox">
                    <img src="../../public/uploads/img_product/{{$value->img}}" alt="" srcset="">
                </div>

            </div>
        </div>
        <div class="col-md-6">

            <div class="detail mt-5">
                <h1>{{$value->name}}</h1>
                <div class="detail-star">
                    <div class="ratingStarContainer">
                        <input type="radio" name="starRating-2" id="star1">
                        <label for="star1"><i class="fa fa-star"></i></label>
                        <input type="radio" name="starRating-2" id="star2">
                        <label for="star2"><i class="fa fa-star"></i></label>
                        <input type="radio" name="starRating-2" id="star3">
                        <label for="star3"><i class="fa fa-star"></i></label>
                        <input type="radio" name="starRating-2" id="star4">
                        <label for="star4"><i class="fa fa-star"></i></label>
                        <input type="radio" name="starRating-2" id="star5">
                        <label for="star5"><i class="fa fa-star"></i></label>
                    </div>
                    <div class="detail-star-span">
                        <span class="detail-span"> (6 customer reviews) </span>
                        <span> 3 sold</span>
                    </div>

                </div>

                <form action="#" method="post">
                    @csrf
                    <div class="detail-content mt-3">
                        <h3>
                            @if($value->promotion >0)
                            <span class="price-product--cost">
                                <strike>{{number_format($value->price,0,'.',',')}}</strike> VNĐ
                            </span>
                            <span class="price-product--sale">
                                @if($value->promotion >0)
                                -
                                {{number_format($value->price - ($value->price*($value->promotion/100)),0,'.',',')}}
                                VNĐ
                                @endif
                            </span>
                            @else
                            <span class="price-product--cost">
                                {{number_format($value->price,0,'.',',')}}
                            </span>
                            @endif
                        </h3>
                        <p>{{ $value->description }}</p>
                    </div>
                    <div class="product-quantity">
                        <span class="product-quantity-minus"></span>
                        {{ $value->qty_product }}
                        <span class="product-quantity-plus"></span>
                    </div>
                    <input type="hidden" value="{{$value->id}}" name="productId">
                    <input type="hidden" value="{{($value->price)-($value->price*$value->promotion/100)}}" name="price">
                    <div class="detail-button">
                        <button class="btn-1" type="submit">Thêm vào giỏ</button>
                        <button class="btn-2">Mua ngay</button>
                        <button class="btn-3"><i class="fa fa-heart"></i></button>
                    </div>
                    <hr>
                    <div class="detail-info mt-3">
                        <span class="sku-wrapper">Categories:
                            <span class="sku">                           
                                <a href="#">{{$category}}</a>,
                            </span>
                        </span>
                        <span class="sku-wrapper">Tags:
                            <span class="sku">
                                @if(count($tag) > 0)                    
                                @foreach($tag as $value_tag)
                                <a href="#">{{$value_tag->name}}</a>,
                                @endforeach
                                @endif
                            </span>
                        </span>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>
<div class="container">
    <div class="ct-info">
        <div class="tabs mt-5">
            <input id="tab1" type="radio" name="grp" checked="checked" />
            <label for="tab1"><span>Description</span></label>
            <div class="ct-info-content">
                <div class="row">
                   {!! $value->detail !!}
               </div>
           </div>

           <input id="tab2" type="radio" name="grp" />
           <label for="tab2"><span>Review (0)</span></label>
           <div>Magni vel officiis expedita dolore adipisci nam praesentium id perspiciatis libero autem voluptatibus
            eligendi ea, illum, cum quos ducimus laboriosam itaque culpa recusandae a excepturi officia amet
        doloribus atque? Eaque?</div>

        <input id="tab3" type="radio" name="grp" />
        <label for="tab3"><span>Question & Answers (1)</span></label>
        <div>Velit obcaecati tempore libero alias, non pariatur rem eligendi minima, labore harum at impedit
            beatae, modi ea quaerat voluptatum! Magni deserunt quaerat fuga rerum adipisci odit perferendis
        mollitia, molestiae explicabo.</div>

        <input id="tab4" type="radio" name="grp" />
        <label for="tab4"><span>Shipping</span></label>
        <div>Accusamus recusandae quam cupiditate eius, aspernatur voluptates, provident odit autem, dolor nesciunt
            mollitia neque corrupti repudiandae eveniet? Iusto, iure? Impedit tempore ullam possimus rerum maxime
        quisquam autem nostrum delectus. Ullam.</div>
    </div>
</div>
</div>
@endforeach
<div style="height: 20px"></div> 
</div>
<div style="height: 30px"></div> 
@endsection('content')