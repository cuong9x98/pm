<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\category;
use DB;


class Product_admin_Controller extends Controller
{
    // Sản phẩm đang bán
    public function SanPhamDangBan(){
    	$category = category::all();
        $product = product::where('status',1)->get();
        return view('admin.product.san_pham_dang_ban',['product'=>$product,'category'=>$category]);
    }
    // Sản phẩm đang đợi duyệt
    public function SanPhamDangDoiDuet($id){
        $product = product::where('status',0)->where('admin_confirm',$id)->get();
        $category = category::all();
        return view('admin.product.san_pham_doi_duyet',['product'=>$product,'category'=>$category]);
    }
    // Sản phẩm đã duyệt
    public function SanPhamDaDuyet($id){
    	$product = product::where('admin_confirm',$id)->get();
        return view('admin.product.san_pham_da_duyet',['product'=>$product]);
    }
    // Duyệt sản phẩm
    public function confirm(Request $Request)
    {
        $product = product::where('id',$Request->id)->update([
            'status'=>$Request->check
        ]);
        return redirect()->route('sendNotifiToUserSell',['id'=>$Request->id,'check'=>$Request->check]);
        return view('admin.product.list_prod');
    }
}
