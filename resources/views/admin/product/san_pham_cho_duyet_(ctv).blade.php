
@extends('admin.master')
@section('title','Danh mục')
@section('css')
<style type="text/css">
	table#tbl_prod{
		width: 99%;
		margin-top: 30px;
	}
	table#tbl_prod td{
		text-align: center;
		padding: 10px;
	}
	strong{
		padding: 20px 200px;
		color: white;
		background-color: #d76161;
		border-radius: 10px;
	}
	button.btn-dark{
		margin-left:10px;
	}
</style>
@endsection('css')
@section('content')
<!--heder end here-->
<div class="link">
	
</div>
<div style="height: 30px"></div>
<div style="border: 1px solid #ebeff6;padding-top: 20px;padding-left: 15px; border-radius: 4px;padding-bottom: 50px; ">
<a href="{{route('SanPhamDaTao',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Sản phẩm đã tạo</button></a>
<a href="{{route('SanPhamChoDuyet',['id'=>Auth::User()->id])}}"><button class="btn btn-dark">Sản phẩm chờ duyệt</button></a>
<a href="{{ route('addAdPro' )}}"><button class="btn btn-dark">Thêm sản phẩm</button></a>
<h3 id="title">Sản phầm chờ duyệt</h3>
<table id="tbl_prod" border="1">
	<thead>
		<td class="title">STT</td>
		<td class="title">ID</td>
		<td class="title">Name</td>
		<td class="title">Ảnh</td>
		<td class="title">Danh mục</td>
		<td class="title">Price</td>
		<td class="title">Số lượng</td>
		<td colspan="2" class="title">Chức năng</td>
	</thead>
	<tbody>
		@if(count($product) == 0)
		<tr>
			<td colspan="8">Không có sản phẩm nào !</td>
		</tr>
		@else
		<?php $i=1 ?>
		@foreach($product as $value_prod)
		<tr>
			<td>{{$i}}</td>
			<td>{{$value_prod->id}}</td>		   			
			<td><a href="{{ route('detailProduct',['id'=>$value_prod->id]) }}">{{$value_prod->name}}</a></td>
			<td><img id="img_prod" src="../../public/uploads/img_product/{{$value_prod->img}}"></td>
			<td>
				@foreach($category as $value_cate)
				@if($value_cate->id == $value_prod->id_cate)
				{{$value_cate->name}}
				@endif
				@endforeach   
			</td>
			<td>{{number_format($value_prod->price,0,'.',',')}} VNĐ</td>
			<td>{{number_format($value_prod->qty_product,0,'.',',')}}</td>
			<td><a href="{{route('getProdedit',['id'=>$value_prod->id])}}"><i class="fas fa-pencil-alt"></i></a></td>
			<td><a href="{{route('getProddestroy',['id'=>$value_prod->id])}}"><i class="fas fa-trash"></i></a></td>
		</tr>
		@endforeach   
		@endif
	</tbody>
</table>
<div style="height: 40px"></div> 
</div>
<div style="height: 30px"></div> 
@endsection('content')
