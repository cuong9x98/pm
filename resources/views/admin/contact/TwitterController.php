<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\twitter;

class TwitterController extends Controller
{
    //
    public function twitter(){
    	// css address
		$twitter = twitter::all();
		return view("admin.contact.twitter",['twitter'=>$twitter]);
	}
	public function Themtwitter(Request $Request){
		DB::table('twitter')->insert([
			'ten'=>$Request->ten,
			'url'=>$Request->url,
			'created_at'=>Carbon::now()
		]);
		return back();
	}
	public function getEdittwitter($id){
		$twitter = twitter::where('id',$id)->get();
		return view("admin.contact.edit_twitter",['twitter'=>$twitter]);
	}

	public function postEdittwitter(Request $Request){
		$twitter = twitter::where('id',$Request->id)->update([
			'ten'=>$Request->ten,
			'url'=>$Request->url,
			'updated_at'=>Carbon::now()
		]);
		return back();
	}
	public function destroytwitter($id){
        twitter::where('id',$id)->delete();
        $twitter = twitter::all();
		return view("admin.contact.twitter",['twitter'=>$twitter]);
    }

    public function selecttwitter($id){
		$twitter = twitter::where('id',$id)->get();
		foreach ($twitter as $value) {
			$status = $value->status;
		}
		if($status == 0){
			$twitter = twitter::where('id',$id)->update([
				'status'=>1
			]);
		}else{
			$twitter = twitter::where('id',$id)->update([
				'status'=>0
			]);
		}
		return redirect()->route('gettwitter');
	}
}
