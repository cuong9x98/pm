@extends('admin.master')
@section('title','Danh mục')
@section('content')
<!-- css address.css -->
<div class="row">
	<div class="col-md-6">
		<div id="list_address">
			<p id="title">Danh sách địa chỉ facebook</p>
			<table id="list_address" border="1">
				<thead>
					<td>STT</td>
					<td>Tên</td>
					<td>url</td>
					<td>Trạng thái</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@foreach($facebook as $value)
					<tr>
						<td class="value">{{$i++}}</td>
						<td class="value">{{$value->ten}}</td>
						<td class="value">{{$value->url}}</td>
						<td class="value">
							<a id="edit" href="{{route('getEditfacebook',['id'=>$value->id])}}"><i class="fas fa-pencil-alt"></i></a> | 
							<a id="edit" href="{{route('remove',['id'=>$value->id])}}"><i class="fas fa-trash"></i></a> | 
							@if($value->status == 0)
							<a href="{{route('selectfacebook',['id'=>$value->id])}}" style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: #ab6cb0;font-weight: 700;">chọn</a>
							@else
							<a href="{{route('selectfacebook',['id'=>$value->id])}}"style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: green;font-weight: 700;">Đã chọn</a>
							@endif
						</td>
					</tr>
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-md-6" >
		<div id="add_address">
			<div id="title_div">
				<p id="title_div">Sửa địa chỉ "{{$value->ten}}"</p>	
				<a id="back" href="{{route('getfacebook')}}">Quay lại</a>
			</div>
			<form method="post" action="{{route('postEditfacebook')}}">
				@csrf
				<label>Tên</label>
				<input type="text" value="{{$value->ten}}" class="form-control" name="ten" required="required" placeholder="Tên ...">
				<label>Url</label>
				<input type="url" value="{{$value->url}}" class="form-control" name="url" required="required" placeholder="Địa chỉ ...">
				<input type="hidden" name="id" value="{{$value->id}}">			
				<input id="add" type="submit" name="add" value="Sửa địa chỉ">	
			</form>
			@endforeach
		</div>
	</div>
</div>
@endsection('content')