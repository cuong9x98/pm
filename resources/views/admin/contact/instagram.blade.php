@extends('admin.master')
@section('title','Danh mục')
@section('content')
<div class="row">
	<div class="col-md-6">
		<div id="list_address">
			<p id="title">Danh sách địa chỉ instagram</p>
			<table id="list_address" border="1">
				<thead>
					<td>STT</td>
					<td>Tên</td>
					<td>Url</td>
					<td>Trạng thái</td>
				</thead>
				<tbody>
					<?php $i=1 ?>
					@if(count($instagram) > 0)
					@foreach($instagram as $value)
					<tr>
						<td class="value">{{$i++}}</td>
						<td class="value">{{$value->ten}}</td>
						<td class="value">{{$value->url}}</td>
						<td class="value">
							<a id="edit" href="{{route('getEditinstagram',['id'=>$value->id])}}"><i class="fas fa-pencil-alt"></i></a> | 
							<a id="edit" href="{{route('destroyinstagram',['id'=>$value->id])}}"><i class="fas fa-trash"></i></a> | 
							@if($value->status == 0)
							<a href="{{route('selectinstagram',['id'=>$value->id])}}" style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: #ab6cb0;font-weight: 700;">chọn</a>
							@else
							<a href="{{route('selectinstagram',['id'=>$value->id])}}"style="border: 1px solid;padding: 0px 4px;border-radius: 5px;color: white;background-color: green;font-weight: 700;">Đã chọn</a>
							@endif
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="5"> Không có dữ liệu</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>	
	</div>
	<div class="col-md-6" >
		<div id="add_address">
			<p>Thêm địa chỉ</p>
			<form method="post" action="{{route('postinstagram')}}">
				@csrf
				<label>Tên</label>
				<input type="text" class="form-control" name="ten" required="required" placeholder="Tiêu đề ...">
				<label>Url</label>
				<input type="url" class="form-control" name="url" required="required" placeholder="Địa chỉ ...">			
				<input id="add" type="submit" name="add" value="Thêm địa chỉ">	
			</form>
		</div>
	</div>
</div>
@endsection('content')