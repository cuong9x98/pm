<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
	<title>@yield('title')</title>
	<base href="{{asset('web')}}/">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Pooled Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link rel="stylesheet" href="css/morris.css" type="text/css"/>
	<!-- Graph CSS -->
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- jQuery -->
	<script src="js/jquery-3.4.1.min.js"></script>
	<!-- //jQuery -->
	<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'/>
	<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<!-- lined-icons -->
	<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
	<!-- //lined-icons -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/dia_chi.css">
	<link rel="stylesheet" type="text/css" href="css/banner.css">
	<script src="js/banner.js" type="text/javascript"></script>
	<style type="text/css">
		li.dropdown.head-dpdn {
			margin-left: 22px;
		}
		table.tbl_order{
			width: 98%;
			border: 2px ;
			margin: auto;
		}
		h3#title{
			text-align: center;
			font-size: 40px;
			margin: 30px 0; 
		}
		div.link{
			background-color: #c8c7c7;
			width: 100%;
			height:50px; 
			line-height: 50px;
			padding-left: 20px;
			color: black
		}
		a.link{
			color: black
		}
		/*strong{
			padding: 20px 200px;
			color: white;
			background-color: #d76161;
			border-radius: 10px;
			}*/
			img#img_prod{
				width: 60px;
				height: 60px
			}
			td.title{
				font-weight: 900;
				font-size: 16px
			}
			.fa-pencil-alt{
				color: green
			}
			.fa-trash{
				color: red
			}
			input.status{
				width: 190px;
				text-align: center;
				padding: 12px;
				margin: auto;
				background-color: #fd8c8c;
				color: white;
				border-radius: 10px;
			}
			input.status-yes{
				width: 190px;
				text-align: center;
				padding: 12px;
				margin: auto;
				background-color: green;
				color: white;
				border-radius: 10px;
				border: 1px solid white;
			}
			div.status-yes{
				width: 190px;
				text-align: center;
				padding: 12px;
				margin: auto;
				background-color: green;
				color: white;
				border-radius: 10px;
			}
			div.status{
				width: 190px;
				text-align: center;
				padding: 12px;
				margin: auto;
				background-color: #fd8c8c;
				color: white;
				border-radius: 10px;
			}
			.w3layouts-right {
				background: #fff;
			}
			li.dropdown.head-dpdn i {
				color: #000;
			}
			i.fa.fa-angle-down {
				color: #000 !important;
			}
			.profile_details_drop.open .fa.fa-angle-up {
				display: block;
				color: #000 !important;
			}
		</style>
		@yield('css')
		<script type="text/javascript" src="ckeditor/ckeditor.js"></script>      	                                     
		<script type="text/javascript">
			var id = {{Auth::User()->id}};
			var role = {{Auth::User()->role}};
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			$(document).ready(function(){
				// phân quyền
				if(role == 2){
					var url = "{{route('notify_order')}}";
					var url_blog = "{{route('notify_blog')}}";
					var url_product = "{{route('notify_product')}}";
					$("a.notifi").attr('href','{{route('getListOrder',['id'=>Auth::User()->id])}}');			
				}
				if(role == 4){
					var url_product = "{{route('notify_product')}}";
					$("a.notifi").attr('href','{{route('getListOrderSale',['id'=>Auth::User()->id])}}');		
				}
				if(role == 7){
					var url = "{{route('notify_order_keToan')}}";
					$("a.notifi").attr('href','{{ route('orderkeToan',['id'=>Auth::User()->id])}}');		
				}
				if(role == 3){
					var url_blog = "{{route('notify_blog')}}";
					var url_product = "{{route('notify_product')}}";
					$("a.notifi").attr('href','{{route('getListOrder',['id'=>Auth::User()->id])}}');			
				}
				if(role == 5){
					var url_blog = "{{route('notify_blog')}}";
					$("a.notifi").attr('href','{{route('getListOrder',['id'=>Auth::User()->id])}}');			
				}
				if(role == 6){
					var url = "{{route('notify_order_sale')}}";
					$("a.notifi").attr('href','{{route('getListOrderSale',['id'=>Auth::User()->id])}}');	
				}
				// ajax thông báo đơn hàng mới
				if(role == 2 || role == 3){
					$.ajaxSetup({
						headers : {
							'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url,
						type : "POST",
						cache: false,
						data : {_token:CSRF_TOKEN,id:id},
						success:function(data){ 
						},error:function(error){
							// alert("Thêm thất bại");
						}
					});  		
				}
				if(role == 6){
					$.ajaxSetup({
						headers : {
							'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url,
						type : "POST",
						cache: false,
						data : {_token:CSRF_TOKEN,id:id},
						success:function(data){ 
						},error:function(error){
							// alert("Thêm thất bại");
						}
					});  		
				}
				if(role == 7){
					$.ajaxSetup({
						headers : {
							'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url,
						type : "POST",
						cache: false,
						data : {_token:CSRF_TOKEN,id:id},
						success:function(data){ 
							console.log(data);
						},error:function(error){
							// alert("Thêm thất bại");
						}
					});  		
				}			
				//ajax thông báo đơn tin tức mới
				if(role == 2 || role == 3){
					$.ajaxSetup({
						headers : {
							'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url_blog,
						type : "POST",
						cache: false,
						data : {_token:CSRF_TOKEN,id:id},
						success:function(data){ 
							console.log(data)
						},error:function(error){
							// alert("Thêm thất bại");
						}
					});
				}
				// ajax thông báo sản phẩm mới
				if(role == 2 || role == 3 || role == 4){
					$.ajaxSetup({
						headers : {
							'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url_product,
						type : "POST",
						cache: false,
						data : {_token:CSRF_TOKEN,id:id},
						success:function(data){ 
							console.log(data)
						},error:function(error){
							// alert("Thêm thất bại");
						}
					});
				}
			});  
		</script> 
	</head> 
	<body>
		<div class="page-container">
			<!--/content-inner-->
			<div class="left-content">

				<div class="mother-grid-inner">
					<!--header start here-->
					<div class="header-main">
						<div class="logo-w3-agile">
							<h1><a href="{{route('getAdHome')}}">LOGO</a></h1>
						</div>
						<div class="w3layouts-right">
							<div class="profile_details_left"><!--notifications of menu start -->
								<ul class="nofitications-dropdown">
									<li class="dropdown head-dpdn" style="float: right;">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i>
											@if(Auth::User()->unreadnotifications->count() > 0)
											<span class="badge blue">{{Auth::User()->unreadnotifications->count()}}</span>
											@endif
										</a>
										<ul class="dropdown-menu">
											<li>
												<div class="notification_header">
													<h3>Bạn có {{Auth::User()->unreadnotifications->count()}} thông báo mới</h3>
												</div>
											</li>
											<li><a href="#">
												@foreach(Auth::User()->notifications as $value)
												@if($value->read_at == null)
												<a class="notifi click" id="{{$value->id}}" href="#"><span style="color: red;" id="{{$value->id}}">{{$value->data['letter']['title']}}</span><br></a>
												@else
												<a class="notifi" href="#">{{$value->data['letter']['title']}}<br></a>
												@endif
												@endforeach
												<div class="clearfix"></div>	
												<script type="text/javascript">		
													var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
													$(document).ready(function(){ 
														$("a.click").click(function(){
															var id = $(this).attr('id');	
															$.ajaxSetup({
																headers : {
																	'X-CSRF-TOKEN' :  $('meta[name="csrf-token"]').attr('content')
																}
															});
															$.ajax({
																url : "{{route('read_notification')}}",
																type : "POST",
																cache: false,
																data : {_token:CSRF_TOKEN,id:id},
																success:function(data){ 
																},error:function(error){
																	// alert("Thêm thất bại");
																}
															});	                                       
														});	          
													})
												</script>
											</a></li>
											<li>
												<div class="notification_bottom">
													<a href="{{route('read_all_cart_admin')}}">Đọc tất cả</a>
												</div> 
											</li>
										</ul>
									</li>
									<div class="clearfix"> </div>
								</ul>
								<div class="clearfix"> </div>
							</div>
							<!--notification menu end -->

							<div class="clearfix"> </div>				
						</div>
						<div class="profile_details w3l" style="width: 45.25%;height: 70px;line-height: 57px;background-color: white;">		
							<ul>
								<li class="dropdown profile_details_drop">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<div class="profile_img">	
											<span class="prfil-img">
												@if(Auth::User()->img!=NULL)
												<img style="border: 2px solid gray; padding: 1px" src="../../public/uploads/img_user/{{ Auth::User()->img }}" alt=""> 
												@else
												<img style="border: 2px solid gray; padding: 1px" src="../../public/uploads/img_user/unnamed.jpg" alt=""> 
												@endif
											</span> 
											<p style="color: black;display: inline-block;font-size: 22px;float: left;margin-left: 30px;">{{Auth::User()->name}}</p>
											<p style="color: black;display: inline-block;float: left;margin-left: 20px;font-size: 14px;"><i>( {{Auth::User()->username}} )</i></p>
											<i class="fa fa-angle-down"></i>
											<i class="fa fa-angle-up"></i>
											<div class="clearfix"></div>
										</div>										
									</a>
									<ul class="dropdown-menu drp-mnu" style="float: right;margin-left: 333px;">
										<li> <a href="{{route('LogoutIndex')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a> </li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="clearfix"> </div>	
					</div>
					<div class="validation-system">
						<div class="validation-form" >
							<!-- CONTENT -->
							@yield('content')
							<!-- END CONTENT -->

							<!-- script-for sticky-nav -->
							<script>
								$(document).ready(function() {
									var navoffeset=$(".header-main").offset().top;
									$(window).scroll(function(){
										var scrollpos=$(window).scrollTop(); 
										if(scrollpos >=navoffeset){
											$(".header-main").addClass("fixed");
										}else{
											$(".header-main").removeClass("fixed");
										}
									});

								});
							</script>
							<!-- /script-for sticky-nav -->
						</div>
					</div>
					<!--//content-inner-->
					<!--/sidebar-menu-->
					<div class="sidebar-menu">
						<header class="logo1">
							<span class="sidebar-icon" class="fa fa-bars"></span>
						</header>
						<div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
						<div class="menu">
							<ul id="menu" >
								<!-- Admin -->
								@if(Auth::User()->role == 1 || Auth::User()->role == 2)
								<li><a href="{{route('getAdHome')}}"><span>Trang chủ</span><div class="clearfix"></div></a></li>
								<li><a><span>Liên hệ</span><div class="clearfix"></div></a>
									<ul>
										<li><a href="{{route('getAddress')}}"><span>Địa chỉ</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getSĐT')}}"><span>Số điện thoại</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getEmail')}}"><span>Email</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getfacebook')}}"><span>Facebook</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getinstagram')}}"><span>Instagram</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('gettwitter')}}"><span>Twitter</span><div class="clearfix"></div></a></li>
									</ul>
								</li>
								<li><a href="{{route('getRole')}}"><span>Phân quyền</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getListOrder',['id'=>Auth::User()->id])}}"><span>Đơn hàng</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getAdCate')}}"><span>Danh mục</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getBanner')}}"><span>Banner</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('editMail')}}"><span>Chỉnh sửa mail</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getProductSell')}}"><span>Sản phẩm</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getBlogIndex')}}"><span>Blog</span><div class="clearfix"></div></a></li>
								<li><a><span>Chính sách</span><div class="clearfix"></div></a>
									<ul>
										<li><a href="{{route('getBaoHanh')}}"><span>Bảo hành</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getCaiDat')}}"><span>Cài đặt</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getNangCap')}}"><span>Nâng cấp</span><div class="clearfix"></div></a></li>
										<li><a href="{{route('getGiaHan')}}"><span>Gia hạn</span><div class="clearfix"></div></a></li>
									</ul>
								</li>
								@endif		
								@if(Auth::User()->role == 3)								
								<li><a href="{{route('getAdHome')}}"><span>Trang chủ</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getProductSell')}}"><span>Sản phẩm</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getBlogIndex')}}"><span>Blog</span><div class="clearfix"></div></a></li>
								<li><a href="{{route('getAdCate')}}"><span>Danh mục</span><div class="clearfix"></div></a></li>
								@endif	
								@if(Auth::User()->role == 4)								
								<li><a href="{{route('SanPhamDaTao',['id'=>Auth::User()->id])}}"><span>Sản phẩm</span><div class="clearfix"></div></a></li>
								@endif	
								@if(Auth::User()->role == 5)								
								<li><a href="{{route('getBlogAdd',['id'=>Auth::User()->id])}}"><span>Tin tức</span><div class="clearfix"></div></a></li>
								@endif
								<!-- Sale -->
								@if(Auth::User()->role == 6)
								<li><a href="{{route('getAdHome')}}"><span>Trang chủ</span><div class="clearfix"></div></a></li>				
								<li><a href="{{route('getListOrderSale',['id'=>Auth::User()->id])}}"><span>Đơn hàng</span><div class="clearfix"></div></a></li>
								@endif
								<!-- Kế toán -->
								@if(Auth::User()->role == 7)
								<li><a href="{{route('getAdHome')}}"><span>Trang chủ</span><div class="clearfix"></div></a></li>			
								<li><a href="{{route('orderkeToan',['id'=>Auth::User()->id])}}"><span>Đơn hàng</span><div class="clearfix"></div></a></li>
								@endif
								<li><a href="{{route('getProfile',['id'=>Auth::User()->id])}}"><span>Thông tin cá nhân</span><div class="clearfix"></div></a></li>	
							</ul>
						</div>
					</div>
					<div class="clearfix"></div>
					<div id="fix"></div>		
				</div>
				<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.0.0-alpha/Chart.min.js"></script>
				<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
				<script type="text/javascript" src="js/dia_chi.js"></script>
				<script>
					var toggle = true;
					$(".sidebar-icon").click(function() {                
						if (toggle)
						{
							$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
							$("#menu span").css({"position":"absolute"});
						}
						else
						{
							$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
							setTimeout(function() {
								$("#menu span").css({"position":"relative"});
							}, 400);
						}

						toggle = !toggle;
					});
				</script>
				<!--js -->
				<script src="js/jquery.nicescroll.js"></script>
				<script src="js/scripts.js"></script>
				<!-- Bootstrap Core JavaScript -->
				<script src="js/bootstrap.min.js"></script>
				<!-- /Bootstrap Core JavaScript -->	   
				<!-- morris JavaScript -->	
				<script src="js/raphael-min.js"></script>
				<script src="js/morris.js"></script>
				<script>
					$(document).ready(function() {
		//BOX BUTTON SHOW AND CLOSE
		jQuery('.small-graph-box').hover(function() {
			jQuery(this).find('.box-button').fadeIn('fast');
		}, function() {
			jQuery(this).find('.box-button').fadeOut('fast');
		});
		jQuery('.small-graph-box .box-close').click(function() {
			jQuery(this).closest('.small-graph-box').fadeOut(200);
			return false;
		});

	    //CHARTS
	    function gd(year, day, month) {
	    	return new Date(year, month - 1, day).getTime();
	    }

	    graphArea2 = Morris.Area({
	    	element: 'hero-area',
	    	padding: 10,
	    	behaveLikeLine: true,
	    	gridEnabled: false,
	    	gridLineColor: '#dddddd',
	    	axes: true,
	    	resize: true,
	    	smooth:true,
	    	pointSize: 0,
	    	lineWidth: 0,
	    	fillOpacity:0.85,
	    	data: [
	    	{period: '2014 Q1', iphone: 2668, ipad: null, itouch: 2649},
	    	{period: '2014 Q2', iphone: 15780, ipad: 13799, itouch: 12051},
	    	{period: '2014 Q3', iphone: 12920, ipad: 10975, itouch: 9910},
	    	{period: '2014 Q4', iphone: 8770, ipad: 6600, itouch: 6695},
	    	{period: '2015 Q1', iphone: 10820, ipad: 10924, itouch: 12300},
	    	{period: '2015 Q2', iphone: 9680, ipad: 9010, itouch: 7891},
	    	{period: '2015 Q3', iphone: 4830, ipad: 3805, itouch: 1598},
	    	{period: '2015 Q4', iphone: 15083, ipad: 8977, itouch: 5185},
	    	{period: '2016 Q1', iphone: 10697, ipad: 4470, itouch: 2038},
	    	{period: '2016 Q2', iphone: 8442, ipad: 5723, itouch: 1801}
	    	],
	    	lineColors:['#ff4a43','#a2d200','#22beef'],
	    	xkey: 'period',
	    	redraw: true,
	    	ykeys: ['iphone', 'ipad', 'itouch'],
	    	labels: ['All Visitors', 'Returning Visitors', 'Unique Visitors'],
	    	pointSize: 2,
	    	hideHover: 'auto',
	    	resize: true
	    });
	});
</script>
@yield('javascript')
</div >
</div>
</body>
</html>