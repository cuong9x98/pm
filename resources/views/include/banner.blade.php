        <!-- mega slide -->
<style>
    .carousel-control-prev{
        left: 110px !important;
    }
    .slide__text{
        left: 15% !important;
    }
    @media screen and (max-width:767px){
        .slide__text{
            left: 5% !important;
        }
        .carousel-control-prev{
            left: 0px !important;
        }
    }
</style>


        <div class="container-fluid mega__slide">
            <div class="row">
               
                <div class="col-md-12">
                    <div id="carouselExampleControls" class="carousel slide mega__slide__box" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="slide__text">
                                    <p class="slide__text--top">
                                        
                                    </p>
                                    <h3 class="slide__text--title">
                                        
                                    </h3>
                                    <p class="slide__text--price">
                                        Chỉ từ
                                        <span class="slide__text--price--dolar">Dưới 10 triệu <sup>99</sup></span>
                                    </p>
                                    <a href="" class="btn slide__text--btn">
                                        Xem ngay bây giờ>
                                    </a>
                                </div>
                                <img src="" class="d-block" alt="." />
                            </div>
                            @foreach($banner as $ban)
                            <div class="carousel-item">
                                <div class="slide__text">
                                    <p class="slide__text--top">
                                        
                                    </p>
                                    <h3  class="slide__text--title">
                                        <p direction="right">{{$ban->tilte}}</p>
                                    </h3>
                                    <p class="slide__text--price">
                                        Chỉ từ
                                        <span class="slide__text--price--dolar">Dưới 10 triệu <sup>99</sup></span>
                                    </p>
                                    <a href="" class="btn slide__text--btn">
                                        Xem ngay bây giờ >
                                    </a>
                                </div>
                                <img src="../../public/uploads/banner/{{$ban->img}}" class="d-block w-100" alt="..." />
                            </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next next-img" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Banner grid -->
        <div class="container">
            <div class="banner__grid row">
                <div class="banner__grid--item col-md-3 col-6">
                    <div class="banner__item--icon">
                        <i class="fa fa-truck banner--icon"></i>
                    </div>
                    <div class="banner__box--text">
                        <h3 class="banner__text--title">
                            Chi phí vận chuyển
                        </h3>
                        <p class="banner__text--detail">
                            Miễn phí
                        </p>
                    </div>
                </div>
                <div class="banner__grid--item col-md-3 col-6">
                    <div class="banner__item--icon">
                        <i class="fa fa-truck banner--icon"></i>
                    </div>
                    <div class="banner__box--text">
                        <h3 class="banner__text--title">
                            Chi phí vận chuyển
                        </h3>
                        <p class="banner__text--detail">
                            Miễn phí
                        </p>
                    </div>
                </div>
                <div class="banner__grid--item col-md-3 col-6">
                    <div class="banner__item--icon">
                        <i class="fa fa-truck banner--icon"></i>
                    </div>
                    <div class="banner__box--text">
                        <h3 class="banner__text--title">
                            Chi phí vận chuyển
                        </h3>
                        <p class="banner__text--detail">
                            Miễn phí
                        </p>
                    </div>
                </div>
                <div class="banner__grid--item col-md-3 col-6">
                    <div class="banner__item--icon">
                        <i class="fa fa-truck banner--icon"></i>
                    </div>
                    <div class="banner__box--text">
                        <h3 class="banner__text--title">
                            Chi phí vận chuyển
                        </h3>
                        <p class="banner__text--detail">
                            Miễn phí
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner column -->
        <div class="container">
            <div class="row banner__column">
                @foreach ($banner_con as $ban)
                <div class="col-md-4 banner__column-item">
                    <a href="" class="banner__column-item--link">
                        <img src="../../public/uploads/banner/{{$ban->img}}" class="img-fluid banner__column-item--img" alt="banner product" />
                    </a>
                </div>
                @endforeach
            </div>
        </div>