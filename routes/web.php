
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Thông báo đơn hàng mới 
// Duyệt đơn hàng
//  

Route::group(['namespace'=>'client'],function(){

	//Home

	Route::get('/','HomeController@getHome')->name('getHome');
	Route::get('/test','HomeController@getTest')->name('getTest');
	Route::get('/san-pham-cua-toi','HomeController@mylove')->name('mylove');
	Route::post('/test','HomeController@postTest')->name('postTest');
	Route::get('/san-pham/{id}','DetailproductController@getDetailproduct')->name('getDetailproduct');
	Route::post('/san-pham','DetailproductController@postComment')->name('postComment');
	Route::get('/don-hang-cua-ban','HomeController@myorder')->name('myorder');
	Route::get('/xoa-don-hang-cua-ban','HomeController@xoa_mylove')->name('xoa_mylove');
	Route::get('/cap-nhat-don-hang-cua-ban','HomeController@update_mylove')->name('update_mylove');
	Route::get('/san-pham-ban-yeu-thich','DetailproductController@add_love')->name('add_love');
	//Ajax index
	Route::get('/gio-hang-index-sale','HomeController@cart_index_sale')->name('cart_index_sale');
	Route::get('/yeu-thich-index-sale','HomeController@love_index_sale')->name('love_index_sale');
	
	Route::get('/doc-1-thong-bao/{id}','notification_clientController@read_notifi_client')->name('read_notifi_client')->middleware('CheckLogoutUser');
	Route::get('doc-thong-bao',function(){
		\Auth::User()->notifications->markAsRead();
		return redirect()->route('getHome');
	})->name('read_all');
	//Home	
	Route::get('/','HomeController@getHome')->name('getHome')->middleware('CheckLogoutUser');
	Route::get('/test','HomeController@getTest')->name('getTest')->middleware('CheckLogoutUser');
	Route::get('/san-pham-cua-toi','HomeController@mylove')->name('mylove')->middleware('CheckLogoutUser');
	Route::post('/test','HomeController@postTest')->name('postTest')->middleware('CheckLogoutUser');
	Route::get('/san-pham/{id}','DetailproductController@getDetailproduct')->name('getDetailproduct')->middleware('CheckLogoutUser');
	Route::post('/san-pham','DetailproductController@postComment')->name('postComment')->middleware('CheckLogoutUser');
	Route::get('/don-hang-cua-ban','HomeController@myorder')->name('myorder')->middleware('CheckLogoutUser');
	Route::get('/xoa-don-hang-cua-ban','HomeController@xoa_mylove')->name('xoa_mylove')->middleware('CheckLogoutUser');
	Route::get('/cap-nhat-don-hang-cua-ban','HomeController@update_mylove')->name('update_mylove')->middleware('CheckLogoutUser');
	Route::get('/san-pham-ban-yeu-thich','DetailproductController@add_love')->name('add_love')->middleware('CheckLogoutUser');
	// Route::get('/tim-kiem','HomeController@getsearch')->name('getsearch')->middleware('CheckLogoutUser');
	Route::post('/tim-kiem','HomeController@postsearch')->name('postsearch')->middleware('CheckLogoutUser');

	//Cart
	Route::get('/gio-hang','CartController@showCart')->name('showCart')->middleware('CheckLogoutUser');
	Route::post('/gio-hang','CartController@getCart')->name('getCart')->middleware('CheckLogoutUser');
	Route::get('/xoa-gio-hang/{id}','CartController@destroy')->name('deleteCart')->middleware('CheckLogoutUser');
	Route::post('/sua-gio-hang','CartController@update')->name('updateCart')->middleware('CheckLogoutUser');
	Route::get('/don-hang','CartController@index')->name('indexCart')->middleware('CheckLogoutUser');
	Route::get('/cap-nhat-gio-hang','Ajax_edit_cartController@update')->name('editCart')->middleware('CheckLogoutUser');
	Route::get('/xoa-gio-hang','Ajax_edit_cartController@delete')->name('xoaCart')->middleware('CheckLogoutUser');
	Route::get('/them-gio-hang','Ajax_edit_cartController@themCart')->name('themCart')->middleware('CheckLogoutUser');
	// Route::post('/don-hang','CartController@store')->name('insertCart')->middleware('CheckLogoutUser');
	Route::resource('order','CartController')->middleware('CheckLogoutUser');
	//Checkout
	Route::get('/dang-nhap-check','CheckoutController@login_checkout')->name('login_check')->middleware('CheckLogoutUser');
	//Login

	Route::get('/dang-nhap','UserController@getLoginUser')->name('getLoginUser');
	Route::post('/dang-nhap','UserController@postLoginUser')->name('postLoginUser');
	Route::get('/quen-mat-khau','UserController@forget')->name('forget');
	Route::get('/getInfo-facebook/{social}','SocialController@getInfoFacebook')->name('getInfoFacebook');
	Route::get('/check-info-facebook/{social}','SocialController@checkInfoFacebook')->name('checkInfoFacebook');
	Route::get('/login-facebook','UserController@login_facebook');
	Route::get('/dang-nhap/callback','UserController@callback_facebook');	
	Route::post('/dang-nhap/email','UserController@getpassword')->name('getpassword');
	Route::get('/dang-nhap/newpass/','UserController@newpass')->name('newpass');
	Route::post('/dang-nhap/postnewpass/','UserController@postnewpass')->name('postnewpass');


	//Logout
	Route::get('/dang-xuat','UserController@getLogoutUser')->name('getLogoutUser');
	//register
	Route::get('/dang-ky','UserController@create')->name('getAddUser')->middleware('CheckLogoutUser');
	Route::get('/xac-thuc-tai-khoan/{id}','UserController@xacthuc_user')->name('xacthuc_user')->middleware('CheckLogoutUser');
	Route::post('/dang-ky','UserController@postcreate')->name('postAddUser')->middleware('CheckLogoutUser');
	Route::get('/kiem-tra','UserController@getcheckout')->name('checkout')->middleware('CheckLogoutUser');
	//category
	Route::group(['namespace'=>'category'],function(){			
		Route::get('/danh-muc/{id}','CategoryController@show')->name('getCate')->middleware('CheckLogoutUser');
		Route::get('/count_prod_by_cate','ajax_count_prod_by_cateController@index')->name('count_prod_by_cate')->middleware('CheckLogoutUser');
	});
	//post quen huyen
	Route::post('/client_post_quan_huyen','Ajax_get_addressController@post_Quan_huyen')->name('post_quan_huyen')->middleware('CheckLogoutUser');
	Route::post('/client_post_phuong_xa','Ajax_get_addressController@post_Phuong_Xa')->name('post_phuong_xa')->middleware('CheckLogoutUser');

	//Blog
	Route::get('/tin-tuc','BlogController@getBlog')->name('getBlog')->middleware('CheckLogoutUser');
	Route::post('/binh-luan','BlogController@postcomment_blog')->name('postcomment_blog')->middleware('CheckLogoutUser');
	Route::get('/xem-blog/{id}','BlogController@getDetailBlog')->name('getDetailBlog')->middleware('CheckLogoutUser');
	//Chinh sach
	Route::get('/xem-chinh-sach','PolicyController@getPolicy')->name('getPolicy')->middleware('CheckLogoutUser');
	Route::get('/xem-bao-hanh','PolicyController@baohanh')->name('baohanh')->middleware('CheckLogoutUser');
	Route::get('/xem-cai-dat','PolicyController@caidat')->name('caidat')->middleware('CheckLogoutUser');
	Route::get('/xem-nang-cap','PolicyController@nangcap')->name('nangcap')->middleware('CheckLogoutUser');
	//Lien He
	Route::post('/post-lien-he','ContactController@lienhe')->name('lienhe');
	Route::get('/lien-he','ContactController@getContact')->name('getContact')->middleware('CheckLogoutUser');

	

	//Payment
	Route::get('/thanh-toan','PaymentControler@getPayment')->name('getPayment')->middleware('CheckLogoutUser');
	Route::group(['middleware' => ['web']], function () {
		Route::post('/thanh-toan','PaymentControler@postPayment')->name('postPayment')->middleware('CheckLogoutUser');	
	});
	

	//Ajax index
	Route::get('/gio-hang-index-sale','HomeController@cart_index_sale')->name('cart_index_sale');
	Route::get('/yeu-thich-index-sale','HomeController@love_index_sale')->name('love_index_sale');
	Route::get('/goi-y-tim-kiem','HomeController@autocomplete')->name('autocomplete');
	//Chinh sach

	
	
});
Route::group(['namespace'=>'admin'],function(){
	Route::group(['prefix'=>'admin'],function(){	
		Route::get('/','AdhomeController@get_list_category')->name('getAdHome')->middleware('CheckLogout');
		Route::post('tim-kiem','AdhomeController@search')->name('search')->middleware('CheckLogout');
		Route::post('tim-kiem-doanh-thu','AdhomeController@searchDoanhThu')->name('searchDoanhThu')->middleware('CheckLogout');
		Route::get('sua-mail','MailController@editMail')->name('editMail')->middleware('CheckLogout');
		Route::get('raw','GraficasController@raw')->name('raw')->middleware('CheckLogout');
		Route::post('ajax-doanh-thu','AdhomeController@AjaxDoanhThu')->name('AjaxdoanhThu')->middleware('CheckLogout');
		Route::post('ajax-doanh-thu-thang','AdhomeController@getDoanhThuThang')->name('getDoanhThuThang')->middleware('CheckLogout');
		//order
		Route::group(['namespace'=>'order'],function(){	
			# Hiển thị danh sách đơn hàng mới admin
			Route::get('/don-hang-moi/{id}','OrderController@show')->name('getListOrder')->middleware('CheckLogout');
			Route::get('/don-hang-da-giao/{id}','OrderController@don_hang_giao')->name('don_hang_giao')->middleware('CheckLogout');
			# Hiển thị danh sách sale
			Route::post('/get-list-sale','OrderController@getListSale')->name('getListSale')->middleware('CheckLogout');
			# Chọn sale duyệt
			Route::post('/selectSale','OrderController@selectSale')->name('selectSale')->middleware('CheckLogout');
			# Hiển thị danh sách đơn hàng của sale	
			Route::get('/don-hang-sale/{id}','order_saleController@getListOrderSale')->name('getListOrderSale')->middleware('CheckLogout');
			# Hiển thị đơn hàng chi tiết admin
			Route::post('/chi-tiet-don-hang/{id}','OrderController@getListDetailOrder')->name('getListDetailOrderAdmin')->middleware('CheckLogout');
			# Hiển thị đơn hàng chi tiết sale
			Route::post('/chi-tiet-don-hang-sale/{id}','order_saleController@getListDetailOrder')->name('getListDetailOrder')->middleware('CheckLogout');
			# Hiển thị đơn hàng chi tiết keToan
			Route::post('/chi-tiet-don-hang-keToan/{id}','order_keToanController@getListDetailOrder')->name('getListDetailOrderKeToan')->middleware('CheckLogout');
			# Thay đổi commissions
			Route::post('/change-commissions','OrderController@changeCommissions')->name('changeCommissions')->middleware('CheckLogout');
			# Lấy tổng tiền của sale của đơn hàng
			Route::post('/commissions-sale','order_saleController@orderCommissions')->name('orderCommissions')->middleware('CheckLogout');
			# đơn hàng bên kế toán
			Route::get('/don-hang-ke-toan/{id}','order_keToanController@orderkeToan')->name('orderkeToan')->middleware('CheckLogout');
			# thông báo đơn hàng mới admin
			Route::post('/notify_cart_admin','notificationController@notify_order')->name('notify_order')->middleware('CheckLogout');
			# thông báo đơn hàng mới customer
			Route::post('/notify_cart_customer','notificationController@notify_customer')->name('notify_customer')->middleware('CheckLogout');
			# thông báo đơn hàng mới sale
			Route::post('/notify_cart_sale','notificationController@notify_order_sale')->name('notify_order_sale')->middleware('CheckLogout');
			# thông báo đơn hàng mới kế toán
			Route::post('/notify_cart_ke_toan','notificationController@notify_order_keToan')->name('notify_order_keToan')->middleware('CheckLogout');
			Route::post('/notify_khach','notificationController@notify_customer')->name('notify_customer');
			# Hiển thị đơn hàng đã chốt sale 
			Route::get('/don-hang-chot-sale/{id}','order_saleController@order_confirm')->name('order_confirm')->middleware('CheckLogout');
			# Thay đổi trạng thái đơn hàng chi tiết
			Route::post('/change-order-detail','order_saleController@change_order_detail')->name('change_order_detail')->middleware('CheckLogout');
			# Sale xác nhận đơn hàng
			Route::post('/sale-confirm','order_saleController@confirmOrder')->name('confirmOrder')->middleware('CheckLogout');
			# đọc thông báo
			Route::post('/read_notification','notificationController@read_notification')->name('read_notification')->middleware('CheckLogout');
			# đọc tất cả thông báo
			Route::get('/read_all_cart_admin',function(){
				\Auth::User()->notifications->markAsRead();
				return back();
			})->name('read_all_cart_admin')->middleware('CheckLogout');



			# Gửi mail khi duyệt đơn
			Route::get('send-mail','Send_mailController@sendEmailToUser')->name('send_mail_order')->middleware('CheckLogout');
		});
		//login
		Route::group(['namespace'=>'login'],function(){			
			Route::get('login','LoginController@getLogin')->name('getLogin')->middleware('CheckLogin');
			Route::post('/login','LoginController@postLogin')->name('postLogin');
			Route::get('/logout','LogoutController@index')->name('LogoutIndex');
		});
		// category
		Route::group(['namespace'=>'category'],function(){	
			// Lấy danh mục được chọn		
			Route::post('/sua-danh-muc','Edit_categoryController@store')->name('editCate')->middleware('CheckLogout');
			// Lưu danh mục sau khi sửa
			Route::post('/luu-danh-muc','Edit_categoryController@update')->name('updateCate')->middleware('CheckLogout');
			Route::get('/danh-muc','AdaddcateController@getAdCate')->name('getAdCate')->middleware('CheckLogout');
			Route::post('/danh-muc','AdaddcateController@postAdCate')->name('postAdCate');
			Route::get('/del/{id}','Ad_del_categoryController@delAdCate')->name('delAdCate')->middleware('CheckLogout');	
		});
		//product
		Route::group(['namespace'=>'product'],function(){
			// Bên admin và editor
			Route::get('san-pham-dang-ban','Product_admin_Controller@SanPhamDangBan')->name('getProductSell')->middleware('CheckLogout');
			Route::get('san-pham-dang-doi-duyet/{id}','Product_admin_Controller@SanPhamDangDoiDuet')->name('getProdIndex')->middleware('CheckLogout');
			Route::get('san-pham-da-duyet/{id}','Product_admin_Controller@SanPhamDaDuyet')->name('getListconfirmProduct')->middleware('CheckLogout');
			Route::get('them-san-pham','ProductController@add')->name('addAdPro')->middleware('CheckLogout');
			Route::post('duyet','Product_admin_Controller@confirm')->name('ConfirmProduct')->middleware('CheckLogout');
			Route::post('get-tag','ajax_get_tag_photoController@getTag')->name('getTag')->middleware('CheckLogout');// lấy tag có sẵn (Nếu thêm chức năng)
			Route::get('gui-mail/{admin_confirm}/{name}','MailController@sendEmailToUser')->name('send-mail-product')->middleware('CheckLogout');
			Route::get('gui-mail-nguoi-tao/{creater}/{messages}','MailController@sendEmailToUserSell')->name('send-mail-product-sell')->middleware('CheckLogout');
			Route::post('thong-bao-san-pham-moi-cho-duyet','NotificationController@notifyAdmin')->name('notify_product')->middleware('CheckLogout');		
			// Bên ctv
			Route::get('sua-san-pham-da-tao/{id}','Product_ctv_Controller@SanPhamDaTao')->name('SanPhamDaTao')->middleware('CheckLogout');
			Route::get('sua-san-pham-cho-duyet/{id}','Product_ctv_Controller@SanPhamChoDuyet')->name('SanPhamChoDuyet')->middleware('CheckLogout');
			Route::get('thong-bao-san-pham-duoc-duyet/{id}','NotificationController@sendNotifiToUserSell')->name('sendNotifiToUserSell')->middleware('CheckLogout');			
			// Cả hai
			Route::post('them-san-pham','ProductController@postAdd')->name('postaddAdPro')->middleware('CheckLogout');
			Route::get('xoa-san-pham/{id}','ProductController@destroy')->name('getProddestroy')->middleware('CheckLogout');
			Route::get('sua-san-pham/{id}','ProductController@edit')->name('getProdedit')->middleware('CheckLogout');
			Route::post('sua-san-pham/{id}','ProductController@postEdit')->name('postProdupdate')->middleware('CheckLogout');
			Route::get('chi-tiet-san-pham/{id}','ProductController@detail')->name('detailProduct')->middleware('CheckLogout');
			Route::get('doc-tat-ca-thong-bao',function(){
				\Auth::User()->notifications->markAsRead();
				return back();
			})->name('read_all_cart_admin')->middleware('CheckLogout');
		});
		// blog
		Route::group(['namespace'=>'blog'],function(){		
			// Bên admin và editor	
			Route::get('tin-tuc-da-dang','Blog_admin_Controller@TinTucDaDang')->name('getBlogIndex')->middleware('CheckLogout');
			Route::get('tin-tuc-cho-duyet/{id}','Blog_admin_Controller@TinTucChoDuyet')->name('getBlogIndexAdmin')->middleware('CheckLogout');
			Route::get('tin-tuc-da-duyet/{id}','Blog_admin_Controller@TinTucDaDuyet')->name('getBlogIndexAdmin_confirm')->middleware('CheckLogout');
			Route::get('thêm-tin-tuc','BlogController@add')->name('getAddBlogIndex')->middleware('CheckLogout');
			Route::post('thêm-tin-tuc','BlogController@postAdd')->name('postAddBlogStore')->middleware('CheckLogout');
			Route::get('sua-tin-tuc/{id}','BlogController@edit')->name('getEditBlogShow')->middleware('CheckLogout');
			Route::post('sua-tin-tuc/{id}','BlogController@postEdit')->name('postEditBlogUpdate')->middleware('CheckLogout');
			Route::get('xoa-tin-tuc/{id}','BlogController@destroy')->name('DeleteBlog')->middleware('CheckLogout');
			Route::post('/thong-bao-co-blog-moi','NotificationController@notifyBlogAdmin')->name('notify_blog')->middleware('CheckLogout');
			Route::post('duyet_blog','Blog_admin_Controller@duyetBlog')->name('ConfirmBlog')->middleware('CheckLogout');
			// Bên ctv
			Route::get('tin-tuc-da-them/{id}','Blog_ctv_Controller@TinTucDaThem')->name('getBlogAdd')->middleware('CheckLogout');
			Route::get('tin-tuc-cho-them/{id}','Blog_ctv_Controller@TinTucDoiDuyet')->name('TinTucDoiDuyet')->middleware('CheckLogout');
			// gửi mail cho người tạo đã duyệt blog
			Route::get('send-mail-blog-sell','Send_mail_blogController@sendEmailToUserBlog')->name('send-mail-blog-sell')->middleware('CheckLogout');
			# Gửi thông báo cho người tạo đã duyệt hoặc không duyệt
			Route::get('send_notify_confirm_blog/{id}/{check}','notification_blogController@sendNotifiToUserBlog')->name('sendNotifiToUserBlog')->middleware('CheckLogout');			
		});
		// user
		Route::group(['namespace'=>'user'],function(){			
			Route::get('/profile/{id}','Profile_2Controller@show')->name('getProfile')->middleware('CheckLogout');
			Route::post('/profile/{id}','Profile_2Controller@update')->name('postProfile')->middleware('CheckLogout');
			//Ajax lấy thông tin tỉnh
			Route::post('/postaddress','Ajax_get_addressController@postaddress')->name('postaddress')->middleware('CheckLogout');
			Route::post('/postXaPhuong','Ajax_get_addressController@postXaPhuong')->name('postXaPhuong')->middleware('CheckLogout');
			Route::post('/postQuanHuyen','Ajax_get_addressController@postQuanHuyen')->name('postQuanHuyen')->middleware('CheckLogout');
		});
		// nofication
		Route::group(['namespace'=>'nofication'],function(){			
			Route::get('thong-bao','noficationController@index')->name('getNofication')->middleware('CheckLogout');
		});
		// role
		Route::group(['namespace'=>'role'],function(){			
			Route::get('phan-quyen','roleController@index')->name('getRole')->middleware('CheckLogout');
			Route::get('quyen','roleController@show')->name('getRoleUser')->middleware('CheckLogout');
			Route::post('quyen','roleController@store')->name('updateRole')->middleware('CheckLogout');
			Route::post('tao-tai-khoan','roleController@create')->name('create')->middleware('CheckLogout');
		});
		// banner
		Route::group(['namespace'=>'banner'],function(){			
			Route::get('banner','BannerController@show')->name('getBanner')->middleware('CheckLogout');
			Route::post('banner','BannerController@add')->name('addBanner')->middleware('CheckLogout');
			Route::get('xoa-banner/{id}','BannerController@delete')->name('deleteBanner')->middleware('CheckLogout');
			Route::get('chon-banner/{id}','BannerController@selectBanner')->name('selectBanner')->middleware('CheckLogout');
			Route::get('chon-banner-nho/{id}','BannerController@selectBannerSmall')->name('selectBannerSmall')->middleware('CheckLogout');
		});

		// contact
		Route::group(['namespace'=>'contact'],function(){
			Route::get('dia-chi','ContactController@DiaChi')->name('getAddress')->middleware('CheckLogout');
			Route::post('them-dia-chi','ContactController@ThemDiaChi')->name('postAddress')->middleware('CheckLogout');
			Route::get('sua-dia-chi/{id}','ContactController@getEdit')->name('getEdit')->middleware('CheckLogout');
			Route::post('sua-dia-chi','ContactController@postEdit')->name('postEdit')->middleware('CheckLogout');
			Route::get('xoa/{id}','ContactController@destroy')->name('destroyDiaChi')->middleware('CheckLogout');
			Route::get('lua-chon/{id}','ContactController@selectAddress')->name('selectAddress')->middleware('CheckLogout');
			Route::get('so-dien-thoai','SoDienThoaiController@SoDienThoai')->name('getSĐT')->middleware('CheckLogout');
			Route::post('them-so-dien-thoai','SoDienThoaiController@ThemSoDienThoai')->name('postSĐT')->middleware('CheckLogout');
			Route::get('sua-so-dien-thoai/{id}','SoDienThoaiController@getEditSĐT')->name('getEditSĐT')->middleware('CheckLogout');
			Route::post('sua-so-dien-thoai','SoDienThoaiController@postEditSĐT')->name('postEditSĐT')->middleware('CheckLogout');
			Route::get('xoa-sdt/{id}','SoDienThoaiController@destroySĐT')->name('destroySĐT')->middleware('CheckLogout');
			Route::get('lua-chon-sdt/{id}','SoDienThoaiController@selectSĐT')->name('selectSĐT')->middleware('CheckLogout');
			Route::get('email','EmailController@Email')->name('getEmail')->middleware('CheckLogout');
			Route::post('them-email','EmailController@ThemEmail')->name('postEmail')->middleware('CheckLogout');
			Route::get('sua-email/{id}','EmailController@getEditEmail')->name('getEditEmail')->middleware('CheckLogout');
			Route::post('sua-email','EmailController@postEditEmail')->name('postEditEmail')->middleware('CheckLogout');
			Route::get('xoa-email/{id}','EmailController@destroyEmail')->name('destroyEmail')->middleware('CheckLogout');
			Route::get('lua-chon-email/{id}','EmailController@selectemail')->name('selectemail')->middleware('CheckLogout');
			Route::get('facebook','facebookController@facebook')->name('getfacebook')->middleware('CheckLogout');
			Route::post('them-facebook','facebookController@Themfacebook')->name('postfacebook')->middleware('CheckLogout');
			Route::get('sua-facebook/{id}','facebookController@getEditfacebook')->name('getEditfacebook')->middleware('CheckLogout');
			Route::post('sua-facebook','facebookController@postEditfacebook')->name('postEditfacebook')->middleware('CheckLogout');
			Route::get('xoa-face/{id}','facebookController@remove')->name('remove')->middleware('CheckLogout');
			Route::get('lua-chon-facebook/{id}','facebookController@selectfacebook')->name('selectfacebook')->middleware('CheckLogout');
			Route::get('instagram','instagramController@instagram')->name('getinstagram')->middleware('CheckLogout');
			Route::post('them-instagram','instagramController@Theminstagram')->name('postinstagram')->middleware('CheckLogout');
			Route::get('sua-instagram/{id}','instagramController@getEditinstagram')->name('getEditinstagram')->middleware('CheckLogout');
			Route::post('sua-instagram','instagramController@postEditinstagram')->name('postEditinstagram')->middleware('CheckLogout');
			Route::get('xoa-instagram/{id}','instagramController@destroyinstagram')->name('destroyinstagram')->middleware('CheckLogout');
			Route::get('lua-chon-instagram/{id}','instagramController@selectinstagram')->name('selectinstagram')->middleware('CheckLogout');
			Route::get('twitter','twitterController@twitter')->name('gettwitter')->middleware('CheckLogout');
			Route::post('them-twitter','twitterController@Themtwitter')->name('posttwitter')->middleware('CheckLogout');
			Route::get('sua-twitter/{id}','twitterController@getEdittwitter')->name('getEdittwitter')->middleware('CheckLogout');
			Route::post('sua-twitter','twitterController@postEdittwitter')->name('postEdittwitter')->middleware('CheckLogout');
			Route::get('xoa-twitter/{id}','twitterController@destroytwitter')->name('destroytwitter')->middleware('CheckLogout');
			Route::get('lua-chon-twitter/{id}','twitterController@selecttwitter')->name('selecttwitter')->middleware('CheckLogout');
		});		

		// Chính sách
Route::group(['namespace'=>'ChinhSach'],function(){			
	Route::get('bao-hanh','ChinhSachController@BaoHanh')->name('getBaoHanh')->middleware('CheckLogout');
	Route::post('bao-hanh','ChinhSachController@postBaoHanh')->name('postBaoHanh')->middleware('CheckLogout');
	Route::get('nang-cap','ChinhSachController@NangCap')->name('getNangCap')->middleware('CheckLogout');
	Route::post('nang-cap','ChinhSachController@postNangCap')->name('postNangCap')->middleware('CheckLogout');
	Route::get('gia-han','ChinhSachController@GiaHan')->name('getGiaHan')->middleware('CheckLogout');
	Route::post('gia-han','ChinhSachController@postGiaHan')->name('postGiaHan')->middleware('CheckLogout');
	Route::get('cai-dat','ChinhSachController@CaiDat')->name('getCaiDat')->middleware('CheckLogout');
	Route::post('cai-dat','ChinhSachController@postCaiDat')->name('postCaiDat')->middleware('CheckLogout');
});
});
});
