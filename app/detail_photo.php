<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_photo extends Model
{
    protected $table = 'detail_photo';

    protected $fillable = [ // Tăng tính bảo mật cho web. Chỉ đc truyền 2 cái này 
    	'img',
    	'id_prod'
    ];

    protected $casts = [ // Gán kiểu cho biến img thành mảng
    	'img' => 'array'
    ];
}
