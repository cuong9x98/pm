<?php

namespace App\Http\Controllers\admin\contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\facebook;
class FacebookController extends Controller
{
    //
    public function facebook(){
    	// css address
		$facebook = facebook::all();
		return view("admin.contact.facebook",['facebook'=>$facebook]);
	}
	public function Themfacebook(Request $Request){
		DB::table('facebook')->insert([
			'ten'=>$Request->ten,
			'url'=>$Request->url,
			'created_at'=>Carbon::now()
		]);
		return back();
	}
	public function getEditfacebook($id){
		$facebook = facebook::where('id',$id)->get();
		return view("admin.contact.edit_facebook",['facebook'=>$facebook]);
	}

	public function postEditfacebook(Request $Request){
		$facebook = facebook::where('id',$Request->id)->update([
			'ten'=>$Request->ten,
			'url'=>$Request->url,
			'updated_at'=>Carbon::now()
		]);
		return back();
	}
	public function remove($id){
        facebook::where('id',$id)->delete();
        $facebook = facebook::all();
		return view("admin.contact.facebook",['facebook'=>$facebook]);
    }

    public function selectfacebook($id){
		$facebook = facebook::where('id',$id)->get();
		foreach ($facebook as $value) {
			$status = $value->status;
		}
		if($status == 0){
			$facebook = facebook::where('id',$id)->update([
				'status'=>1
			]);
		}else{
			$facebook = facebook::where('id',$id)->update([
				'status'=>0
			]);
		}
		return redirect()->route('getfacebook');
	}
}
