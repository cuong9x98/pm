<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\category;
use App\product;
use App\orders;
use App\User;
class AdhomeController extends Controller
{
    // hiển thị danh sách category ô tìm kiếm
    public function get_list_category(){
        $category = category::all();
        return view('admin.index',['category'=>$category]);
    }
    // Biểu đồ theo tháng
    public function getDoanhThuThang(Request $Request){
        $array_month = $Request->get('x');
        $data = [];
        //Lấy dữ liệu của biểu đồ 
        $month_0 = $array_month[0];
        if(strlen($month_0)==6){
            $m_0 = substr($month_0,0,1);
            $y_0 = substr($month_0,2,4);
        }else{
            $m_0 = substr($month_0,0,2);
            $y_0 = substr($month_0,3,4);
        }
        // 1
        $month_1 = $array_month[1];
        if(strlen($month_1)>5){
            $m_1 = substr($month_1,0,1);
            $y_1 = substr($month_1,2,4);
        }else{
            $m_1 = substr($month_1,0,2);
            $y_1 = substr($month_1,3,4);
        }
        //2
        $month_2 = $array_month[2];
        if(strlen($month_2)>5){
            $m_2 = substr($month_2,0,1);
            $y_2 = substr($month_2,2,4);
        }else{
            $m_2 = substr($month_2,0,2);
            $y_2 = substr($month_2,3,4);
        }
        //3
        $month_3 = $array_month[3];
        if(strlen($month_3)>5){
            $m_3 = substr($month_3,0,1);
            $y_3 = substr($month_3,2,4);
        }else{
            $m_3 = substr($month_3,0,2);
            $y_3 = substr($month_3,3,4);
        }
        //4
        $month_4 = $array_month[4];
        if(strlen($month_4)>5){
            $m_4 = substr($month_4,0,1);
            $y_4 = substr($month_4,2,4);
        }else{
            $m_4 = substr($month_4,0,2);
            $y_4 = substr($month_4,3,4);
        }
        //5
        $month_5 = $array_month[5];
        if(strlen($month_5)>5){
            $m_5 = substr($month_5,0,1);
            $y_5 = substr($month_5,2,4);
        }else{
            $m_5 = substr($month_5,0,2);
            $y_5 = substr($month_5,3,4);
        }
        //6
        $month_6 = $array_month[6];
        if(strlen($month_6)>5){
            $m_6 = substr($month_6,0,1);
            $y_6 = substr($month_6,2,4);
        }else{
            $m_6 = substr($month_6,0,2);
            $y_6 = substr($month_6,3,4);
        }
        //7
        $month_7 = $array_month[7];
        if(strlen($month_7)>5){
            $m_7 = substr($month_7,0,1);
            $y_7 = substr($month_7,2,4);
        }else{
            $m_7 = substr($month_7,0,2);
            $y_7 = substr($month_7,3,4);
        }
        //8
        $month_8 = $array_month[8];
        if(strlen($month_8)>5){
            $m_8 = substr($month_8,0,1);
            $y_8 = substr($month_8,2,4);
        }else{
            $m_8 = substr($month_8,0,2);
            $y_8 = substr($month_8,3,4);
        }
        //9
        $month_9 = $array_month[9];
        if(strlen($month_9)==6){
            $m_9 = substr($month_9,0,1);
            $y_9 = substr($month_9,2,4);
        }else{
            $m_9 = substr($month_9,0,2);
            $y_9 = substr($month_9,3,4);
        }
        $data['month_0'] = 0;
        $data['month_1'] = 0;
        $data['month_2'] = 0;
        $data['month_3'] = 0;
        $data['month_4'] = 0;
        $data['month_5'] = 0;
        $data['month_6'] = 0;
        $data['month_7'] = 0;
        $data['month_8'] = 0;
        $data['month_9'] = 0;
        $orders = orders::where('status',5)->get();
        if($Request->get('role')==2 || $Request->get('role')==3 || $Request->get('role')==7){
            foreach ($orders as $value) {
                if(substr($value->updated_at,5,2)==$m_0 && substr($value->updated_at,0,4)==$y_0){
                    $data['month_0']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_1 && substr($value->updated_at,0,4)==$y_1){
                    $data['month_1']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_2 && substr($value->updated_at,0,4)==$y_2){
                    $data['month_2']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_3 && substr($value->updated_at,0,4)==$y_3){
                    $data['month_3']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_4 && substr($value->updated_at,0,4)==$y_4){
                    $data['month_4']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_5 && substr($value->updated_at,0,4)==$y_5){
                    $data['month_5']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_6 && substr($value->updated_at,0,4)==$y_6){
                    $data['month_6']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_7 && substr($value->updated_at,0,4)==$y_7){
                    $data['month_7']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_8 && substr($value->updated_at,0,4)==$y_8){
                    $data['month_8']+=$value->total_price;
                }
                if(substr($value->updated_at,5,2)==$m_9 && substr($value->updated_at,0,4)==$y_9){
                    $data['month_9']+=$value->total_price;
                }
            }
        }
        if($Request->get('role')==6){
            foreach ($orders as $value) {
                if(substr($value->updated_at,5,2)==$m_0 && substr($value->updated_at,0,4)==$y_0){
                    $data['month_0']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_1 && substr($value->updated_at,0,4)==$y_1){
                    $data['month_1']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_2 && substr($value->updated_at,0,4)==$y_2){
                    $data['month_2']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_3 && substr($value->updated_at,0,4)==$y_3){
                    $data['month_3']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_4 && substr($value->updated_at,0,4)==$y_4){
                    $data['month_4']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_5 && substr($value->updated_at,0,4)==$y_5){
                    $data['month_5']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_6 && substr($value->updated_at,0,4)==$y_6){
                    $data['month_6']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_7 && substr($value->updated_at,0,4)==$y_7){
                    $data['month_7']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_8 && substr($value->updated_at,0,4)==$y_8){
                    $data['month_8']+=$value->total_commission;
                }
                if(substr($value->updated_at,5,2)==$m_9 && substr($value->updated_at,0,4)==$y_9){
                    $data['month_9']+=$value->total_commission;
                }
            }
        }
        // Lấy doanh thu
        $data['sum'] = 0;
        $user = User::where('role',8)->get();
        $user = count($user);
        $data['user'] = $user;
        $now = getdate();
        if($Request->get('role')==2 || $Request->get('role')==3 || $Request->get('role')==7){
            $data['buy'] = count($orders);
            foreach ($orders as $value) {
                if(substr($value->updated_at,0,4)==$now["year"]){
                    $data['sum']+=$value->total_price;
                }
            }
        }
        // if($Request->get('role')==6){
        //     $orders = orders::where('status',5)->where('id_sale',$Request->get('id'))->get();
        //     $data['buy'] = count($orders);
        //     $now = getdate();
        //     foreach ($orders as $value) {
        //         if(substr($value->updated_at,0,4)==$year_5){
        //             $data['sum']+=$value->total_commission;
        //         }
        //     }    
        // }
        echo json_encode($data);
    }
    // Biểu đồ theo năm
    public function AjaxDoanhThu(Request $Request){
        $array_year = $Request->get('x');
        $data = [];
        $year_0 = $array_year[0];
        $year_1 = $array_year[1];
        $year_2 = $array_year[2];
        $year_3 = $array_year[3];
        $year_4 = $array_year[4];
        $year_5 = $array_year[5];
        $data['year_0'] = 0;
        $data['year_1'] = 0;
        $data['year_2'] = 0;
        $data['year_3'] = 0;
        $data['year_4'] = 0;
        $data['year_5'] = 0;
        $orders = orders::where('status',5)->get();
        if($Request->get('role')==2 || $Request->get('role')==3 || $Request->get('role')==7){
            foreach ($orders as $value) {
                if(substr($value->updated_at,0,4)==$year_0){
                    $data['year_0']+=$value->total_price;
                }
                if(substr($value->updated_at,0,4)==$year_1){
                    $data['year_1']+=$value->total_price;
                }
                if(substr($value->updated_at,0,4)==$year_2){
                    $data['year_2']+=$value->total_price;
                }
                if(substr($value->updated_at,0,4)==$year_3){
                    $data['year_3']+=$value->total_price;
                }
                if(substr($value->updated_at,0,4)==$year_4){
                    $data['year_4']+=$value->total_price;
                }
                if(substr($value->updated_at,0,4)==$year_5){
                    $data['year_5']+=$value->total_price;
                }
            }
        }
        if($Request->get('role')==6){
            foreach ($orders as $value) {
                if(substr($value->updated_at,0,4)==$year_0){
                    $data['year_0']+=$value->total_commission;
                }
                if(substr($value->updated_at,0,4)==$year_1){
                    $data['year_1']+=$value->total_commission;
                }
                if(substr($value->updated_at,0,4)==$year_2){
                    $data['year_2']+=$value->total_commission;
                }
                if(substr($value->updated_at,0,4)==$year_3){
                    $data['year_3']+=$value->total_commission;
                }
                if(substr($value->updated_at,0,4)==$year_4){
                    $data['year_4']+=$value->total_commission;
                }
                if(substr($value->updated_at,0,4)==$year_5){
                    $data['year_5']+=$value->total_commission;
                }
            }
        }
        //Lấy doanh thu
        $data['sum'] = 0;
        $user = User::where('role',8)->get();
        $user = count($user);
        $data['user'] = $user;
        if($Request->get('role')==2 || $Request->get('role')==3 || $Request->get('role')==7){
            $data['buy'] = count($orders);
            foreach ($orders as $value) {
                if(substr($value->updated_at,0,4)==$year_5){
                    $data['sum']+=$value->total_price;
                }
            }
        }
        if($Request->get('role')==6){
            $orders = orders::where('status',5)->where('id_sale',$Request->get('id'))->get();
            $data['buy'] = count($orders);
            foreach ($orders as $value) {
                if(substr($value->updated_at,0,4)==$year_5){
                    $data['sum']+=$value->total_commission;
                }
            }    
        }
        $data['sum']=number_format($data['sum'],0,',',','); 
        $data['user'] = number_format($data['user'],0,',',',');
        $data['buy'] = number_format($data['buy'],0,',',',');
        echo json_encode($data);
    }
    // Tìm sản phẩm theo danh mục
    public function search(Request $Request){
        $data = "";
        if($Request->get('role')==2){
            if($Request->get('category') == "all"){
                $product = product::all();
                if(count($product) > 0){
                    foreach ($product as $value) {
                        $data .="<tr>";                        
                        $data .= "<td>".$value->name."</td>";
                        $data .= "<td><img src='../../public/uploads/img_product/".$value->img."' style='width:100px; height:100px'></td>";
                        $data .= "<td>".$value->price."</td>";
                        $data .= "<td><span ondblclick='changePromotion(this)' id='".$value->id."'>".$value->promotion."</span></td>";
                        $data.="</tr>";    
                    }
                }else{
                    $data = "<td colspan='4'>Không có sản phẩm nào</td>";
                }        
            }else{
                $product = product::where('id_cate',$Request->get('category'))->get();
                if(count($product) > 0){
                    foreach ($product as $value) {
                        $data ="<tr>";
                        $data .= "<td>".$value->name."</td>";
                        $data .= "<td><img src='../../public/uploads/img_product/".$value->img."' style='width:100px; height:100px'></td>";
                        $data .= "<td>".$value->price."</td>";
                        $data .= "<td><span ondblclick='changePromotion(this)' id='".$value->id."'>".$value->promotion."</span></td>";
                        $data.="</tr>";
                    }        
                }else{
                   $data = "<td colspan='4'>Không có sản phẩm nào</td>";
               }
           }    
           echo $data;        
       }
   }
    // Tìm kiếm doanh thu
   public function searchDoanhThu(Request $Request){
    $sum = 0;
    if($Request->get('role')==2){
        $orders = orders::all();
        foreach ($orders as $value) {
            if($Request->get('year') != "null"){
                if($Request->get('month') != "null"){
                    if($Request->get('date') != "null"){
                        if(substr($value->updated_at,8,2)==$Request->get('date') && substr($value->updated_at,5,2)==$Request->get('month') && substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_price;
                        } 
                    }else{
                        if(substr($value->updated_at,5,2)==$Request->get('month') && substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_price;
                        } 
                    }
                }else{
                    if($Request->get('date') != "null"){
                        if(substr($value->updated_at,8,2)==$Request->get('date') && substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_price;
                        }
                    }else{
                        if(substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_price;
                        }
                    }
                }             
            }else if($Request->get('month') != "null"){
                if($Request->get('date') != "null"){
                    if(substr($value->updated_at,5,2)==$Request->get('month') && substr($value->updated_at,8,2)==$Request->get('date') ){
                        $sum+=$value->total_price;
                    }
                }else{
                    if(substr($value->updated_at,5,2)==$Request->get('month')){
                        $sum+=$value->total_price;
                    }
                }
            }else{
                if($Request->get('date') != "null"){
                    if(substr($value->updated_at,8,2)==$Request->get('date')){
                        $sum+=$value->total_price;
                    }
                }else{
                    $search = "Không";
                }
            }
        }
    }
    if($Request->get('role')==6){
        $orders = orders::where('id_sale',$Request->get('id_user'))->where('status',5)->get();
        foreach ($orders as $value) {
            if($Request->get('year') != "null"){
                if($Request->get('month') != "null"){
                    if($Request->get('date') != "null"){
                        if(substr($value->updated_at,8,2)==$Request->get('date') && substr($value->updated_at,5,2)==$Request->get('month') && substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_commission;
                        } 
                    }else{
                        if(substr($value->updated_at,5,2)==$Request->get('month') && substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_commission;
                        } 
                    }
                }else{
                    if($Request->get('date') != "null"){
                        if(substr($value->updated_at,8,2)==$Request->get('date') && substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_commission;
                        }
                    }else{
                        if(substr($value->updated_at,0,4)==$Request->get('year') ){
                            $sum+=$value->total_commission;
                        }
                    }
                }             
            }else if($Request->get('month') != "null"){
                if($Request->get('date') != "null"){
                    if(substr($value->updated_at,5,2)==$Request->get('month') && substr($value->updated_at,8,2)==$Request->get('date') ){
                        $sum+=$value->total_commission;
                    }
                }else{
                    if(substr($value->updated_at,5,2)==$Request->get('month')){
                        $sum+=$value->total_commission;
                    }
                }
            }else{
                if($Request->get('date') != "null"){
                    if(substr($value->updated_at,8,2)==$Request->get('date')){
                        $sum+=$value->total_commission;
                    }
                }else{
                    $search = "Không";
                }
            }
        }
    }
    echo number_format($sum,0,',',','); 
}  


}
