<?php

namespace App\Http\Controllers\admin\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\category;
use DB;
use Str;
use App\product;
use App\detail_photo;
use Carbon\Carbon;
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use App\notifications;
use App\User;
use App\tag;

class ProductController extends Controller
{
    // show trang thêm
    public function add(){
    	$category = category::all();
   		return view('admin.product.them_san_pham',['category'=>$category]);
    }
    //	post thêm
    public function postAdd(Request $Request){ 
	    // Nếu admin tạo thì ko cần duyệt
	    $User = User::where('id',$Request->creater)->get();
	    foreach ($User as $user) {
            $role = $user->role;
            $admin_confirm = $user->id;
	    }
	    if($role==2 || $role==3){
            $status= 1;
	    }else{
            $status=0;
            $admin_confirm = User::where('role',3)->inRandomOrder()->get(); 
            foreach ($admin_confirm as $id_admin) {
              $admin_confirm = $id_admin->id;
            }
	    }
	    // Thêm và lưu trữ sản phẩm
	    $img_name = $Request->file('img_name')->getClientOriginalName();   
	    $id_prod = DB::table('product')->insertGetId(
	      [
	       'name'=>$Request->name,
	       'price'=>$Request->price,
	       'qty_product'=>$Request->qty_product,
	       'description'=>$Request->description,
	       'detail'=>$Request->detail,
	       'img'=>$img_name,
	       'promotion'=>$Request->promotion,
	       'id_cate'=>$Request->cate,           
	       'creater'=>$Request->creater,
	       'status'=>$status,
           'like'=>0,
	       'admin_confirm'=>$admin_confirm,
	       'created_at'=>Carbon::now(),
	     ]
	    );
	    // Lưu trữ tag 
	    for($i=1;$i<=$Request->qty_tag;$i++){
	      $tag = "tag_".$i;
	      DB::table('tag')->insert(
	        [
	         'name'=>$Request->$tag,
	         'id_prod'=> $id_prod,
	         'created_at'=>Carbon::now(),
	       ]
	     );
	    }
	    $Request->file('img_name')->move('uploads/img_product/',$img_name);
	    // Thêm và lưu trữ các ảnh con 
	    //dd($Request->all()); -> Lấy hết dữ liệu gửi từ client
	    $new_detail_photo = new detail_photo;
	    if($Request->files_compare){
	      $array_image = [];
	      foreach ($Request->files_compare as $key=>$item){
	        if(!is_null($item)){
	          array_push($array_image,$key);
	        }
	      }
	      if($Request->hasFile('files')){
	        $arrayFiles = [];
	        foreach ($Request->file('files') as $key => $item){
	          if(in_array($key, $array_image)){
	            $file_name=Str::random(4).'_'.$item->getClientOriginalName();
	            array_push($arrayFiles,$file_name);
	            $item->move('uploads/detail_photo/',$file_name);
	          }
	        }
	        $new_detail_photo->img = $arrayFiles;
	        // dd(gettype($arrayFiles)); -> xem kiểu dữ liệu của biến
	      }
	    }
	    $new_detail_photo->id_prod = $id_prod;
	    $new_detail_photo->save();
	    return redirect()->route('send-mail-product',['admin_confirm'=>$admin_confirm,'name'=>$Request->name]);
	    return view('admin.product.add_product')->with('messages','Thêm thành công!');
  	}
  	// Xóa sản phẩm
  	public function destroy($id){
        product::where('id',$id)->delete();
        $detail_photo = detail_photo::where('id_prod',$id)->get();        
        foreach ($detail_photo as $value) {
            foreach ($value->img as $key=>$value_detail_photo){
                $url = 'C:\xampp\htdocs\shop_pm2\public\uploads\detail_photo\\';
                File::delete($url.$value_detail_photo);                 
            }
        } 
        $product = DB::table('product')->where('id',$id)->get();
        foreach ($product as $value){
            $url = 'C:\xampp\htdocs\shop_pm2\public\uploads\img_product\\';
            File::delete($url.$value->img);
        }
        return back();
    }
    // Sửa sản phẩm
    public function edit($id){
        $product = product::where('id',$id)->get();
        $category = category::all();
        $tag = tag::where('id_prod',$id)->get();
        $detail_photo = new detail_photo();
        $detail_photo = detail_photo::where('id_prod',$id)->get();
        return view('admin.product.sua_san_pham',['tag'=>$tag,'product'=>$product,'category'=>$category,'detail_photo'=>$detail_photo]);
    }
    // post sửa
    public function update(Request $Request, $id){
       	$tag = tag::where('id_prod',$id)->delete();
       	for($i=1;$i<=$Request->qty_tag;$i++){
        	$input_tag = "tag_".$i;
        	DB::table('tag')->insert(
          		[
           			'name'=>$Request->$input_tag,
           			'id_prod'=> $id,
           			'created_at'=>Carbon::now(),
       			]
   			);
   		}
        //====================================================================
        //             CHỈNH PHẦN THÔNG TIN SẢN PHẨM
        //====================================================================
    	if($Request->hasFile('img_name')){
	        $product = DB::table('product')->where('id',$id)->get();
	        foreach ($product as $value){
	            $url = 'C:\xampp\htdocs\shop_pm2\public\uploads\img_product\\';
	            File::delete($url.$value->img);
	        }
	        $name_img = $Request->file('img')->getClientOriginalName();
	        DB::table('product')->where('id',$id)->update(
	            [
	                'name'=>$Request->name,
	                'price'=>$Request->price,
	                'qty_product'=>$Request->qty_product,
	                'description'=>$Request->description,
	                'detail'=>$Request->detail,
	                'img'=>$name_img,
	                'promotion'=>$Request->promotion,
	                'id_cate'=>$Request->cate,
	                'updated_at'=>Carbon::now(),
	                'status'=>0,
	            ]
	        );
        $Request->file('img_name')->move('uploads/img_product/',$img_name);
    	}else{
	        DB::table('product')->where('id',$id)->update(
	            [
	                'name'=>$Request->name,
	                'price'=>$Request->price,
	                'qty_product'=>$Request->qty_product,
	                'description'=>$Request->description,
	                'detail'=>$Request->detail,
	                'promotion'=>$Request->promotion,
	                'id_cate'=>$Request->cate,
	                'updated_at'=>Carbon::now(),
	                'status'=>0,
	            ]
	        );
    	}
        //====================================================================
        //             CHỈNH SỬA ẢNH CHI TIẾT SẢN PHẨM
        //====================================================================
            // =========================
            // Tạo mảng để lưu giá trị ảnh chuẩn bị thêm vào database
            // =========================
    	if($Request->files_compare){
	        $array_image = [];
	        $array_image_old = [];
	        foreach ($Request->files_compare as $key=>$item) {
	            if(!(is_null($item))){
	                array_push($array_image,$key);
	                array_push($array_image_old,$item);
	            }
	        }
                // =========================
                // Thêm ảnh vào database
                // =========================
                # Nếu người dùng thêm ảnh mới
	        if($Request->hasFile('files')){   
	            $arrayFiles = [];
	            $detail_photo = detail_photo::where('id_prod',$id)->get();  

                    //===== Xóa ảnh bỏ đi trong thư mục =====//     Xem xét  
                    // foreach ($detail_photo as $value) {
                    //     foreach ($value->img as $value_detail_photo){ // Lấy mảng ảnh trong database                          
                    //         foreach ($array_image_old as $value){ // Lấy các ảnh bên client
                    //             if($value==$value_detail_photo){
                    //                 array_push($arrayFiles,$value_detail_photo);
                    //             }               
                    //         }                  
                    //     }                  
                    // }
                    //===== End xóa ảnh bỏ đi trong thư mục =====// 

                //===== Tạo mảng chứa ảnh mới =====//
           		foreach ($detail_photo as $value) {
                    foreach ($value->img as $value_detail_photo){ // Lấy mảng ảnh trong database                          
                        foreach ($array_image_old as $value){ // Lấy các ảnh bên client
                            if($value==$value_detail_photo){
                                array_push($arrayFiles,$value_detail_photo);
                            }               
                        }                  
                    }                  
                }
                foreach ($Request->file('files') as $key=>$item) {
                    if(in_array($key,$array_image)){
                        $file_name=Str::random(4).'_'.$item->getClientOriginalName();
                        array_push($arrayFiles,$file_name);
                        $item->move('uploads/detail_photo/',$file_name);
                    }
                }
                //===== End tạo mảng chứa ảnh mới =====//

                //===== Cập nhật lại trong database =====//
                DB::table('detail_photo')->where('id_prod',$id)->update(
                    [
                        'img'=>$arrayFiles,
                        'id_prod'=>$id,
                        'updated_at'=>Carbon::now(),
                    ]
                ); 
                DB::table('product')->where('id',$id)->update(
                    [
                        'status'=>0,
                    ]
                );
            }
            # Nếu người dùng ko thêm ảnh mà chỉ xóa ở ảnh cũ
            else{ 
                $arrayFiles = [];
                $detail_photo = detail_photo::where('id_prod',$id)->get(); 
                    //===== Xóa ảnh bỏ đi trong thư mục =====// xem xét

                    // foreach ($detail_photo as $value) {
                    //     foreach ($value->img as $key=>$value_detail_photo){
                    //         if(!(in_array($key,$array_image))){
                    //             $url = 'C:\xampp\htdocs\shop_pm2\public\uploads\detail_photo\\';
                    //             File::delete($url.$value_detail_photo);
                    //         }
                    //     }                        
                    // }
                    //===== End xóa ảnh bỏ đi trong thư mục =====//  

                    //===== Tạo mảng chứa ảnh mới =====//
                foreach ($detail_photo as $value) {
                    foreach ($value->img as $key=>$value_detail_photo){ // Lấy mảng ảnh trong database
                        foreach ($array_image as $value){ // Lấy các ảnh bên client
                            if($key==$value){
                                array_push($arrayFiles,$value_detail_photo);
                                break;
                            }               
                        }    
                    }                  
                }
                    //===== End tạo mảng chứa ảnh mới =====//

                    //===== Cập nhật lại trong database =====//
                DB::table('detail_photo')->where('id_prod',$id)->update(
                    [
                        'img'=>$arrayFiles,
                        'id_prod'=>$id,
                        'updated_at'=>Carbon::now(),
                    ]
                );   
                DB::table('product')->where('id',$id)->update(
                    [
                        'status'=>0,
                    ]
                );   
            }
        }
        return back()->with('messages','Chỉnh sửa thành công !');
    }
    // Chi tiết sản phẩm 
    public function detail($id){
        $product = product::where('id',$id)->get();
        foreach ($product as $value) {
            $id_prod = $value->id;
            $category = category::where('id',$value->id_cate)->get();
            foreach ($category as $value_cate) {
                $category = $value_cate->name;
            }
        }
        $tag = tag::where('id_prod',$id)->get();
        $detail_photo = detail_photo::where('id_prod',$id_prod)->get();
        return view('admin.product.chi_tiet_san_pham',['category'=>$category,'tag'=>$tag,'product'=>$product,'detail_photo'=>$detail_photo]);
    }
}
