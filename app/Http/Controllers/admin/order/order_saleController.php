<?php

namespace App\Http\Controllers\admin\order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\orders;
use App\orders_detail;
use App\User;
use App\product;
use App\province;
use App\district;
use App\ward;
use Carbon\Carbon;
use App\contact;
class order_saleController extends Controller
{
    // Lấy danh sách đơn hàng bên sale
	public function getListOrderSale($id){
		$data = [];
		$orders = orders::where('id_sale',$id)->where('notifi',2)->get();
		if(count($orders)>0){
			foreach ($orders as $key=>$value_order) {
                // Lấy thông tin đơn hàng
				$data[$key]['qty'] = $value_order->qty;
				$data[$key]['status'] = $value_order->status;
				$data[$key]['TenĐH'] = $value_order->TenĐH;
				$data[$key]['total_price'] = $value_order->total_price;
				$data[$key]['total_commission'] = $value_order->total_commission;
				$data[$key]['id'] = $value_order->id;
				$data[$key]['created_at'] = $value_order->created_at;

                // Lấy thông tin người đặt
				$customer = User::where('id',$value_order->id_customer)->get();
				if($value_order->id_customer == 0){
					$contact = contact::where('id_order',$value_order->id)->get();
					foreach ($contact as $value_customer) {    
						$data[$key]['name'] = $value_customer->name;  
						$data[$key]['phone'] = $value_customer->phone;  
						$data[$key]['address'] = $value_customer->street;
						$data[$key]['TinhTP'] = $value_customer->city;
						$data[$key]['QuanHuyen'] = $value_customer->town;        
						$data[$key]['XaPhuong'] = $value_customer->village;
					}
				}else{
					$customer = User::where('id',$value_order->id_customer)->get();
					foreach ($customer as $value_customer) {    
						$data[$key]['name'] = $value_customer->name;  
						$data[$key]['phone'] = $value_customer->phone;  
						$data[$key]['address'] = $value_customer->address['dia_chi'];
                        // Lất TinhTP
						$province = province::where('id',$value_customer->address['TinhTP'])->get();
						foreach ($province as $value_province) {
							$data[$key]['TinhTP'] = $value_province->_name;
						}
                        // Lấy QuanHuyen
						$district = district::where('id',$value_customer->address['QuanHuyen'])->get();
						foreach ($district as $value_district) {
							$data[$key]['QuanHuyen'] = $value_district->_name;
						}
                        // Lấy XaPhuong
						$ward = ward::where('id',$value_customer->address['XaPhuong'])->get();
						foreach ($ward as $value_ward) {
							$data[$key]['XaPhuong'] = $value_ward->_name;
						}
					}
				}
			} 
			return view('admin.cart.order',['order'=>$orders,'data'=>$data]);
		}else{
			return view('admin.cart.order',['order'=>$orders]);
		}
	}
	// Lấy đơn hàng chi tiết bên sale
	public function getListDetailOrder(Request $Request){
		$data = "<tr>";
		$orders = orders::where('id',$Request->get('id'))->get();
		foreach ($orders as $value_orders) {
			$data.="<input type='hidden' id='total_commission' value='".number_format($value_orders->total_commission,'0',',',',')."'>";
		}
		$orders_detail = orders_detail::where("order_id",$Request->get('id'))->get();
		foreach ($orders_detail as $value) {
			$data.="<td>1</td>";
			$product = product::where('id',$value->id_prod)->get();
			foreach ($product as $value_prod) {
				$data.="<td>".$value_prod->name."</td>";
			}
			$data.="<td>".$value->qty."</td>";
			$data.="<td>".number_format($value->total_price,'0',',',',')." VNĐ </td>";
			if($Request->get('id_user') != 2){
				$data.="<td><span ondblclick='changeCommission(this)' id='".$Request->get('id')."'>".$value->commission_."</span> % </td>";
			}else{
				$data.="<td><span>".$value->commission_."</span> % </td>";
			}
			$data.="<td><span id='commission_price'>".number_format($value->commission_price,'0',',',',')."</span> VNĐ </td>";
		}
		$data.="</tr>";
		echo $data;
	}
	
	// Ajax thay đổi tiền hoa hồng
	public function orderCommissions(Request $Request){
		$orders = orders::where('id',$Request->get('id'))->get();
		foreach ($orders as $value) {
			$total_commission = $value->total_commission;
		}
		echo number_format($total_commission,'0',',',',');
	}
	
	//Xác nhận đơn hàng
	public function confirmOrder(Request $Request){
		$orders = orders::find($Request->id);
		$orders->status = $Request->status;
		$orders->updated_at =  Carbon::now();
		$orders->save();
		return redirect()->route('getListOrderSale',['id'=>$Request->id_user]);
	}

	public function order_confirm($id){
		$data = [];
		$orders = orders::where('id_sale',$id)->where('status',5)->get();
		if(count($orders)>0){
			foreach ($orders as $key=>$value_order) {
                // Lấy thông tin đơn hàng
				$data[$key]['qty'] = $value_order->qty;
				$data[$key]['status'] = $value_order->status;
				$data[$key]['TenĐH'] = $value_order->TenĐH;
				$data[$key]['total_price'] = $value_order->total_price;
				$data[$key]['total_commission'] = $value_order->total_commission;
				$data[$key]['id'] = $value_order->id;
				$data[$key]['created_at'] = $value_order->created_at;
				$data[$key]['updated_at'] = $value_order->updated_at;

                // Lấy thông tin người đặt
				$customer = User::where('id',$value_order->id_customer)->get();
				if($value_order->id_customer == 0){
					$contact = contact::where('id_order',$value_order->id)->get();
					foreach ($contact as $value_customer) {    
						$data[$key]['name'] = $value_customer->name;  
						$data[$key]['phone'] = $value_customer->phone;  
						$data[$key]['address'] = $value_customer->street;
						$data[$key]['TinhTP'] = $value_customer->city;
						$data[$key]['QuanHuyen'] = $value_customer->town;        
						$data[$key]['XaPhuong'] = $value_customer->village;
					}
				}else{
					$customer = User::where('id',$value_order->id_customer)->get();
					foreach ($customer as $value_customer) {    
						$data[$key]['name'] = $value_customer->name;  
						$data[$key]['phone'] = $value_customer->phone;  
						$data[$key]['address'] = $value_customer->address['dia_chi'];
                        // Lất TinhTP
						$province = province::where('id',$value_customer->address['TinhTP'])->get();
						foreach ($province as $value_province) {
							$data[$key]['TinhTP'] = $value_province->_name;
						}
                        // Lấy QuanHuyen
						$district = district::where('id',$value_customer->address['QuanHuyen'])->get();
						foreach ($district as $value_district) {
							$data[$key]['QuanHuyen'] = $value_district->_name;
						}
                        // Lấy XaPhuong
						$ward = ward::where('id',$value_customer->address['XaPhuong'])->get();
						foreach ($ward as $value_ward) {
							$data[$key]['XaPhuong'] = $value_ward->_name;
						}
					}
				}
			} 
			return view('admin.cart.don_hang_da_chot',['order'=>$orders,'data'=>$data]);
		}else{
			return view('admin.cart.don_hang_da_chot',['order'=>$orders]);
		}
	}
}
