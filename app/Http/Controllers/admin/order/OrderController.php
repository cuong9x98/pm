<?php

namespace App\Http\Controllers\admin\order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\orders;
use App\orders_detail;
use App\User;
use App\product;
use App\province;
use App\district;
use App\ward;
use App\contact;
class OrderController extends Controller
{
    
    // Hiển thị danh sách đơn hàng đã giao
    public function don_hang_giao($id){
        $data = [];
        $orders = orders::where('id_sale',"!=",null)->get();
        if(count($orders)>0){
            foreach ($orders as $key=>$value_order) {
                $sale = User::where('id',$value_order->id_sale)->get();
                foreach ($sale as $value_sale) {    
                    $data[$key]['sale'] = $value_sale->username;  
                }
                // Lấy thông tin đơn hàng
                $data[$key]['qty'] = $value_order->qty;
                $data[$key]['TenĐH'] = $value_order->TenĐH;
                $data[$key]['total_price'] = $value_order->total_price;
                $data[$key]['total_commission'] = $value_order->total_commission;                
                $data[$key]['id'] = $value_order->id;
                $data[$key]['created_at'] = $value_order->created_at;
                $data[$key]['status'] = $value_order->status;
                // Lấy thông tin người đặt
                $customer = User::where('id',$value_order->id_customer)->get();
                if($value_order->id_customer == 0){
                    $contact = contact::where('id_order',$value_order->id)->get();
                    foreach ($contact as $value_customer) {    
                        $data[$key]['name'] = $value_customer->name;  
                        $data[$key]['phone'] = $value_customer->phone;  
                        $data[$key]['address'] = $value_customer->street;
                        $data[$key]['TinhTP'] = $value_customer->city;
                        $data[$key]['QuanHuyen'] = $value_customer->town;        
                        $data[$key]['XaPhuong'] = $value_customer->village;
                    }
                }else{
                    $customer = User::where('id',$value_order->id_customer)->get();
                    foreach ($customer as $value_customer) {    
                        $data[$key]['name'] = $value_customer->name;  
                        $data[$key]['phone'] = $value_customer->phone;  
                        $data[$key]['address'] = $value_customer->address['dia_chi'];
                        // Lất TinhTP
                        $province = province::where('id',$value_customer->address['TinhTP'])->get();
                        foreach ($province as $value_province) {
                            $data[$key]['TinhTP'] = $value_province->_name;
                        }
                        // Lấy QuanHuyen
                        $district = district::where('id',$value_customer->address['QuanHuyen'])->get();
                        foreach ($district as $value_district) {
                            $data[$key]['QuanHuyen'] = $value_district->_name;
                        }
                        // Lấy XaPhuong
                        $ward = ward::where('id',$value_customer->address['XaPhuong'])->get();
                        foreach ($ward as $value_ward) {
                            $data[$key]['XaPhuong'] = $value_ward->_name;
                        }
                    }
                }
            } 
            return view('admin.cart.don_hang_giao',['order'=>$orders,'data'=>$data]);
        }else{
            return view('admin.cart.don_hang_giao',['order'=>$orders]);
        }
    }
    // Hiển thị danh sách đơn hàng bên admin
    public function show($id){
        $data = [];
        $orders = orders::where('status',0)->get();
        if(count($orders)>0){
            foreach ($orders as $key=>$value_order) {
                // Lấy thông tin đơn hàng
                $data[$key]['qty'] = $value_order->qty;
                $data[$key]['TenĐH'] = $value_order->TenĐH;
                $data[$key]['total_price'] = $value_order->total_price;
                $data[$key]['total_commission'] = $value_order->total_commission;                
                $data[$key]['id'] = $value_order->id;
                $data[$key]['created_at'] = $value_order->created_at;
                // Lấy thông tin người đặt
                $customer = User::where('id',$value_order->id_customer)->get();
                if($value_order->id_customer == 0){
                    $contact = contact::where('id_order',$value_order->id)->get();
                    foreach ($contact as $value_customer) {    
                        $data[$key]['name'] = $value_customer->name;  
                        $data[$key]['phone'] = $value_customer->phone;  
                        $data[$key]['address'] = $value_customer->street;
                        $data[$key]['TinhTP'] = $value_customer->city;
                        $data[$key]['QuanHuyen'] = $value_customer->town;        
                        $data[$key]['XaPhuong'] = $value_customer->village;
                    }
                }else{
                    $customer = User::where('id',$value_order->id_customer)->get();
                    foreach ($customer as $value_customer) {    
                        $data[$key]['name'] = $value_customer->name;  
                        $data[$key]['phone'] = $value_customer->phone;  
                        $data[$key]['address'] = $value_customer->address['dia_chi'];
                        // Lất TinhTP
                        $province = province::where('id',$value_customer->address['TinhTP'])->get();
                        foreach ($province as $value_province) {
                            $data[$key]['TinhTP'] = $value_province->_name;
                        }
                        // Lấy QuanHuyen
                        $district = district::where('id',$value_customer->address['QuanHuyen'])->get();
                        foreach ($district as $value_district) {
                            $data[$key]['QuanHuyen'] = $value_district->_name;
                        }
                        // Lấy XaPhuong
                        $ward = ward::where('id',$value_customer->address['XaPhuong'])->get();
                        foreach ($ward as $value_ward) {
                            $data[$key]['XaPhuong'] = $value_ward->_name;
                        }
                    }
                }
            } 
            return view('admin.cart.index',['order'=>$orders,'data'=>$data]);
        }else{
            return view('admin.cart.index',['order'=>$orders]);
        }
    }

    // Thay đổi % hoa hồng
    public function changeCommissions(Request $Request){
        $commission = 0;
        $orders_detail = orders_detail::where("order_id",$Request->get('id'))->get();
        foreach ($orders_detail as $value) {
            $total_price = $value->total_price;
        }
        $orders_detail = orders_detail::where("order_id",$Request->get('id'))->update([
            'commission_'=>$Request->get('value'),
            'commission_price'=>($Request->get('value')*$total_price)/100,
        ]);
        $orders_detail = orders_detail::where("order_id",$Request->get('id'))->get();
        foreach ($orders_detail as $value) {
            $commission +=  $value->commission_price;
        }
        $orders = orders::where('id',$Request->get('id'))->update([
            'total_commission'=>$commission
        ]);
        echo number_format(($Request->get('value')*$total_price)/100,'0',',',',');
    }

    // Lấy danh sách tài khoản sale
    public function getListSale(){
        $users = User::where("role",6)->get();
        $data = "";
        foreach ($users as $value) {
            $data.= "<option value='".$value->id."'>".$value->name."</option>";
        }
        $data .= "";
        echo $data;
    }

    // Lấy danh sách đơn hàng chi tiết
    public function getListDetailOrder(Request $Request){
        $data = "<tr>";
        $orders = orders::where('id',$Request->get('id'))->get();
        foreach ($orders as $value_orders) {
            $data.="<input type='hidden' id='total_commission' value='".number_format($value_orders->total_commission,'0',',',',')."'>";
        }
        $orders_detail = orders_detail::where("order_id",$Request->get('id'))->get();
        foreach ($orders_detail as $value) {
            $data.="<td>1</td>";
            $product = product::where('id',$value->id_prod)->get();
            foreach ($product as $value_prod) {
                $data.="<td>".$value_prod->name."</td>";
            }
            $data.="<td>".$value->qty."</td>";
            $data.="<td>".number_format($value->total_price,'0',',',',')." VNĐ </td>";
            if($Request->get('id_user') != 2){
                $data.="<td><span ondblclick='changeCommission(this)' id='".$Request->get('id')."'>".$value->commission_."</span> % </td>";
            }else{
                $data.="<td><span>".$value->commission_."</span> % </td>";
            }
            $data.="<td><span id='commission_price'>".number_format($value->commission_price,'0',',',',')."</span> VNĐ </td>";
            $data.="<input type='hidden' name='id' value='{{$value['id']}}'>";
        }
        $data.="</tr>";
        echo $data;
    }

    // Xác nhận đơn hàng
    public function selectSale(Request $Request){
        $orders = orders::where('id',$Request->id)->update([
            'id_sale'=>$Request->sale
        ]);
        return back();
    }
}
