<?php

namespace App\Http\Controllers\admin\role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\role;
use DB;
class roleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $role = role::all();
        $users = User::all();
        return view('admin.role.role',['role'=>$role,'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $Request)
    {  
        $Request->validate([
            'username' => 'unique:users',
            'email' => 'unique:users',
            'confim_password'=>'min:6|same:password',
        ],[
            "username."=>"Tên đăng nhập không để trống",
            "username.unique"=>"Tên đăng nhập đã tồn tại",
            "email.unique" => "Email này đã được đăng ký",
            "confim_password.min"=>"Mật khẩu lớn hơn 6 kí tự",
            "confim_password.same"=>"Mật khẩu không khớp"
        ]); 
        DB::table('users')->insert([
            'name'=>$Request->name,
            'email'=>$Request->email,
            'username'=>$Request->username,
            'password'=>bcrypt($Request->password),
            'role'=>$Request->role,
        ]);
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $Request)
    {
        //cập nhật quyền
        $users['data'] = User::where('id',$Request->id_user)->update([
            'role'=>$Request->edit_role
        ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $Request)//$id
    {
        $role = role::all();
        $users['data'] = User::where('id',$Request->get('id'))->get();
        $data = "";
        foreach ($users['data'] as $value) {
            $data.="<p><span style='font-weight: 900'>Tài khoản</span> : ".$value->name."</p><select name='edit_role' style='padding: 0;width: 286px;margin-bottom: 20px;'>";

            foreach ($role as $value_role) {
                if($value_role->id != 1){
                    if($value->role == $value_role->id)
                        $data.= "<option id='_".$value_role->id."' selected='' value='".$value_role->id."'>".$value_role->name."</option>";
                    else{
                        $data.= "<option id='_".$value_role->id."' value='".$value_role->id."'>".$value_role->name."</option>";
                    }
                }
                
            }
        }
        $data .= "</select>";
        echo $data;          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
