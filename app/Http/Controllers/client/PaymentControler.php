<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\province;
use DB;
use App\contact;
use Session;
class PaymentControler extends Controller
{
    public function getPayment(){
        return redirect('/thanh-toan');
    }
    public function postPayment(Request $request){
        Session::put('name', $request->name); 
        Session::put('city', $request->get_city); 
        Session::put('town', $request->get_town); 
        Session::put('phone', $request->phone); 
        Session::put('email', $request->email); 
        Session::put('village', $request->get_village); 
        Session::put('street', $request->street); 
        Session::put('check', 1); 
        
        return view('client.payment');   
    }
}
