<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog;
use DB;
use App\comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
class BlogController extends Controller
{
    public function getBlog(){
        $blog = blog::all();
    	return view('client.blog',['blog' => $blog]);
    }
    public function getDetailBlog($id){
        $url = URL::current();
        $data = DB::table('blog')->where('id',$id)->get();
        $blog = blog::all();
        $comment = DB::table('comment')->where('id_obj',$id)->orderBy('created_at','ASC')->get();
        
        return view('client.chi-tiet-blog',['data'=>$data,'id'=>$id,'comment'=>$comment,'url'=>$url,'blog'=>$blog]);
    }
    public function postcomment_blog(Request $request)
    {
        if(Auth::check()){
            comment::insert([
                'content'=>$request->content,
                'id_check'=>1,
                'id_obj' =>$request->id,
                'id_user'=>Auth()->user()->id,
                'created_at'=> new \DateTime(),
            ]);
            return back()->with('success','Bình luận thành công');
        }else{
            return view('client.login');
        }
        
    }
}
