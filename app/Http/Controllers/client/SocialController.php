<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
class SocialController extends Controller
{
    public function getInfoFacebook($social){
        return Socialite::driver($social)->redirect();
    }
    public function checkInfoFacebook($social){
        $info = Socialite::driver($social)->user();
        dd($info);
    }
}
