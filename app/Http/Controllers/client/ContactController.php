<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Mail\LienHe;
class ContactController extends Controller
{
    public function getContact(){
    	return view('client.contact');
    }
    public function lienhe(Request $request){
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $content = $request->content;
        $data = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ];
    
       Mail::send('client.mai_lienhe', $data, function ($message) use ($email) {
           $message->to($email)->subject("Câu hỏi khách hàng");
       });
       echo "<script> alert('Gửi mail thành công');history.back();</script>";
    }
}
