<?php

namespace App\Http\Controllers\client;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Gloudemans\Shoppingcart\Facades\Cart;

class Ajax_edit_cartController extends Controller
{
    public function update(Request $request){
        Cart::update($request->id,['qty'=>$request->qty]);
        
        $string = '';
        foreach(Cart::content() as $item){
            $string .= '<tr class='.$item->rowId.'>
            <td> '.$item->id.'</td>
            <td>'.$item->name.'</td>
            <td><img src="'.asset('uploads/img_product/'.$item->options->image).'" width="80px" height="80px" alt=""></td>
            <td>
                <div class="product-quantity">
                       
                    <div class="product-quantity" style = "width:100px">
                        <span class="" ></span>
                        <input class="qty" type="" style="width:25px" value="'.$item->qty.'" name="quantity" />
                        <a class="update_cart" id="'.$item->rowId.'" onclick="return updateCart(this);"><span class="glyphicon glyphicon-repeat"></span></a>
                    </div>
                    <input type="hidden" value="'.$item->rowId.'" name="rowId" class="form-control">
                    
                    
                </div>
                
            </td>   
            <td>
                '.number_format($item->price,0,'.',',').' '.'VNĐ'.'
            </td>
            <td>
            '.number_format($item->price*$item->qty,0,'.',',').' ' .'VNĐ'.'
            </td>
            <th style="text-align:center;font-size:48px" id="'.$item->rowId.'" class="delete_row" onclick="return delete_cart(this);" ><a class="glyphicon glyphicon-trash"></a></th>
        </tr>
        <script>
            (function incrementDecrementInput() {
                var increment = document.querySelector(".product-quantity-plus");
                var decrement = document.querySelector(".product-quantity-minus");
                var input = document.querySelector(".product-quantity > input");

                // increment.addEventListener("click", function () {
                //     input.value++;
                // });

                // decrement.addEventListener("click", function () {
                //     if (input.value > 1) {
                //         input.value--;
                //     }
                });

            })();
        </script>
        
        
        ';
        }
        $count = Cart::count();
        $subtotal=Cart::subtotal();
        
        return ['string'=>$string,'count'=>$count,'subtotal'=>$subtotal];
    }
    public function delete(Request $request){
        Cart::update($request->get('id'),0);
        $count = Cart::count();
        $subtotal=Cart::subtotal();
       return ['count'=>$count,'subtotal'=>$subtotal];
    }
    public function themCart(Request $request){
        
        $id = $request->id;
        $quantity = 1;
        $tien = $request->price;
        $data = DB::table('product')->where('id',$id)->first();
        $result['id'] = $data->id;
        $result['name'] = $data->name;
        $result['qty'] = $quantity;
        $result['price'] = $tien;
        $result['weight'] = $data->price;
        $result['options']['image'] = $data->img;
        $result['options']['creater']=$data->creater;
        Cart::add($result);
        return  $count = Cart::count();
        
    }
}
