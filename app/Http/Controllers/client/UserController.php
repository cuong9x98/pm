<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;
use App\province;
use App\district;
use App\street;
use Session;
use Socialite; 
use App\Social; 
use App\Login; 
use Mail;
use App\Mail\getPassword;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;
session_start();
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.register');
    }
    public function postcreate(Request $request){
        $email = $request->email;
        $name = $request->name;

        $result = array();
        $result['name'] = $request->name;
        $result['username'] = $request->username;
        $result['email'] = $request->email;
        $result['password'] =bcrypt($request->password);
        $result['address'] = $request->address;
        $result['phone'] = $request->numberphone;
        $result['sex'] = $request->sex;
        $result['img'] = $request->image;
        $result['date'] = '01/01/2015';
        $result['role'] = '0';
        $insert = DB::table('users')->insertGetId($result);
        Session::put('customer_id',$insert);
        Session::put('customer_name',$request->name);
        
        $result = User::where('username',$request->username)->first();

        $toke = bcrypt(mt_rand(0, 1000 ));
        User::where('email',$email)->update([
            'remember_token' =>$toke,
        ]);
        $url = route('xacthuc_user',$toke);
        $data = [
            'name' =>$name,
            'url'=>$url,
        ];
        Mail::send('client.xacthuc_user', $data, function ($message) use ($email) {
            $message->to($email)->subject("Xác thực tài khoản");
        });
        return back()->with('success','Bạn hãy kiểm tra hòm thư của bạn');
    }
    public function xacthuc_user($id){
        User::where('remember_token',$id)->update([
            'role' =>8,
        ]);
        return Redirect('/dang-nhap')->with('mess','Tài khoản đã được mở');
    }
    public function getcheckout(){
        
        $id= Auth::id();
        $data = DB::table('users')->where('id',$id)->first();
        $citys = DB::table('province')->get();
        $projects = DB::table('district')->get();
        $streets = DB::table('street')->get();

        return view('client.checkout',['data'=>$data,'citys'=>$citys,'projects'=>$projects,'streets'=>$streets]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getLoginUser(){
        return view('client.login');
    }
    public function postLoginUser(Request $request){
        $user = $request->username;
        $pass = $request->password;
        $data = DB::table('users')->where('username',$user)->where('password',$pass)->first();
        
        if (Auth::attempt(['username'=>$user, 'password'=>$pass])) {
            if(Auth::User()->role ==8){
                // Session::put('username',$user);
                // Session::put('userid',$data->id);
                return redirect('/');   
                
                
            }if(Auth::User()->role ==0){
                return back()->with('error','Tài khoản hiện đang bị khóa');
            }
            else{
                return back()->with('error',"Không được phép dùng tài khoản admin") ;
            }
            
        }else{
            return redirect('/dang-nhap');
        }      
    }
    public function getLogoutUser(){
        Auth::logout();
        Cart::destroy();
        return redirect('/');
    }
    public function login_facebook(){
        return Socialite::driver('facebook')->redirect();
    }
    public function callback_facebook(){
        $provider = Socialite::driver('facebook')->user();
        $account = Social::where('provider','facebook')->where('provider_user_id',$provider->getId())->first();
        if($account){
            //login in vao trang quan tri  
            $account_name = Login::where('admin_id',$account->user)->first();
            Session::put('admin_login',$account_name->admin_name);
            Session::put('admin_id',$account_name->admin_id);
            return redirect('/')->with('message', 'Đăng nhập  thành công');
        }else{

            $hieu = new Social([
                'provider_user_id' => $provider->getId(),
                'provider' => 'facebook'
            ]);

            $orang = Login::where('admin_email',$provider->getEmail())->first();

            if(!$orang){
                $orang = Login::create([
                    'name' => $provider->getName(),
                    'email' => $provider->getEmail(),
                    'phone' => '',
                    'password'=>'',
                    'role' => 5
                ]);
            }
            $hieu->login()->associate($orang);
            $hieu->save();

            $account_name = Login::where('admin_id',$account->user)->first();

            Session::put('admin_login',$account_name->admin_name);
             Session::put('admin_id',$account_name->admin_id);
            return redirect('/')->with('message', 'Đăng nhập Admin thành công');
        } 
    }
    public function forget()
    {
        return view('client.forget');
    }
    public function getpassword(Request $request){
        $email = $request->send;
        if(!$email){
            return back();
        }

        $toke = bcrypt(mt_rand(0, 1000 ));
        User::where('email',$request->send)->update([
            'remember_token' =>$toke,
       ]);
        $code = User::where('email',$request->send)->get('remember_token');
        $url = route('newpass',['toke'=>$toke,'email'=>$email]);
        $data = [
            'url'=>$url,
        ];
    
       Mail::send('client.mess', $data, function ($message) use ($email) {
           $message->to($email)->subject("Lấy lại mật khẩu");
       });

        return view('client.thongbao');
    }
    public function newpass(){
        return view('client.newpass');
    }
    public function postnewpass(Request $request){
        $email = $request->email;
        $toke = $request->toke;
        $user = User::where('remember_token',$toke)->get()->first();
       
        User::where('remember_token',$toke)->update([
            'password' =>bcrypt($request->password)
        ]);
        return Redirect('/dang-nhap');
   
        
    }

}
