<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\orders;
use App\orders_detail;
use DB;
use App\product;
use Session;
use App\contact;
use App\category;
use Mail;
use App\Mail\XacThuc;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;
use  Illuminate\Support\Facades\Redirect;
session_start();

class CartController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.sussecc');
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
    {
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $orderId =  $this->insertOrder($request);
        $this->inserOrder_detail($orderId, $request);
        $data = DB::table('orders_detail')->where('order_id', $orderId)->get();
        $this->update_address($orderId);
        $this->update_buy();
        $this->xacthuc($request);
        Cart::destroy();
        
        return view('client.success',['orderId'=>$orderId]);
    }
    //Hàm gửi mail Xác nhận
    public function xacthuc($request){
        $email =$request->email;
        $phone =$request->phone;
        $name =$request->name;
        $address =$request->address;
        $data = [
            'name'=>$name,
            'phone'=>$phone,
            'email'=>$email,
            'address'=>$address,
        ];
       Mail::send('client.xacthuc', $data, function ($message) use ($email) {
           $message->to($email)->subject("Xác nhận đơn hàng");
       });
    }
    //Hàm thêm bản ghi đơn hàng
    public function insertOrder($request){
        if(Auth::check()){
            $orderId = orders::insertGetId([
                "id_customer"=>0,
                "TenĐH"=>mt_rand(),
                "qty"=>$request->soluong,
                'id_customer'=>Auth::user()->id,
                "total_price"=>str_replace(',','',$request->total_price),
                "status" => 0,
                "created_at"=> new \DateTime(),
            ]);
        }else{
            $orderId = orders::insertGetId([
                "id_customer"=>0,
                "TenĐH"=>'hanoi',
                "qty"=>$request->soluong,
                'id_customer'=>0,
                "total_price"=>str_replace(',','',$request->total_price),
                "status" => 0,
                "created_at"=> new \DateTime(),
            ]);
        }
            return $orderId;
    }
    //Hàm thêm bản ghi vào chi tiết đơn hàng
    public function inserOrder_detail($orderId, $request){
        $tong = 0;
        foreach(Cart::content() as $item){
            $data = product::where('id',$item->id)->get()->first();
            $com  = category::where('id',$data->id_cate)->get()->first();
            $commission = $com->commission;
            $tong += ($commission/100) *($item->price*$item->qty);
            $qty_update = $data->qty_product - $item->qty;
            
           orders_detail::insertGetId([
                "order_id"=>$orderId,
                "qty"=>$item->qty,
                "total_price"=>$item->price*$item->qty,
                "commission_"=>$commission,
                "commission_price"=>($commission/100) *($item->price*$item->qty),
                "id_prod"=>$item->id,
                
                "status"=>0,
                "create_at"=>new \DateTime(),
                "notifi"=>0,
            ]);
            //Hàm cập nhật số lượng hàng trong kho
            product::where('id',$item->id)->update([
                "qty_product"=>$qty_update
            ]);
        } 
        
        orders::where('id',$orderId)->update(["total_commission"=>$tong]);
    
    }
    //Hàm cập nhật số lượt mua hàng
    public function update_buy(){
        foreach(Cart::content() as $item ){
            $product = DB::table('product')->where('id',$item->id)->first();

            $tang = ($product->buy) + ($item->qty);
            $product->buy = $tang;
            Product::where('id',$item->id)->update([
                "buy"=>$tang,
            
            ]);
        }

        
    }
    //Hàm thêm bản ghi contact
    public function update_address($orderId){
        contact::insert([
            'id_order'=>$orderId,
            'name'=>session::get('name'),
            'phone'=>session::get('phone'),
            'email'=>session::get('email'),
            'city'=>session::get('city'),
            'town'=>session::get('town'),
            'village'=>session::get('village'),
            'street'=>session::get('street'),
            'check'=>1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $qty= $request->quantity;
        $id = $request->rowId;
        Cart::update($id, $qty);
        return Redirect('gio-hang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo ($id);
        Cart::update($id,0);
        return Redirect('gio-hang');
    }

    public function getCart(Request $request){
        $id = $request->productId;
        $quantity = $request->quantity;
        $tien = $request->price;
        $data = DB::table('product')->where('id',$id)->first();
        $result['id'] = $data->id;
        $result['name'] = $data->name;
        $result['qty'] = $quantity;
        $result['price'] = $tien;
        $result['weight'] = $data->price;
        $result['options']['image'] = $data->img;
        $result['options']['creater']=$data->creater;
     
        Cart::add($result);
        return redirect(route('showCart'));
    }
    

        
    public function showCart(){
        return view('client.cart');
    }
}

