<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\category;
use App\product;
use App\blog;
use App\mylove;
use App\tag;
use App\banner;
use DB;
use  App\phone;
use App\email;
use Gloudemans\Shoppingcart\Facades\Cart;
class HomeController extends Controller
{
   	public function getHome(){
   		$category = category::all();
		$product_promotion = product::where('status',1)->orderBy('promotion','DESC')->get();
		$product_view = product::where('status',1)->orderBy('view','DESC')->get();
		$product_created = product::where('status',1)->orderBy('created_at','DESC')->get();
		$product_buy = product::where('status',1)->orderBy('buy','DESC')->get();
		$blog = blog::all();
		$phone = phone::where('status',1)->get();
		$email = email::where('status',1)->get();
		$banner = banner::where('status',1)->where('check',0)->get();
		$banner_con = banner::where('status',1)->where('check',1)->get();
   		foreach ($category as $value_cate) {
   			$qty_product["".$value_cate->id.""] = product::where('id_cate',$value_cate->id)->where('status',1)->get();
   		}	
    	return view('client.index',['qty_product'=>$qty_product,'blog'=>$blog,'product_promotion'=>$product_promotion,'product_view'=>$product_view,'product_created'=>$product_created,'product_buy'=>$product_buy,'banner'=>$banner,'phone'=>$phone,'email'=>$email,'banner_con'=>$banner_con]);
    }

    public function getTest(){
   		return view('test-2');
    }

    public function postTest(Request $Request){
   		echo $Request->file;
	}
	//Ham tim kiem san pham
	public function postsearch(Request $Request){
		$keywords = $Request->key;
		$category = category::all();
   		$product = product::all();
		$blog = blog::all();
		$data = DB::table('product')->where('name','like','%'.$keywords.'%')->get(); 
		$tag = DB::table('tag')->where('name',$keywords)->get();
	
		if(count($tag)){
			foreach($tag as $t){
				$data_tag =  DB::table('product')->where('id',$t->id_prod)->get();
			}
			
			return view('client.home.search',['blog'=>$blog,'data'=>$data,'data_tag'=>$data_tag]);	
		}else{
			
			return view('client.home.search',['blog'=>$blog,'data'=>$data]);	
		}
			
	}
	public function myorder()
	{
		return view('client.myorder');
	}
	// public function getsearch(){
		
	// 	return view('client.index');
	// }
	public function mylove(Request $request){
		$id_user = Auth()->user()->id;
		$data = mylove::where('id_id_user',$id_user)->get();
		return view('client.mylove',['data' => $data]);
	}
	public function xoa_mylove(Request $request){
		mylove::where('id',$request->row_id)->delete();
		return "Bạn đã cho sản phẩm vào giỏ";
	}
	public function update_mylove(Request $request){
		mylove::where('id',$request->row_id)->delete();

		$data = DB::table('product')->where('id',$request->id)->first();
        $result['id'] = $request->id;
        $result['name'] = $data->name;
        $result['qty'] = 1;
        $result['price'] = $data->price-($data->price*($data->promotion/100));
        $result['weight'] = $data->price;
        $result['options']['image'] = $data->img;
        $result['options']['creater']=$data->creater;
     
        Cart::add($result);
		$count = Cart::count();


		 return ['string'=>'Bạn đã thêm 1 sản phẩm vào giỏ hàng','count'=>$count];
	}
	public function autocomplete(Request $request){
		$data_name = product::select("name")->where("name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data_name);
	}
	public function cart_index_sale(Request $request){
		$data = DB::table('product')->where('id',$request->id)->first();
        $result['id'] = $request->id;
        $result['name'] = $data->name;
        $result['qty'] = 1;
        $result['price'] = $data->price-($data->price*($data->promotion/100));
        $result['weight'] = $data->price;
        $result['options']['image'] = $data->img;
        $result['options']['creater']=$data->creater;
     
        Cart::add($result);
		$count = Cart::count();

		return ['string'=>'Bạn đã thêm 1 sản phẩm vào giỏ hàng','count'=>$count];
	}
	public function love_index_sale(Request $request){
		$kiemtra = mylove::where('id_prod',$request->get('id'))->where('id_id_user',Auth()->user()->id)->get();
		if(count($kiemtra)>0){
			return "Sản phẩm này bạn đã yêu thích rồi";
		}else{
			mylove::insert([
				"id_prod"=>$request->get('id'),
				"id_id_user"=>Auth()->user()->id,
				"status"=>0,
				"created_at"=> new \DateTime(),
			]);
			return "Đã thêm 1 sản phẩm yêu thích";
		}
	}
}
