<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\DatabaseNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use App\User;
use App\orders;
use App\orders_detail;
use DB;
use Carbon\Carbon;
use App\notifications;

class notification_clientController extends Controller
{
    //
	public function read_notifi_client($id){
		$notifications = notifications::where('id',$id)->update([
			'read_at'=>Carbon::now(),
		]);
		return back();
	}
}
