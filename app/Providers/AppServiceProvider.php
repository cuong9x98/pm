<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\category;
use App\address;
use App\phone;
use App\email;
use App\facebook;
use App\instagram;
use App\twitter;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['category']= category::all();
        $data['address']= address::all();
        $data['phone']= phone::all();
        $data['email']= email::all();
        $data['facebook']= facebook::all();
        $data['instagram']= instagram::all();
        $data['twitter']= twitter::all();

        view()->share($data);
    }
}
