<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'name'=>'Super admin',
        		'email'=>'quangnamd67@gmail.com',
        		'username'=>'super.admin',
        		'password'=>bcrypt('namcoi'),
        		'role'=>1,
        		'created_at'=>Carbon::now(),
        	],
            [
                'name'=>'Admin',
                'email'=>'quangnamd68@gmail.com',
                'username'=>'admin',
                'password'=>bcrypt('namcoi'),
                'role'=>2,
                'created_at'=>Carbon::now(),
            ],
            [
                'name'=>'Editor',
                'email'=>'quangnamd69@gmail.com',
                'username'=>'editor',
                'password'=>bcrypt('namcoi'),
                'role'=>3,
                'created_at'=>Carbon::now(),
            ],
            [
                'name'=>'CTV đăng sản phẩm',
                'email'=>'quangnamd70@gmail.com',
                'username'=>'ctv.dang.sp',
                'password'=>bcrypt('namcoi'),
                'role'=>4,
                'created_at'=>Carbon::now(),
            ],
            [
                'name'=>'CTV đăng blog',
                'email'=>'quangnamd71@gmail.com',
                'username'=>'ctv.dang.blog',
                'password'=>bcrypt('namcoi'),
                'role'=>5,
                'created_at'=>Carbon::now(),
            ],
            [
                'name'=>'Sale',
                'email'=>'quangnamd72@gmail.com',
                'username'=>'sale',
                'password'=>bcrypt('namcoi'),
                'role'=>6,
                'created_at'=>Carbon::now(),
            ],
            [
                'name'=>'Kế toán',
                'email'=>'quangnamd73@gmail.com',
                'username'=>'ke.toan',
                'password'=>bcrypt('namcoi'),
                'role'=>7,
                'created_at'=>Carbon::now(),
            ],
            [
                'name'=>'Mua hàng',
                'email'=>'quangnamd74@gmail.com',
                'username'=>'mua.hang',
                'password'=>bcrypt('namcoi'),
                'role'=>8,
                'created_at'=>Carbon::now(),
            ],
        ];

        DB::table('users')->insert($data);
    }
}
