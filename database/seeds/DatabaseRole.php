<?php

use Illuminate\Database\Seeder;
class DatabaseRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [		
    		[
    			'id'=>1,
    			'name'=>"Super admin",
    		],
            [
                'id'=>2,
                'name'=>"Admin",
            ],
            [
                'id'=>3,
                'name'=>"Editor",
            ],
            [
                'id'=>4,
                'name'=>"Cộng tác viên đăng sản phẩm",
            ],
            [
                'id'=>5,
                'name'=>"Cộng tác viên đăng blog",
            ],
            [
                'id'=>6,
                'name'=>"Sale",
            ],
            [
                'id'=>7,
                'name'=>"Kế toán",
            ],
            [
                'id'=>8,
                'name'=>"Mua hàng",
            ],
    		
    	];
    	DB::table('role')->insert($data);
    }
}
