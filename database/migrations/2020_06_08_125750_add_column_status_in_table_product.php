<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnStatusInTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
           
            $table->integer('status')->default('0'); //0:Đang chờ duyệt, 1: Đã đăng bán 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('price');
            $table->integer('qty_product');
            $table->string('description');
            $table->text('detail');
            $table->integer('view')->nullable($value = true);
            $table->integer('buy')->nullable($value = true);
            $table->integer('like')->nullable($value = true);
            $table->string('img');
            $table->string('promotion'); 
            $table->unsignedBigInteger('creater');
            $table->foreign('creater')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_cate');
            $table->foreign('id_cate')
                ->references('id')
                ->on('category')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }
}
