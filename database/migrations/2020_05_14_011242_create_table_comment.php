<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // người dùng bình luận lấy id người dùng, id người tọa, id sản phẩm
        // Lấy id sản phẩm tìm đến người tạo
        Schema::create('comment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content');
            $table->integer('id_check'); // của blog hay là của sản phẩm 0:prod, 1:blog
	        $table->integer('id_obj');
            $table->integer('status'); //0:chưa xem, 1:đã xem
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
